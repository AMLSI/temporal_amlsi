To create problem files, run createTrainingProblems.sh.  

The parameters of the problem are (rows, cols, robots)

The ranges for the competition will span: rows: (4-6), cols: (3,6)
robots: (2,3).  Though I expect to create randomaized problems, the
values for rows, cols, robots will not deviate much from the values
listed in the createTrainingProblems script.

I will select 5 to 10 randomly chosen problem instances from this
benchmark for testing.



