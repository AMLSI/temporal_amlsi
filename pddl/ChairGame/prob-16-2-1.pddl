(define (problem P16E2S30)
  (:domain ChairGame)
(:objects
 P1  P2  P3  P4  P5  P6  P7  P8  P9  P10  P11  P12  P13  P14  P15  P16  - person
 NO1  NO2  - empty )
(:init
(before P1 P5)
(before P5 P15)
(before P15 P4)
(before P4 NO1)
(before NO1 P3)
(before P3 P7)
(before P7 P8)
(before P8 P6)
(before P6 P10)
(before P10 P11)
(before P11 P9)
(before P9 P13)
(before P13 P14)
(before P14 P12)
(before P12 P16)
(before P16 P2)
(before P2 NO2)
(before NO2 P1)
)
(:goal (and
(before P1 P2)
(before P2 P3)
(before P3 P4)
(before P4 P5)
(before P5 P6)
(before P6 P7)
(before P7 P8)
(before P8 P9)
(before P9 P10)
(before P10 P11)
(before P11 P12)
(before P12 P13)
(before P13 P14)
(before P14 P15)
(before P15 P16)
))
)
