(define (domain matchcellar)
(:requirements :strips :typing :negative-preconditions :durative-actions)
(:types 
fuse match - object
)(:predicates
	(handfree)
	(unused ?match - MATCH)
	(mended ?fuse - FUSE)
	(light ?match - MATCH)
)
(:durative-action mend_fuse
	:parameters ( ?fuse - FUSE ?match - MATCH )
	:duration (= ?duration 2.0)
	:condition (and
		(at start (handfree) )
		(at start (not (mended ?fuse)) )
		(over all (light ?match) )
	)
	:effect (and
		(at start (not (handfree)) )
		(at end (mended ?fuse) )
		(at end (handfree) )
	)
)
(:durative-action light_match
	:parameters ( ?match - MATCH )
	:duration (= ?duration 5.0)
	:condition (and
		(at start (unused ?match) )
	)
	:effect (and
		(at start (not (unused ?match)) )
		(at start (light ?match) )
		(at end (not (light ?match)) )
	)
)
)