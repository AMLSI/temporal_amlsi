;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; The pizza domain 2013
;;
;; In this domain, a robot waiter has to cut one or more pizzas and serve
;; slices of the same size to a set of guests. There are actions to hold a
;; pizza slice from a tray, to leave a pizza slice on a tray, to cut a pizza
;; slice in two equal-sized parts, and to serve a slice to a guest. Cutting
;; or serving a slice is not reversible, therefore a hidden difficulty of the
;; domain relays on the decision of choosing the right size for serving the
;; first slice. Problems in this domain have one or several slices situated in
;; one or several trays in the initial state, and goals consist of having all
;; guest served with slices of equal size.
;; 
;; Main difficulty of this domain comes from a possible large grounding due to
;; objects with irrelevant names. It is a problem derived from the STRIPS-PDDL
;; representation, that forces to define all possible future slices in advance.
;; Additionally, the creation of new slices can be realized with all unused
;; slice symbols
;;
;;
;; Author: Tomás de la Rosa and Raquel Fuentetaja
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain pizza)
(:requirements :typing)
(:types slice person tray size)

(:predicates (ontray ?x - slice ?y - tray)
	     (holding ?x - slice ?y - tray)
	     (served ?x - person)
	     (pizzasize ?x - slice ?z - size)
             (nextsize ?x - size ?y - size)
	     (servedsize ?x - size)
	     (undecidedsize)
	     (freearms)
	     (differ ?x - slice ?c - slice)
             (notexist ?s - slice))

(:action hold
  :parameters (?x - slice ?y - tray)
  :precondition (and (ontray ?x ?y)
		     (freearms))
  :effect (and (not (ontray ?x ?y))
	       (holding ?x ?y)
	       (not (freearms))))


(:action leave
  :parameters (?x - slice ?y - tray)
  :precondition (and (holding ?x ?y))
  :effect (and (ontray ?x ?y)
	       (not (holding ?x ?y))
	       (freearms)))


(:action cut
  :parameters (?s - slice ?half1 - slice ?half2 - slice ?t - tray ?z1 - size ?zhalf - size)
  :precondition (and (differ ?half1 ?half2)
		     (holding ?s ?t)
		     (pizzasize ?s ?z1)
		     (nextsize ?z1 ?zhalf)
                     (notexist ?half1)
		     (notexist ?half2))
  :effect (and (not (holding ?s ?t))
	       (freearms)
               (not (notexist ?half1))
	       (not (notexist ?half2))
	       (ontray ?half1 ?t)
	       (ontray ?half2 ?t)
               (pizzasize ?half1 ?zhalf)
	       (pizzasize ?half2 ?zhalf)))


(:action first-serve
  :parameters (?s - slice ?p - person ?t - tray ?z - size)
  :precondition (and (ontray ?s ?t)
		     (pizzasize ?s ?z)
		     (freearms)
		     (undecidedsize))
  :effect (and (not (ontray ?s ?t))
	       (served ?p)
               (not (undecidedsize))
               (servedsize ?z)))


(:action serve
  :parameters (?s - slice ?p - person ?t - tray ?z - size)
  :precondition (and (ontray ?s ?t)
		     (pizzasize ?s ?z) 
                     (freearms)
		     (servedsize ?z))
  :effect (and (not (ontray ?s ?t))
	       (served ?p))))

