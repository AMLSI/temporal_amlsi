(define (domain SHORTEST)
	(:requirements :strips :typing)
	(:types node)
	(:predicates
		(at ?n - node)
		(connected ?n1 - node ?n2 - node)
	)

	(:action move_to
		 :parameters (?n1 - node ?n2 - node)
	     :precondition (and (at ?n1) (connected ?n1 ?n2))
	     :effect
	     	 (and
             (not (at ?n1))
             (at ?n2)
		     )
	)
)