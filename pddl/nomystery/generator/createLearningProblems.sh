#!/bin/bash

nomystery -l  8 -p  8 -c 1.5 -s 2014 -e 0 > p-c15-08.pddl
nomystery -l 10 -p 10 -c 1.5 -s 2014 -e 0 > p-c15-10.pddl
nomystery -l 12 -p 12 -c 1.5 -s 2014 -e 0 > p-c15-12.pddl
nomystery -l 14 -p 14 -c 1.5 -s 2014 -e 0 > p-c15-14.pddl
nomystery -l 16 -p 16 -c 1.5 -s 2014 -e 0 > p-c15-16.pddl

nomystery -l  8 -p  8 -c 1.3 -s 2014 -e 0 > p-c13-08.pddl
nomystery -l 10 -p 10 -c 1.3 -s 2014 -e 0 > p-c13-10.pddl
nomystery -l 12 -p 12 -c 1.3 -s 2014 -e 0 > p-c13-12.pddl
nomystery -l 14 -p 14 -c 1.3 -s 2014 -e 0 > p-c13-14.pddl
nomystery -l 16 -p 16 -c 1.3 -s 2014 -e 0 > p-c13-16.pddl

nomystery -l  8 -p  8 -c 1.1 -s 2014 -e 0 > p-c11-08.pddl
nomystery -l 10 -p 10 -c 1.1 -s 2014 -e 0 > p-c11-10.pddl
nomystery -l 12 -p 12 -c 1.1 -s 2014 -e 0 > p-c11-12.pddl
nomystery -l 14 -p 14 -c 1.1 -s 2014 -e 0 > p-c11-14.pddl
nomystery -l 16 -p 16 -c 1.1 -s 2014 -e 0 > p-c11-16.pddl

nomystery -l  8 -p  8 -c 1.0 -s 2014 -e 0 > p-c10-08.pddl
nomystery -l 10 -p 10 -c 1.0 -s 2014 -e 0 > p-c10-10.pddl
nomystery -l 12 -p 12 -c 1.0 -s 2014 -e 0 > p-c10-12.pddl
nomystery -l 14 -p 14 -c 1.0 -s 2014 -e 0 > p-c10-14.pddl
nomystery -l 16 -p 16 -c 1.0 -s 2014 -e 0 > p-c10-16.pddl

