(define (problem initial_states1)
(:domain termes)
(:objects
    n0 - numb
    n1 - numb
    pos-0 - position
    pos-1 - position
    pos-11 - position
)
(:init
    (at pos-0)
    (SUCC n1 n0)
    (height pos-0 n0)
    (height pos-1 n0)
    (height pos-11 n0)
    (IS-DEPOT pos-1)
    (NEIGHBOR pos-0 pos-1)
    (NEIGHBOR pos-1 pos-0)
    (NEIGHBOR pos-0 pos-11)
    (NEIGHBOR pos-11 pos-0)
    (NEIGHBOR pos-1 pos-11)
    (NEIGHBOR pos-11 pos-1)
)
(:goal
(and
)
)
)