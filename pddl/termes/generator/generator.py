#!/usr/bin/env python
import sys, random
import datetime

#*****************#
# Functions declarations
#*****************#
def get_objects():
    str_objects="\n"

    # -- numb
    for i in range(0, num_numb+1):
        str_objects=str_objects+"\n\tn"+str(i)+" - numb"

    # -- position
    for i in range(0,num_pos):
        for j in range(0,num_pos):
            str_objects=str_objects+"\n\tpos-"+str(i)+"-"+str(j)+" - position"
           
    return(str_objects+"\n")
   


#*****************#
def get_init():
    str_init="\n"
    
    str_init=str_init+"\n\t(at pos-0-0)"
   
    #height
    for i in range(0,num_pos):
        for j in range(0,num_pos):
            str_init=str_init+"\n\t(height pos-"+str(i)+"-"+str(j)+" n0)"

    #succ
    for i in range(1, num_numb+1):
        str_init=str_init+"\n\t(SUCC n"+str(i)+" n"+str(i-1)+")"

    #NIGHBOR
    for i in range(0,num_pos):
        for j in range(0,num_pos):
            if(i-1 > 0):
                str_init=str_init+"\n\t(NEIGHBOR pos-"+str(i)+"-"+str(j)+" pos-"+str(i-1)+"-"+str(j)+")"
            if(j-1 > 0):
                str_init=str_init+"\n\t(NEIGHBOR pos-"+str(i)+"-"+str(j)+" pos-"+str(i)+"-"+str(j-1)+")"
            if(i+1 < num_pos):
                str_init=str_init+"\n\t(NEIGHBOR pos-"+str(i)+"-"+str(j)+" pos-"+str(i+1)+"-"+str(j)+")"
            if(j+1 < num_pos):
                str_init=str_init+"\n\t(NEIGHBOR pos-"+str(i)+"-"+str(j)+" pos-"+str(i)+"-"+str(j+1)+")"

    #is depot
    i = random.randint(0,num_pos)
    j = random.randint(0,num_pos)
    str_init=str_init+"\n\t(IS-DEPOT p-"+str(i)+"-"+str(j)+")"
    return(str_init+"\n")

#*****************#
def get_goals():
   str_goal=""
   str_goal=str_goal+"\n  (and\n"

    for i in range(0,num_pos):
        for j in range(0,num_pos):
            numb = random.randint(0,num_numb+1)
            str_init=str_init+"\n\t(height pos-"+str(i)+"-"+str(j)+" n"+str(numb)+")"
   for i in range(1,num_nuts+1):
      str_goal=str_goal+ "   (tightened nut"+str(i)+")\n"
            
   str_goal=str_goal+")"
   return(str_goal)
#*****************#

#*****************#
# MAIN
#*****************#
# Reading the command line arguments
try:
   num_numb = int(sys.argv[1])
   num_pos =  int(sys.argv[2])
   name = sys.argv[3]
   print name
except:
   print "Usage: " +sys.argv[0] + " <num_spanners> <num_nuts> <num_locations>"
   print "  num_numb (min 1)"
   print "  num_pos (min 1)"

   sys.exit(1)

seed = datetime.datetime.now().strftime( "%s")
random.seed( seed )
filename= sys.argv[4]
with open( filename, "w") as output:
   output.write("(define (problem "+name+")")
   output.write(" (:domain termes)")
   output.write(" (:objects "+ get_objects()+")")
   output.write(" (:init " + get_init()+")")
   output.write(" (:goal"+ get_goals()+"))")

print "wrote domain to:" +  filename
sys.exit(0)
