(define (problem depotprob1818) (:domain Depot)
(:objects
	depot - Depot
	distributor - Distributor
	truck - Truck
	pallet - Pallet
	crate - Crate
	hoist - Hoist)
(:init
	(at pallet depot)
	(clear crate)
	(at truck distributor)
	(at hoist depot)
	(available hoist)
	(at crate distributor)
	(on crate pallet)
)

(:goal (and
	)
))