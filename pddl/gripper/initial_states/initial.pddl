(define (problem gripper-x-1)
   (:domain gripper-typed)
   (:requirements :typing)
   (:objects r1 r2 - room
             b - ball
	     grip - gripper)
   (:init (at-robby r1)
          (free grip)
          (at b r1))
   (:goal ()))
