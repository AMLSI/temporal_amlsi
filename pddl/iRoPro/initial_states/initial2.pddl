(define (problem study-task3-swap)
(:domain iRoPro)
(:objects
	posA posB posC - position
	c - cube
	b - base
	r - roof)
(:init
	(clear c)
	(on c posA)
	(flat c)
	(thin c)
	(stackable c posA)
	(stackable c posB)
	(stackable c posC)
	(stackable c b)
	(not (stackable c r))
	
	(clear b)
	(on b posB)
	(flat b)
	(not(thin b))
	(stackable b posA)
	(stackable b posB)
	(stackable b posC)
	(not (stackable b r))
	(not (stackable b c))

	(clear r)
	(on r posC)
	(thin r)
	(not(flat r))
	(stackable r posA)
	(stackable r posB)
	(stackable r posC)
	(stackable r b)
	(stackable r c)

	(not (clear posA))
	(not (clear posB))
	(not (clear posC))

)
(:goal (and(on r posB) (on c posA)) )
)
