(define (problem study-task3-swap)
(:domain iRoPro)
(:objects
	posA posB posC - position
	c - cube
	b - base
	r - roof)
(:init
	(not(clear c))
	(flat c)
	(thin c)
	(stackable c posA)
	(stackable c posB)
	(stackable c posC)
	(stackable c b)
	(not (stackable c r))
	
	(not(clear b))
	(flat b)
	(not(thin b))
	(stackable b posA)
	(stackable b posB)
	(stackable b posC)
	(not (stackable b r))
	(not (stackable b c))

	(clear r)
	(thin r)
	(not(flat r))
	(stackable r posA)
	(stackable r posB)
	(stackable r posC)
	(stackable r b)
	(stackable r c)

	(clear posA)
	(not (clear posB))
	(clear posC)
	(on c b)
	(on b posB)
	(on r c)

)
(:goal (and(on r posB) (on c posA)) )
)
