(define (domain iRoPro)
(:requirements :strips :typing :negative-preconditions)
(:types
	element - object
	position - element
	obj - element
	cube - obj
	base - obj
	roof - obj)

(:predicates
	(clear ?e - element)
	(thin ?e - obj)
	(flat ?e - obj)
	(on ?obj2 - obj ?obj1 - element)
	(stackable ?obj2 - obj ?obj1 - element)
)

(:action move-vacuum
  :parameters (?obj - obj ?fromLoc - element ?toLoc - element)
  :precondition (and (on ?obj ?fromLoc)
                     (clear ?toLoc)
		     (not(clear ?fromLoc))
		     (flat ?obj)
		     (stackable ?obj ?toLoc)
		     (clear ?obj) )
  :effect (and (on ?obj ?toLoc)
  	       (not(clear ?toLoc))
	       (clear ?fromLoc)
	       (not(on ?obj ?fromLoc)) )
)

(:action move-grip
  :parameters (?obj - obj ?fromLoc - element ?toLoc - element)
  :precondition (and (on ?obj ?fromLoc)
  		     (clear ?toLoc)
		     (not(clear ?fromLoc))
		     (thin ?obj)
		     (stackable ?obj ?toLoc)
		     (clear ?obj) )
  :effect (and (on ?obj ?toLoc)
  	       (not(clear ?toLoc))
	       (clear ?fromLoc)
	       (not(on ?obj ?fromLoc)) )
)

(:action side-pp
  :parameters (?obj - obj ?fromLoc - element ?toLoc - element)
  :precondition (and (on ?obj ?fromLoc)
  		     (clear ?toLoc)
		     (thin ?obj)
		     (stackable ?obj ?toLoc)
		     (not(clear ?obj)) )
  :effect (and (on ?obj ?toLoc)
  	       (not(clear ?fromLoc))
	       (clear ?fromLoc)
	       (not(on ?obj ?fromLoc)) )
)

)