#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 09:43:25 2020

@author: maxence
"""
from tabulate import tabulate
from math import sqrt

def mean(l):
    if len(l) == 0:
        return 0
    
    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

#
def std(l):
    ll = []
    N=0
    for x in l:
        ll.append(float(x))
        N+=1
    mean_ = mean(l)
    sum_=0    
    for x in ll:
        sum_+=((x - mean_)*(x - mean_))
    return float(sqrt((1/N)*sum_))

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def read_results(domain, method="amlsi",directory="./",plan=True,Automaton=True, Std=False):
    reader= Reader(domain, method, directory)
    print("Benchmark")
    reader.print_benchmark_info()
    if(Automaton):
        print("Automaton properties")
        reader.print_automaton_results()
    print("Overall results")
    reader.print_domain_results()
    if(plan):
        reader.print_planning_test()
    if(Std):
        print("Standard Deviation")
        reader.print_std_split()
    
def read_results_ablation(domain, method="amlsi",directory="./"):
    reader= Reader(domain, method, directory)
    print("Ablation")
    print("Generation Step alone")
    reader.print_domain_results(key="generation")
    reader.print_planning_test(key="generation")
    print("Without Tabu Search")
    reader.print_domain_results(key="simple")
    reader.print_planning_test(key="simple")
    print("Tabu Search Alone")
    reader.print_domain_results(key="tabu")
    reader.print_planning_test(key="tabu")
    
class domain_data:
    def __init__(self):
        self.metrics = ["fscore",
                        "precondition",
                        "effect",
                        "distance",
                        "solve",
                        "accuracy",
                        "time"]
        self.benchmark_info = ["act","pred","I+","I-","i+","i-",
                               "E+","E-","e+","e-"]
        self.data = {}
        self.data_is1 = {}
        self.data_is2 = {}
        self.data_is3 = {}
        for metric in self.metrics:
            self.data[metric] = {}
            self.data_is1[metric] = {}
            self.data_is2[metric] = {}
            self.data_is3[metric] = {}
            for f in {25, 100}:
                self.data[metric][f] = {}
                self.data_is1[metric][f] = {}
                self.data_is2[metric][f] = {}
                self.data_is3[metric][f] = {}
                for n in {0, 20}:
                    self.data[metric][f][n] = []
                    self.data_is1[metric][f][n] = []
                    self.data_is2[metric][f][n] = []
                    self.data_is3[metric][f][n] = []
                    
        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []
        
class automaton_data:
    def __init__(self):
        
        self.metrics = ["fscore",
                        "recall",
                        "precision",
                        "states",
                        "observations",
                        "transitions",
                        "compression"]
        
        self.data = {}
        for metric in self.metrics:
            self.data[metric] = []
            
class Reader():
    def __init__(self, domain, method="amlsi",directory="./"):
        self.fluent = {-1:25, 100:25, 25:100}
        self.noise = {-1:0, 0:20, 20:0}
        self.domain = domain
        self.method = method
        self.directory = directory
        self.data = {}
        self.data["amlsi"]=domain_data()
        self.data["generation"]=domain_data()
        self.data["simple"]=domain_data()
        self.data["tabu"]=domain_data()
        self.data["automaton"]=automaton_data()
        
    def read_log_domain(self):
        #Read log files
        for initial in [1,2,3]: 
            file_ = ("%s%s_%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            key = "amlsi"
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                        key = "amlsi"
                    elif line[:4] == "Time":
                        self.data[key].data["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:6] == "FSCORE":
                        self.data[key].data["fscore"][fluent][noise].append(line[9:])
                        continue
                    elif line[:23] == "Error Rate Precondition":
                        self.data[key].data["precondition"][fluent][noise].append(line[26:])
                        continue
                    elif line[:24] == "Error Rate Postcondition":
                        self.data[key].data["effect"][fluent][noise].append(line[27:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data[key].data["distance"][fluent][noise].append(line[23:])
                        continue
                    elif line[:20] == "Only generation step":
                        key = "generation"
                        continue
                    elif line[:19] == "Without tabu search":
                        key = "simple"
                        continue
                    elif line[:17] == "Tabu search alone":
                        key = "tabu"
                        continue
                    else:
                        continue
                    
    def read_log_domain_split_is(self):
        #Read log files
        for param in [[1, self.data["amlsi"].data_is1],[2, self.data["amlsi"].data_is2],[3, self.data["amlsi"].data_is3]]:
            initial = param[0]
            data = param[1]
            file_ = ("%s%s_%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            key = "amlsi"
            p = False
            fluent = -1
            noise= -1
            amlsi=True
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                        key = "amlsi"
                        amlsi=True
                    elif line[:4] == "Time" and amlsi:
                        data["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:6] == "FSCORE" and amlsi:
                        data["fscore"][fluent][noise].append(line[9:])
                        continue
                    elif line[:23] == "Error Rate Precondition" and amlsi:
                        data["precondition"][fluent][noise].append(line[26:])
                        continue
                    elif line[:24] == "Error Rate Postcondition" and amlsi:
                        data["effect"][fluent][noise].append(line[27:])
                        continue
                    elif line[:20] == "Syntactical distance" and amlsi:
                        data["distance"][fluent][noise].append(line[23:])
                        continue
                    elif line[:20] == "Only generation step":
                        key = "generation"
                        amlsi=False
                        continue
                    elif line[:19] == "Without tabu search":
                        key = "simple"
                        amlsi=False
                        continue
                    elif line[:17] == "Tabu search alone":
                        key = "tabu"
                        amlsi=False
                        continue
                    else:
                        continue
                    
        # file_ = ("%s%s_%s/planning_test_results.txt" % 
        #          (self.directory, self.domain, self.method))
        # raws = extract_line(file_)
        # fluent=-1
        # noise=-1
        # datas = {1:self.data["amlsi"].data_is1,
        #          2:self.data["amlsi"].data_is2,
        #          3:self.data["amlsi"].data_is3}
        # n = 0
        # for line in raws:
        #     if line[:13] == "Initial State":
        #         data=datas[int(line[14:])]
        #         fluent=-1
        #     elif line[:4] == "### ":
        #         fluent = self.fluent[fluent]
        #         noise = -1
        #     elif line[:4] == "*** ":
        #         noise = self.noise[noise]
        #         key = "amlsi"
        #     elif line[:20] == "Only generation step":
        #         key = "generation"
        #         continue
        #     elif line[:19] == "Without tabu search":
        #         key = "simple"
        #         continue
        #     elif line[:17] == "Tabu search alone":
        #         key = "tabu"
        #         continue
        #     else:
        #         if key == "amlsi": 
        #             tab = line.split()
        #             if(tab[1] == "problems"):
        #                 n = float(tab[0])
        #             elif(tab[1] == "solved"):
        #                 data["solve"][fluent][noise].append(float(tab[0])/n)
        #             elif(tab[1] == "correctly"):
        #                 data["accuracy"][fluent][noise].append(float(tab[0])/n)
        #                 continue
        #         else:
        #             continue
                    
    def read_log_acc(self):
        #Read log files
        file_ = ("%s%s_%s/planning_test_results.txt" % 
                     (self.directory, self.domain, self.method))
        raws = extract_line(file_)
        key = "amlsi"
        fluent = -1
        noise= -1
        n = 0
        for line in raws:
            if line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = -1
            elif line[:4] == "*** ":
                noise = self.noise[noise]
                key = "amlsi"
            elif line[:20] == "Only generation step":
                key = "generation"
                continue
            elif line[:19] == "Without tabu search":
                key = "simple"
                continue
            elif line[:17] == "Tabu search alone":
                key = "tabu"
                continue
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    self.data[key].data["solve"][fluent][noise].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    self.data[key].data["accuracy"][fluent][noise].append(float(tab[0])/n)
                    continue
            
    def print_domain_results(self, key="amlsi"):
        self.read_log_domain()
        heads = ["Fluents", "Noise","fscore","prec","eff","dist"]
        res = []
        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res_ = []
            res_.append(param[0])
            res_.append(param[1])
            for metrics in self.data[key].metrics:
                if(metrics == "time"):
                    continue
                if(metrics == "accuracy"):
                    continue
                if(metrics == "solve"):
                    continue
                else:
                    res_.append(mean(self.data[key].data[metrics][res_[0]][res_[1]])*100)
                #print(metrics)
                #print(res_)
            res.append(res_)
            
        print(tabulate(res, headers=heads))
        
    def print_planning_test(self, key="amlsi"):
        self.read_log_acc()
        heads = ["Fluents", "Noise","Solved","Accuracy"]
        res = []
        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res_ = []
            res_.append(param[0])
            res_.append(param[1])
            for metrics in self.data[key].metrics:
                if(metrics == "time"):
                    continue
                if(metrics == "accuracy"):
                    res_.append(mean(self.data[key].data[metrics][res_[0]][res_[1]])*100)
                    continue
                if(metrics == "solve"):
                    res_.append(mean(self.data[key].data[metrics][res_[0]][res_[1]])*100)
                    continue
                else:
                    continue
                    
                #print(metrics)
                #print(res_)
            res.append(res_)
            
        print(tabulate(res, headers=heads))
    
    def print_std_split(self):
        self.read_log_domain_split_is()
        heads = ["Fluents", "Noise","fscore","prec","eff","dist","solved","accuracy"]
        res_ = {}
        res=[]
        for p in [[1, self.data["amlsi"].data_is1],[2, self.data["amlsi"].data_is2],[3, self.data["amlsi"].data_is3]]:
            initial = p[0]
            data = p[1]
            res_[initial]={}
            for f in [100, 25]:
                res_[initial][f] = {}
                for n in [0,20]:
                    res_[initial][f][n] = {}
                    for metrics in self.data["amlsi"].metrics:
                        if(metrics == "time"):
                            continue
                        else:
                            res_[initial][f][n][metrics] = mean(data[metrics][f][n])*100
        
        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res2 = []
            res2.append(param[0])
            res2.append(param[1])
            tmp = []
            for metric in self.data["amlsi"].metrics:
                if(metric == "time"):
                    continue
                else:
                    tmp = []
                    for initial in [1,2,3]:
                        tmp.append(res_[initial][param[0]][param[1]][metric])
                    res2.append(std(tmp))
            res.append(res2)          
        print(tabulate(res, headers=heads))
        
    def read_log_benchmark(self):
        for initial in [1,2,3]: 
            file_ = ("%s%s_%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:7] == "I+ size":
                    self.data["amlsi"].data_benchmark["I+"].append(line[9:])
                elif line[:7] == "I- size":
                    self.data["amlsi"].data_benchmark["I-"].append(line[9:])
                elif line[:12] == "x+ mean size":
                    self.data["amlsi"].data_benchmark["i+"].append(line[14:])
                elif line[:12] == "x- mean size":
                    self.data["amlsi"].data_benchmark["i-"].append(line[14:])
                elif line[:7] == "E+ size":
                    self.data["amlsi"].data_benchmark["E+"].append(line[9:])
                elif line[:7] == "E- size":
                    self.data["amlsi"].data_benchmark["E-"].append(line[9:])
                elif line[:12] == "e+ mean size":
                    self.data["amlsi"].data_benchmark["e+"].append(line[14:])
                elif line[:12] == "e- mean size":
                    self.data["amlsi"].data_benchmark["e-"].append(line[14:])
                elif line[:9] == "# actions":
                    self.data["amlsi"].data_benchmark["act"].append(line[10:])
                elif line[:11] == "# predicate":
                    self.data["amlsi"].data_benchmark["pred"].append(line[12:])
                else:
                    continue
                
    def print_benchmark_info(self):
        self.read_log_benchmark()
        res = []
        res_ = []
        for info in self.data["amlsi"].benchmark_info:
            res_.append(mean(self.data["amlsi"].data_benchmark[info]))
            
        res.append(res_)
        print(tabulate(res, headers=self.data["amlsi"].benchmark_info))
        
    def read_log_automaton(self):
        for initial in [1,2,3]: 
            file_ = ("%s%s_%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:16] == "#Observed states":
                    self.data["automaton"].data["observations"].append(line[18:])
                elif line[:7] == "#States":
                    self.data["automaton"].data["states"].append(line[9:])
                elif line[:12] == "#Transitions":
                    self.data["automaton"].data["transitions"].append(line[14:])
                elif line[:17] == "Compression level":
                    self.data["automaton"].data["compression"].append(line[19:])
                elif line[:6] == "Recall":
                    self.data["automaton"].data["recall"].append(line[9:])
                elif line[:9] == "Precision":
                    self.data["automaton"].data["precision"].append(line[12:])
                elif line[:16] == "Fscore automaton":
                    self.data["automaton"].data["fscore"].append(line[18:])
                else:
                    continue
                
    def print_automaton_results(self):
        self.read_log_automaton()
        heads = ["fscore","R","P","#States","#Obs","#Transitions","Compression level"]
        res = []
        res_ = []
        for metrics in self.data["automaton"].metrics:
            if(metrics == "states" or metrics == "observations" or metrics == "transitions" or metrics == "compression"):
                res_.append(mean(self.data["automaton"].data[metrics]))
            else:
                res_.append(mean(self.data["automaton"].data[metrics])*100)
        res.append(res_)
        print(tabulate(res, headers=heads))
        
