#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Grand Maxence
"""

from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
import os, sys, shutil

def test_plan(file_plan, problem, domain):
    if(os.path.getsize(file_plan) == 0):
        return [0,0]
    f = open(file_plan, 'r')
    for line in f:
        if(line[0:len(line)-1] == "NO SOLUTION" or line==""):
            return [0,0]
        else:
            break

    command = "../validate -v -t 0.0001 %s %s %s > /dev/null 2> /dev/null" % (domain, problem, file_plan)
    ret = os.system(command)
    # print(command)
    if(ret == 0):
        return [1,1]
    else:
        return [1,0]

def accuracy_domain(domain, rep_problem, rep_plans, n):
    s = 0
    a = 0
    for i in range(n):
        nn = i+1
        plan_file = "%s/plan_%d" % (rep_plans, nn)
        problem_file = "%s/instance-%d.pddl" % (rep_problem, nn)
        score_ = test_plan(plan_file, problem_file, domain)
        s += score_[0]
        a += score_[1]
    return [s/n*100, a/n*100]

def cost(file_plan):
    if(os.path.getsize(file_plan) == 0):
        return 0
    f = open(file_plan, 'r')
    first=True
    max_value=0
    for line in f:
        line = line[0:len(line)-1]
        if(first):
            if(line == "NO SOLUTION" or line==""):
                return 0
            first=False
        else:
            if(line != ""):
                start = float(line.split(":")[0])
                line=line.split(":")[1]
                tab=line.split(" ")
                line=tab[len(tab)-1]
                duration=float(line[1:len(line)-1])
                value=start+duration
                if(max_value < value):
                    max_value=value
    return max_value

def score(plan_ref, plan):
    c = cost(plan_ref)
    cBis = cost(plan)
    if(cBis == 0):
        return 0
    return c / cBis

def score_domain(rep_ref, rep, n=20):
    score_ = 0
    for i in range(n):
        nn = i+1
        plan_file_ref = "%s/plan_%d" % (rep_ref, nn)
        plan_file = "%s/plan_%d" % (rep, nn)
        score_ += score(plan_file_ref, plan_file)
    return score_

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def mean(l):
    if len(l) == 0:
        return 0

    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Delta")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg

def read_results(domain, method="amlsi",directory="./"):
    reader= Reader(domain, method, directory)
    print("Overall results")
    reader.print_domain_results()
    print("\nStandard deviation between configurations")
    reader.print_conf_std()
    print("\nResults for each initial states")
    reader.print_domain_results_split()
    print("\nStandard deviation between initial states")
    reader.print_std_split()
    print("\nAutomaton results")
    reader.print_automaton_results()
    print("\nBenchmark info")
    reader.print_benchmark_info()

class Reader:
    def __init__(self, domain, method="amlsi_convergent2",directory="./"):

        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:25, 25:100, 100:25}
        self.noise = {-1:0, 0:1, 1:10, 10:0}
        self.metrics_domain = ["fscore",
                               "fscore2",
                               "precondition",
                               "effect",
                               "distance",
                               "solve",
                               "accuracy",
                               "ipc",
                               "iteration",
                               "time"]

        self.metrics_automaton = ["fscore",
                                  "recall",
                                  "precision",
                                  "states",
                                  "observations",
                                  "transitions",
                                  "compression",
                                  "time"]

        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]

        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []
        self.clear()

    def clear(self):
        self.data_automaton = {}
        for metric in self.metrics_automaton:
            self.data_automaton[metric] = []
        self.data_domain = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            for f in {25,100}:
                self.data_domain[metric][f] = {}
                for n in {0, 1, 10}:
                    self.data_domain[metric][f][n] = []

    def read_log_domain_planning(self, Delta=10):
        # file_ = ("%s/%s/%s/planning_results.txt" %
        #          (self.directory, self.domain, self.method))
        # raws = extract_line(file_)
        # fluent=-1
        # noise=-1
        # for line in raws:
        #     if line[:4] == "### ":
        #         fluent = 100
        #         noise = 0
        #     elif line[:4] == "*** ":
        #         noise = 0
        #     else:
        #         tab = line.split()
        #         if(tab[1] == "problems"):
        #             n = float(tab[0])
        #         elif(tab[1] == "solved"):
        #             self.data_domain["solve"][fluent][noise].append(float(tab[0])/n)
        #         elif(tab[1] == "correctly"):
        #             self.data_domain["accuracy"][fluent][noise].append(float(tab[0])/n)
        #         else:
        #             continue

        for initial in [1,2,3]:
            for run in [0,1,2,3,4]:
                for fluent in [25,100]:
                    for noise in [0,1,10]:
                        file_ = ("%s/%s/%s/plans/IS%d/RUN%d/OBS%d/NOISE%d" %
                                 (self.directory, self.domain, self.method, initial, run,fluent,noise))
                        file_ref = ("../pddl/temporal/%s/reference/plans" % self.domain)
                        self.data_domain["ipc"][fluent][noise].append(score_domain(file_ref, file_))
                        domain_file = ("../pddl/temporal/%s/domain.pddl" % self.domain)
                        rep_instances = ("../pddl/temporal/%s/instances" % self.domain)
                        accSolve = accuracy_domain(domain_file, rep_instances, file_, 20)
                        # print(accSolve)
                        # print(file_)
                        self.data_domain["solve"][fluent][noise].append(accSolve[0])
                        self.data_domain["accuracy"][fluent][noise].append(accSolve[1])


    def read_log_domain(self, Delta=10):
        #Read log files
        for initial in [1,2,3]:
            file_ = ("%s/%s/%s/IS%d/log.txt" % \
                     (self.directory, self.domain,self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                    elif line[:14] == "Temoral FSCORE":
                        self.data_domain["fscore"][fluent][noise].append(line[17:])
                        continue
                    elif line[:4] == "Time":
                        self.data_domain["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:19] == "Intermediate FSCORE":
                        self.data_domain["fscore2"][fluent][noise].append(line[21:])
                        continue
                    elif line[:9] == "Iteration":
                        self.data_domain["iteration"][fluent][noise].append(line[9:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise].append(line[23:])
                        continue
                    else:
                        continue

        for initial in [1,2,3]:
            file_ = ("%s/%s/%s/IS%d/log.txt" % \
                     (self.directory, self.domain,self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:16] == "#Observed states":
                    self.data_automaton["observations"].append(line[18:])
                elif line[:7] == "#States":
                    self.data_automaton["states"].append(line[9:])
                elif line[:12] == "#Transitions":
                    self.data_automaton["transitions"].append(line[14:])
                elif line[:17] == "Compression level":
                    self.data_automaton["compression"].append(line[19:])
                elif line[:6] == "Recall":
                    self.data_automaton["recall"].append(line[9:])
                elif line[:9] == "Precision":
                    self.data_automaton["precision"].append(line[12:])
                elif line[:13] == "Automaton Time":
                    self.data_automaton["time"].append(line[16:])
                elif line[:16] == "Fscore automaton":
                    self.data_automaton["fscore"].append(line[18:])
                else:
                    continue

    def read_log_domain2(self, Delta=10):
        #Read log files
        for initial in [1,2,3]:
            file_ = ("%s/%s/%s/IS%d/log.txt" % \
                     (self.directory, self.domain,self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent2[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                    elif line[:14] == "Temoral FSCORE":
                        self.data_domain["fscore"][fluent][noise].append(line[17:])
                        continue
                    elif line[:4] == "Time":
                        self.data_domain["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:19] == "Intermediate FSCORE":
                        self.data_domain["fscore2"][fluent][noise].append(line[21:])
                        continue
                    elif line[:9] == "Iteration":
                        self.data_domain["iteration"][fluent][noise].append(line[9:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise].append(line[23:])
                        continue
                    else:
                        continue

        for initial in [1,2,3]:
            file_ = ("%s/%s/%s/IS%d/log.txt" % \
                     (self.directory, self.domain,self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:16] == "#Observed states":
                    self.data_automaton["observations"].append(line[18:])
                elif line[:7] == "#States":
                    self.data_automaton["states"].append(line[9:])
                elif line[:12] == "#Transitions":
                    self.data_automaton["transitions"].append(line[14:])
                elif line[:17] == "Compression level":
                    self.data_automaton["compression"].append(line[19:])
                elif line[:6] == "Recall":
                    self.data_automaton["recall"].append(line[9:])
                elif line[:9] == "Precision":
                    self.data_automaton["precision"].append(line[12:])
                elif line[:13] == "Automaton Time":
                    self.data_automaton["time"].append(line[16:])
                elif line[:16] == "Fscore automaton":
                    self.data_automaton["fscore"].append(line[18:])
                else:
                    continue
        self.data_automaton["time"]

    def print_domain_results(self):
        self.read_log_domain()
        self.read_log_domain_planning()
        heads = ["Fluents", "Noise","fscore","dist","solve","accuracy","ipc","time"]
        res = []
        for f in [100,25]:
            for noise in [0,1,10]:
                res_ = []
                res_.append(f)
                res_.append(noise)
                for metrics in ["fscore", "distance", "solve", "accuracy", "ipc", "time"]:
                    if(metrics == "time" or metrics == "accuracy" or metrics == "solve" or metrics == "ipc"):
                        res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]]))
                    else:
                        res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]])*100)
                    #print(metrics)
                    #print(res_)
                res.append(res_)

        print(tabulate(res, headers=heads))


    def print_domain_results_no_planning(self):
        self.read_log_domain()
        heads = ["Fluents", "Noise","fscore","dist","time"]
        res = []
        for f in [100,25]:
            for noise in [0,1,10]:
                res_ = []
                res_.append(f)
                res_.append(noise)
                for metrics in ["fscore", "distance", "time"]:
                    if(metrics == "time" or metrics == "accuracy" or metrics == "solve" or metrics == "ipc"):
                        res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]]))
                    else:
                        res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]])*100)
                    #print(metrics)
                    #print(res_)
                res.append(res_)

        print(tabulate(res, headers=heads))

def read_log_domain(domain, directory):
    print("\n\nAlgorithm : 2 Operators")
    reader = Reader(domain, method="2OP", directory=directory)
    reader.print_domain_results()
    print("\n\nAlgorithm : 3 Operators")
    reader = Reader(domain, method="3OP", directory=directory)
    reader.print_domain_results()

def read_log_domain_no_planning(domain, directory):
    print("\n\nAlgorithm : 2 Operators")
    reader = Reader(domain, method="2OP", directory=directory)
    reader.print_domain_results_no_planning()
    print("\n\nAlgorithm : 3 Operators")
    reader = Reader(domain, method="3OP", directory=directory)
    reader.print_domain_results_no_planning()
