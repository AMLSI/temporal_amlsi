#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 10:06:42 2020

@author: maxence
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Grand Maxence
"""

from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
from math import sqrt
T=30

def mean(l):
    if len(l) == 0:
        return 0
    
    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

#
def std(l):
    ll = []
    N=0
    for x in l:
        ll.append(float(x))
        N+=1
    mean_ = mean(l)
    sum_=0    
    for x in ll:
        sum_+=((x - mean_)*(x - mean_))
    return float(sqrt((1/N)*sum_))

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def read_results(domain, method="amlsi",directory="./"):
    reader= Reader(domain, method, directory)
    print("Overall results")
    reader.print_domain_results()
    print("\nStandard deviation between configurations")
    reader.print_conf_std()
    print("\nResults for each initial states")
    reader.print_domain_results_split()
    print("\nStandard deviation between initial states")
    reader.print_std_split()
    print("\nAutomaton results")
    reader.print_automaton_results()
    print("\nBenchmark info")
    reader.print_benchmark_info()

class Reader:
    def __init__(self, domain, method="amlsi",directory="./"):
        
        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:25, 100:25, 25:100}
        self.noise = {-1:0, 0:20, 20:0}
        self.metrics_domain = ["fscore",
                               "precondition",
                               "effect",
                               "distance",
                               "solve",
                               "accuracy",
                               "time"]
        self.metrics_automaton = ["fscore",
                                  "recall",
                                  "precision",
                                  "states",
                                  "observations",
                                  "transitions",
                                  "compression"]
        
        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]
        
        self.data_domain = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            for f in {25, 100}:
                self.data_domain[metric][f] = {}
                for n in {0, 20}:
                    self.data_domain[metric][f][n] = {}
                    for it in range(T):
                        self.data_domain[metric][f][n][it+1] = []
                    
        self.data_automaton = {}
        for metric in self.metrics_automaton:
            self.data_automaton[metric] = {}
            for it in range(T):
                self.data_automaton[metric][it+1] = []
            
        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []
        
    def read_log_domain(self):
        #Read log files
        for initial in [1,2,3]:
            file_ = ("%s%s_%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            it=1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                        it=0
                    elif line[:6] == "FSCORE":
                        self.data_domain["fscore"][fluent][noise][it].append(line[9:])
                        continue
                    elif line[:23] == "Error Rate Precondition":
                        self.data_domain["precondition"][fluent][noise][it].append(line[26:])
                        continue
                    elif line[:24] == "Error Rate Postcondition":
                        self.data_domain["effect"][fluent][noise][it].append(line[27:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise][it].append(line[23:])
                        continue
                    elif line[:9] == "Iteration":
                        it+=1
                        continue
                    else:
                        continue
                    
    def read_acc(self):
        file_ = ("%s%s_%s/planning_test_results.txt" % 
                 (self.directory, self.domain, self.method))
        raws = extract_line(file_)
        fluent=-1
        noise=-1
        for line in raws:
            if line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = -1
            elif line[:4] == "*** ":
                noise = self.noise[noise]
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    self.data_domain["solve"][fluent][noise][30].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    self.data_domain["accuracy"][fluent][noise][30].append(float(tab[0])/n)
                else:
                    continue
        
   
    
    def plot_domain(self):
        self.read_log_domain()
        metrics = ["fscore", "distance"]
        conf = np.array([[100,0],[100,20],[25,0],[25,20]])
        to_plot={}
        for c in conf:
            fluent = c[0]
            noise = c[1]
            if not fluent in to_plot:
                to_plot[fluent]={}
            to_plot[fluent][noise]={}
            for m in metrics:
                y = np.zeros(T)
#                y2 = np.zeros(T)
                for it in range(1,T+1):
                    y[it-1] = mean(np.array([float(x)*100 for x in self.data_domain[m][fluent][noise][it]]))
#                    y2[it-1] = std(np.array([
#                            100 if float(x)*100 > 100 \
#                            else 0 if float(x)*100 < 0\
#                            else float(x)*100 for x in self.data_domain[m][fluent][noise][it]]))
#                to_plot[fluent][noise][m] = {}
#                to_plot[fluent][noise][m]["mean"] = y
#                to_plot[fluent][noise][m]["std"] = y2
                to_plot[fluent][noise][m]=y
        return to_plot
    
def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Iteration")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg
   
def print_res(domain, rep="../../experiment/", method="amlsi_incr2"):
    for gen in ["passive", "UCT2"]:
        reader= Reader(domain, method=method+"/"+gen,directory=rep)
        reader.read_log_domain()
        reader.read_acc()
        heads = ["Fluents", "Noise","fscore","prec","eff","dist", "solve", "accuracy"]
        res = []
        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res_ = []
            res_.append(param[0])
            res_.append(param[1])
            for metrics in reader.metrics_domain:
                if(metrics == "time"):
                    continue
                res_.append(mean(reader.data_domain[metrics][res_[0]][res_[1]][30])*100)
            res.append(res_)
        print(tabulate(res, headers=heads))
                
def print_res_passive(domain, rep="../../experiment/", method="amlsi_incr2"):
    for gen in ["passive"]:
        reader= Reader(domain, method=method+"/"+gen,directory=rep)
        reader.read_log_domain()
        reader.read_acc()
        heads = ["Fluents", "Noise","fscore","prec","eff","dist", "solve", "accuracy"]
        res = []
        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res_ = []
            res_.append(param[0])
            res_.append(param[1])
            for metrics in reader.metrics_domain:
                if(metrics == "time"):
                    continue
                res_.append(mean(reader.data_domain[metrics][res_[0]][res_[1]][30])*100)
            res.append(res_)
        print(tabulate(res, headers=heads))
        
def save_passive(domain, rep="../../experiment/", method="amlsi_incr2",\
                 repsave="./../plot/online/"):
    colors = {1:"b", 2:"g", 3:"r", 4:"k"}
    markers = {1:"d-", 2:"o-", 3:"^-", 4:"s-"}
    leg = {1:"(100,0)", 2:"(100,20)", 3:"(25,0)", 4:"(25,20)"}
    fig = plt.figure(num=None, figsize=(15, 5), dpi=80, facecolor='w', edgecolor='k')
    gs = gridspec.GridSpec(1,2)
    fscoreD = plt.subplot(gs[0, 0])
    distance = plt.subplot(gs[0, 1])
    legends1 = {"fscore":[], "distance":[]}
    legends2 = {"fscore":[], "distance":[]}
    sc = 1
    for fluent in [100,25]:
        for noise in [0,20]:    
            reader= Reader(domain, method=method+"/passive",directory=rep)
            to_plot_domain = reader.plot_domain()
            x = np.array(range(1,T+1))
            l1=plot(fscoreD, x, to_plot_domain[fluent][noise]["fscore"], \
                     "FScore",m=markers[sc], c=colors[sc])
            l2=plot(distance, x, to_plot_domain[fluent][noise]["distance"],\
                     "Syntactical Distance", m=markers[sc], c=colors[sc])
            legends1["fscore"].append(l1)
            legends1["distance"].append(l2)
            legends2["fscore"].append(leg[sc])
            legends2["distance"].append(leg[sc])
            sc += 1
    fscoreD.legend(legends1["fscore"], legends2["fscore"])
    distance.legend(legends1["distance"], legends2["distance"])
    fig.savefig("%s%s_passive.pdf" % (repsave, domain))
            
def save_passive_metric(domain, rep="../../experiment/", method="amlsi_incr2",\
                 repsave="./../plot/online/", metric="distance"):
    colors = {1:"b", 2:"g", 3:"r", 4:"k"}
    markers = {1:"d-", 2:"o-", 3:"^-", 4:"s-"}
    leg = {1:"(100,0)", 2:"(100,20)", 3:"(25,0)", 4:"(25,20)"}
    fig = plt.figure(num=None, figsize=(5, 5), dpi=80, facecolor='w', edgecolor='k')
    gs = gridspec.GridSpec(1,1)
    plot_ = plt.subplot(gs[0, 0])
    legends1 = {metric:[]}
    legends2 = {metric:[]}
    sc = 1
    for fluent in [100,25]:
        for noise in [0,20]:    
            reader= Reader(domain, method=method+"/passive",directory=rep)
            to_plot_domain = reader.plot_domain()
            x = np.array(range(1,T+1))
            l1=plot(plot_, x, to_plot_domain[fluent][noise][metric], \
                     metric,m=markers[sc], c=colors[sc])
            legends1[metric].append(l1)
            legends2[metric].append(leg[sc])
            sc += 1
    plot_.legend(legends1[metric], legends2[metric])
    fig.savefig("%s%s_passive_%s.pdf" % (repsave, domain, metric))
    
def save_res_multi(domain, rep="../../experiment/", method="amlsi_incr2", \
                   repsave="./../plot/online/"):
    colors = {"passive":"b", "UCT2":"g", "passive_denoise":"r", "UCT":"k"}
    markers = {"passive":"o-", "UCT2":"^-", "passive_denoise":"s-", "UCT":"d-"}
    l = {"passive":"passive", "UCT2":"active", "passive_denoise":"s-", "UCT":"d-"}
    for fluent in [100,25]:
        for noise in [0,20]:    
            fig = plt.figure(num=None, figsize=(15, 5), dpi=80, facecolor='w', edgecolor='k')
            gs = gridspec.GridSpec(1,2)
            fscoreD = plt.subplot(gs[0, 0])
            distance = plt.subplot(gs[0, 1])
            legends1 = {"fscore":[], "distance":[]}
            legends2 = {"fscore":[], "distance":[]}
            for gen in ["passive", "UCT2"]:
                reader= Reader(domain, method=method+"/"+gen,directory=rep)
                to_plot_domain = reader.plot_domain()
                x = np.array(range(1,T+1))
                l1=plot(fscoreD, x, to_plot_domain[fluent][noise]["fscore"], \
                     "FScore",m=markers[gen], c=colors[gen])
                l2=plot(distance, x, to_plot_domain[fluent][noise]["distance"],\
                     "Syntactical Distance", m=markers[gen], c=colors[gen])
                legends1["fscore"].append(l1)
                legends1["distance"].append(l2)
                legends2["fscore"].append(l[gen])
                legends2["distance"].append(l[gen])
            fscoreD.legend(legends1["fscore"], legends2["fscore"])
            distance.legend(legends1["distance"], legends2["distance"])
            fig.savefig("%s%s_%d_%d.pdf" % (repsave, domain, fluent, noise))