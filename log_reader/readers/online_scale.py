#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 14:50:29 2020

@author: maxence
"""

from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def mean(l):
    if len(l) == 0:
        return 0
    
    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Delta")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg

def read_results(domain, method="amlsi",directory="./"):
    reader= Reader(domain, method, directory)
    print("Overall results")
    reader.print_domain_results()
    print("\nStandard deviation between configurations")
    reader.print_conf_std()
    print("\nResults for each initial states")
    reader.print_domain_results_split()
    print("\nStandard deviation between initial states")
    reader.print_std_split()
    print("\nAutomaton results")
    reader.print_automaton_results()
    print("\nBenchmark info")
    reader.print_benchmark_info()

class Reader:
    def __init__(self, domain, method="amlsi_convergent2",directory="./"):
        
        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:25, 100:25, 25:100}
        self.noise = {-1:0, 0:20, 20:0}
        self.metrics_domain = ["fscore",
                               "precondition",
                               "effect",
                               "distance",
                               "solve",
                               "accuracy",
                               "iteration"]
        
        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]
        
        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []
        self.clear()
        
    def clear(self):
        self.data_domain = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            for f in {25, 100}:
                self.data_domain[metric][f] = {}
                for n in {0, 20}:
                    self.data_domain[metric][f][n] = []
    def read_log_domain(self, strategy="passive", scale=1):
        #Read log files
         
        file_ = ("%s%s_%s/%s/scale%d/log.txt" % \
                 (self.directory, self.domain, self.method,strategy,\
                  scale))
        raws = extract_line(file_)
        p = False
        fluent = -1
        noise= -1
        for line in raws:
            if line == "############################################":
                p = True
            if p:
                if line == "############################################":
                    noise = -1
                    continue
                elif line[:4] == "### ":
                    fluent = self.fluent[fluent]
                    noise = -1
                elif line[:4] == "*** ":
                    noise = self.noise[noise]
                elif line[:6] == "FSCORE":
                    self.data_domain["fscore"][fluent][noise].append(line[9:])
                    continue
                elif line[:23] == "Error Rate Precondition":
                    self.data_domain["precondition"][fluent][noise].append(line[26:])
                    continue
                elif line[:24] == "Error Rate Postcondition":
                    self.data_domain["effect"][fluent][noise].append(line[27:])
                    continue
                elif line[:9] == "Iteration":
                    self.data_domain["iteration"][fluent][noise].append(line[9:])
                    continue
                elif line[:20] == "Syntactical distance":
                    self.data_domain["distance"][fluent][noise].append(line[23:])
                    continue
                else:
                    continue
        file_ = ("%s%s_%s/%s/scale%d/planning_test_results.txt" % 
                 (self.directory, self.domain, self.method,strategy,scale))
        raws = extract_line(file_)
        fluent=-1
        noise=-1
        for line in raws:
            if line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = -1
            elif line[:4] == "*** ":
                noise = self.noise[noise]
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    self.data_domain["solve"][fluent][noise].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    self.data_domain["accuracy"][fluent][noise].append(float(tab[0])/n)
                else:
                    continue
        
    def save_passive(self, domain, metric="accuracy", rep="../../experiment/", method="amlsi_incr2",\
                 repsave="./../plot/online/"):
        colors = {1:"b", 2:"g", 3:"r", 4:"k"}
        markers = {1:"d-", 2:"o-", 3:"^-", 4:"s-"}
        leg = {1:"(100,0)", 2:"(100,20)", 3:"(25,0)", 4:"(25,20)"}
        fig = plt.figure(num=None, figsize=(5, 5), dpi=80, facecolor='w', edgecolor='k')
        gs = gridspec.GridSpec(1,1)
        graph = plt.subplot(gs[0, 0])
        legends1 = {metric:[]}
        legends2 = {metric:[]}
        score={}
        for strat in ["passive"]:
            score[strat]={}
            score[strat][100]={}
            score[strat][100][0]=[]
            score[strat][100][20]=[]
            score[strat][25]={}
            score[strat][25][0]=[]
            score[strat][25][20]=[]
            for scale in (1,2,3,4,5):
                self.clear()
                self.read_log_domain(strategy=strat, scale=scale)
                for fluent in (100,25):
                    for noise in (0,20):
                        score[strat][fluent][noise].append(100*mean(self.data_domain[metric][fluent][noise]))
        sc=1
        for fluent in [100,25]:
            for noise in [0,20]:    
                for strat in ["passive"]:
                    x = np.array((1,2,3,4,5))
                    l1=plot(graph, x, score[strat][fluent][noise], \
                     metric,m=markers[sc], c=colors[sc])
                    legends1[metric].append(l1)
                    legends2[metric].append(leg[sc])
                graph.legend(legends1[metric], legends2[metric])
                sc += 1
        fig.savefig("%s_scale_%s_%s.pdf" % (repsave, domain, metric))
