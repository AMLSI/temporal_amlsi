/**
 * 
 * This package contains all classes implementing baseline algorithm
 * @author Maxence Grand
 *
 */
package baseline;