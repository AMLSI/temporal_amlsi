/**
 * 
 * This package contains all classes and interfaces used to implement
 * automata
 * 
 * @author Maxence Grand
 *
 */
package fsm;
