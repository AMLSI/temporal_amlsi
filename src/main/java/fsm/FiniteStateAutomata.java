package fsm;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Queue;
import exception.BlocException;
/**
 * This class represent a finite state automaton
 * @author Maxence Grand
 */
public class FiniteStateAutomata{
    /**
     * the Alphabet
     */
    private Alphabet sigma;
    /**
     * The set of State
     */
    private List<Integer> Q;
    /**
     * The set of Final state
     */
    private List<Integer> F;
    /**
     * The initial state
     */
    private int q0;
    /**
     * The set of Transition
     */
    private Map<Integer, Map<Symbol, List<Integer>>> delta;
    /**
     * The set of bloc transition
     */
    private Map<Bloc, Map<Symbol, List<Bloc>>> deltaBloc;
    //private List<Pair<Pair<Bloc, Symbol>, Bloc>> deltaBloc;
    /**
     * The bloc's partition
     */
    private Partition partition;

    /**
     * Constructs an automaton
     */
    public FiniteStateAutomata(){
        this.delta = new HashMap<>();
        this.deltaBloc = new HashMap<>();
        this.Q = new ArrayList<>();
        this.F = new ArrayList<>();
        partition = new Partition();
    }

    /**
     * Constructs an automaton from an other automaton
     * @param other an automaton
     * @throws exception.BlocException
     */
    public FiniteStateAutomata(FiniteStateAutomata other)throws BlocException{
        this();
        this.setQ0(other.getQ0());
        for(int q : other.getQ()){
            this.getQ().add(q);
        }
        for(int q : other.getF()){
            this.getF().add(q);
        }
        Alphabet a = new Alphabet(other.getSigma().getSymboles());
        this.setSigma(a);
        partition = other.partition.clone();
        other.delta.forEach((q,v) -> {
        	v.forEach((sym,l) -> {
        		l.forEach(qPrime -> {
        			try {
						this.addTransition(new Pair<>(q,sym), qPrime);
					} catch (BlocException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.exit(1);
					}
        		});
        	});
        });
    }

    /**
     * Clone the automaton
     * @return An automaton
     */
    @Override
    public FiniteStateAutomata clone(){
        try{
            return new FiniteStateAutomata(this);
        }catch(BlocException e){
            e.printStackTrace();
            throw new RuntimeException("impossible Clone");
        }
    }

    /**
     * The setter of the partition
     * @param p A partition
     */
    public void setPartition(Partition p){
        partition = p;
    }

    /**
     * The getter of the partition
     * @return A partition
     */
    public Partition getPartition(){
        return partition;
    }

    /**
     *  Return the state resulting of &#947; (q,s)
     * @param q The state
     * @param s The symbol
     * @return A state if &#947; (b,s) exists, -1 otherwise
     */
    public int getTransition(Integer q, Symbol s){
    	if(this.delta.containsKey(q)) {
    		if(this.delta.get(q).containsKey(s)) {
    			return this.delta.get(q).get(s).get(0);
    		}
    	}
        return -1;
    }

    /**
     * Return the bloc resulting of &#947; (b,s)
     * @param b The bloc
     * @param s The symbol
     * @return A bloc if &#947; (b,s) exists, a bloc BlocException otherwise
     * @throws BlocException
     */
    public Bloc getBlocTransition(Bloc b, Symbol s)throws BlocException{
    	if(this.deltaBloc.containsKey(b)) {
    		if(this.deltaBloc.get(b).containsKey(s)) {
    			return this.deltaBloc.get(b).get(s).get(0);
    		} else {
    			throw new BlocException();
    		}
    	} else {
    		throw new BlocException();
    	}
    }

    /**
     * All transitions feasible from state q
     * @param q A state
     * @return A list of outgoing actions
     */
    public List<Symbol> getPossibleTransition(int q){
        List<Symbol> trans = new ArrayList<>();
        if(this.delta.containsKey(q)) {
    		trans.addAll(this.delta.get(q).keySet());
    	}
        return trans;
    }

    /**
     * All transitions feasible from bloc where is the state q
     * @param q A state
     * @return A list of outgoing actions
     * @throws BlocException
     */
    public List<Symbol> getPossibleBlocTransition(int q)throws BlocException{
        Bloc bloc = partition.getBloc(q);
        List<Symbol> trans = new ArrayList<>();
        if(this.deltaBloc.containsKey(bloc)) {
        	trans.addAll(this.deltaBloc.get(bloc).keySet());
        }
        return trans;
    }

    /**
     * Add a new transition &#947;;(p.X, p.Y) : i
     * @param p &#947;; input
     * @param i &#947;; output
     * @throws BlocException
     * @see fsm.Pair
     */
    public void addTransition(Pair<Integer,Symbol> p, Integer i)
        throws BlocException{
        if(! this.delta.containsKey(p.getX())) {
        	this.delta.put(p.getX(), new HashMap<>());
        }
        if(! this.delta.get(p.getX()).containsKey(p.getY())) {
        	this.delta.get(p.getX()).put(p.getY(), new ArrayList<>());
        }
        if(! this.delta.get(p.getX()).get(p.getY()).contains(i)) {
        	this.delta.get(p.getX()).get(p.getY()).add(i);
        }

        Bloc b1 = partition.getBloc(p.getX());
        if(! this.deltaBloc.containsKey(b1)) {
        	this.deltaBloc.put(b1, new HashMap<>());
        }
        if(! this.deltaBloc.get(b1).containsKey(p.getY())) {
        	this.deltaBloc.get(b1).put(p.getY(), new ArrayList<>());
        }
        Bloc b2 = partition.getBloc(i);
        if(! this.deltaBloc.get(b1).get(p.getY()).contains(b2)) {
        	this.deltaBloc.get(b1).get(p.getY()).add(b2);
        }
    }

    /**
     * Remove the transition &#947;;(p.X, p.Y) : i
     * @param p &#947;; input
     * @param i &#947;; output
     * @throws BlocException
     * @see fsm.Pair
     */
    public void delTransition(Pair<Integer,Symbol> p, Integer i)
        throws BlocException{
    	if(this.delta.containsKey(p.getX())) {
    		if(this.delta.get(p.getX()).containsKey(p.getY())) {
    			this.delta.get(p.getX()).get(p.getY()).remove(i);
    			if(this.delta.get(p.getX()).get(p.getY()).isEmpty()) {
    				this.delta.get(p.getX()).remove(p.getY());
    			}
    		}
    	}
        Bloc b1 = partition.getBloc(p.getX());
        Bloc b2 = partition.getBloc(i);
        if(this.deltaBloc.containsKey(b1)) {
    		if(this.deltaBloc.get(b1).containsKey(p.getY())) {
    			this.deltaBloc.get(b1).get(p.getY()).remove(b2);
    			if(this.deltaBloc.get(b1).get(p.getY()).isEmpty()) {
    				this.deltaBloc.get(b1).remove(p.getY());
    			}
    		}
    	}
    }

    /**
     * Check if the transition &#947;;(p.X, p.Y) : i exists
     * @param p &#947;; input
     * @param i &#947;; output
     * @return True if &#947;;(p.X, p.Y) : i exists
     * @see fsm.Pair
     */
    public boolean containsTransition(Pair<Integer,Symbol> p, Integer i){
    	if(this.delta.containsKey(p.getX())) {
    		if(this.delta.get(p.getX()).containsKey(p.getY())) {
    			if(this.delta.get(p.getX()).get(p.getY()).contains(i)) {
    				return true;
    			}
    		}
    	}
        return false;
    }

    /**
     * Check if there exists at least one transition delta(p.X, p.Y) : i
     * @param p Delta input
     * @return True if there exists at least one transition delta(p.X, p.Y) : i
     * @see fsm.Pair
     */
    public boolean containsTransition(Pair<Integer,Symbol> p){
    	if(this.delta.containsKey(p.getX())) {
    		if(this.delta.get(p.getX()).containsKey(p.getY())) {
    			return true;
    		}
    	}
        return false;
    }

    /**
     * Check if there exists at least one transition &#947;;Bloc(p.X, p.Y) : i
     * @param p &#947;; input
     * @return True if there exists at least one transition
     * &#947;;Bloc(p.X, p.Y) : i
     * @see fsm.Pair
     */
    public boolean containsBlocTransition(Pair<Bloc,Symbol> p){
        if(this.deltaBloc.containsKey(p.getX())) {
        	if(this.deltaBloc.get(p.getX()).containsKey(p.getY()))  {
        		return true;
        	} else {
        		return false;
        	}
        } else {
        	return false;
        }
    }

    /**
     * Return the set of state transitions
     * @return &#947;;
     */
    public Map<Integer, Map<Symbol, List<Integer>>> getDelta(){
        return this.delta;
    }

    /**
     * Return the set of bloc transitions
     * @return &#947;;Bloc
     */
    public Map<Bloc, Map<Symbol, List<Bloc>>> getDeltaBloc(){
        return this.deltaBloc;
    }

    /**
     * Set the set of states
     * @param Q The new set of states
     */
    public void setQ(List<Integer> Q){
        this.Q.clear();
        for(int q : Q){
            this.Q.add(q);
        }
    }

    /**
     * Set the set of final states
     * @param F The new set of final states
     */
    public void setF(List<Integer> F){
        this.F.clear();
        for(int q : F){
            this.F.add(q);
        }
    }

    /**
     * Return the set of states
     * @return Q
     */
    public List<Integer> getQ(){
        return this.Q;
    }

    /**
     * Return the set of final states
     * @return F
     */
    public List<Integer> getF(){
        return this.F;
    }

    /**
     * Set the initial state
     * @param q0 The new initial state
     */
    public void setQ0(int q0){
        this.q0 = q0;
    }

    /**
     * Return the initial state
     * @return q0
     */
    public int getQ0(){
        return this.q0;
    }

    /**
     * Set the alphabet
     * @param a The new alphabet
     */
    public void setSigma(Alphabet a){
        this.sigma = a;
    }

    /**
     * Return the alphabet
     * @return Sigma
     */
    public Alphabet getSigma(){
        return this.sigma;
    }

    /**
     * Write the automaton in file with graphviz format
     * @param file The file the automaton is written
     * @throws BlocException
     */
    public void writeDotFile(String file)throws BlocException{
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));

            bw.write("digraph fsa{\n");
            bw.write("rankdir=LR\n");
            bw.write("graph [bgcolor=white];\n");
            bw.write("start [shape=none]\n");
            bw.write("node [shape = doublecircle];");
            for(int q : this.F){
                bw.write(" "+partition.getBloc(q).min());
            }
            bw.write(";\n");
            if(Q.size() > F.size()){
                bw.write("node [shape = circle];");
                for(int q : this.Q){
                    if(!this.F.contains(q)){
                        bw.write(" "+partition.getBloc(q).min());
                    }
                }
                bw.write(";\n");
            }
            bw.write("start->"+partition.getBloc(this.q0).min()+"\n");
            this.deltaBloc.forEach((b1,v) -> {
            	v.forEach((s,l) -> {
            		l.forEach(b2 -> {
            			String str = "";
                		str += b1.min()+" -> "+b2.min();
                		str += "[label = \""+s.toString()+"\"]";
                		str+="\n";
                		try {
							bw.write(str);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.exit(1);
						}
            		});
            	});
            });
            bw.write("}\n");
            bw.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Check if a word is accepted by the ie all action are accepted starting
     * from q0
     * @param word The word to check
     * @return True if the word is accepted
     * @throws BlocException
     */
    public boolean accept(Example word)throws BlocException{
        Bloc q = partition.getBloc(q0);
        for(Symbol s : word.getActionSequences()){
            if(containsBlocTransition(new Pair<>(q,s))){
                q = getBlocTransition(q,s);
            }else{
                return false;
            }
        }
        boolean b = false;
        for(int s : q.getStates()){
            b |= this.F.contains(s);
        }
        return b;
    }

    /**
     * Check if all words in the sample I are accepted
     * @param I The sample to check
     * @return True if all words in the sample I are accepted
     * @throws BlocException
     */
    public boolean acceptAll(Sample I)throws BlocException{
        boolean b = true;
        for(Example example : I.getExamples()){
            //System.out.println(example+" "+accept(example));
            b &= accept(example);
            if(!b) {
            	return b;
            }
        }
        return b;
    }

    /**
     * Check if at least one word in the sample I is accepted
     * @param I The sample to check
     * @return True if at least one word in the sample I is accepted
     * @throws BlocException
     */
    public boolean acceptOneOf(Sample I)throws BlocException{

    	boolean b = false;
        for(Example example : I.getExamples()){
            b |= accept(example);
            if(b) {
            	return b;
            }
        }
        return b;
    }

    /**
     * Determinize the automaton ie merge all states leading to a
     * undeterministic behaviour
     * @return The new non-deterministic Partition
     * @throws BlocException
     */
     public Partition determinization() throws BlocException{
        FiniteStateAutomata fsa = clone();
        for(Bloc b : this.partition.getBlocs()){
            for(Symbol s : this.getPossibleBlocTransition(b.min())){
            	List<Integer> statesToMerge = new ArrayList<>();

            	//Search state causing non-determinism
                if(this.deltaBloc.containsKey(b)) {
                	if(this.deltaBloc.get(b).containsKey(s)) {
                		for(Bloc b2 : this.deltaBloc.get(b).get(s)) {
                			statesToMerge.add(b2.min());
                		}
                	}
                }

                //Merge states responsible of the non-determinism
                if(statesToMerge.size() > 1){
                    Partition newPi = this.partition.clone();
                    for(int i =1 ; i < statesToMerge.size(); i++){
                        newPi.merge(statesToMerge.get(i-1),
                                    statesToMerge.get(i));
                    }
                    fsa = FSAFactory.derive(fsa, newPi, this.partition);
                    this.delta = new HashMap<>();
                    this.deltaBloc = new HashMap<>();
                    this.Q = new ArrayList<>();
                    this.F = new ArrayList<>();
                    this.partition = new Partition();
                    this.setQ0(fsa.getQ0());
                    for(int q : fsa.getQ()){
                        this.getQ().add(q);
                    }
                    for(int q : fsa.getF()){
                        this.getF().add(q);
                    }
                    Alphabet a = new Alphabet(fsa.getSigma().getSymboles());
                    this.setSigma(a);
                    this.partition = fsa.partition.clone();
                    fsa.delta.forEach((q,v) -> {
                    	v.forEach((sym,l) -> {
                    		l.forEach(qPrime -> {
                    			try {
									this.addTransition(new Pair<>(q,sym), qPrime);
								} catch (BlocException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									System.exit(1);
								}
                    		});
                    	});
                    });
                    return this.determinization();
                }
            }
        }
        return this.partition;
    }

     /**
      * Determinize the automaton ie remove merges leading to a
      * undeterministic behaviour
      * @param pi the previous partition
      * @return The new non-deterministic Partition
      * @throws BlocException
      */
    public Partition fission(Partition pi)
        throws BlocException{
        for(Bloc bloc : pi.getBlocs()){
            //Find the first bloc making undeterminisme
            List<Symbol> list = getPossibleBlocTransition(bloc.min());

            //Find symbol wich makes undeterminism
            Symbol undetSym = null;
            for(Symbol s : list){
                if(this.deltaBloc.get(bloc).get(s).size() > 1){
                    undetSym = s;
                    break;
                }
            }
            if(undetSym == null){
                continue;
            }
            //Search greatest state in the bloc wich produces non determinism
            int j = bloc.min();
            for(int q : bloc.getStates()){
                list = this.getPossibleTransition(q);
                if(list.contains(undetSym) && q > j){
                    j = q;
                }
            }
            this.partition.fission(j);
            FiniteStateAutomata other = this.clone();
            other.getDelta().clear();
            other.getDeltaBloc().clear();
            other.setPartition(this.partition);
            this.delta.forEach((q,v) -> {
            	v.forEach((sym,l) -> {
            		l.forEach(qPrime -> {
            			try {
							other.addTransition(new Pair<>(q,sym), qPrime);
						} catch (BlocException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.exit(1);
						}
            		});
            	});
            });
            this.delta = new HashMap<>();
            this.deltaBloc = new HashMap<>();
            this.Q = new ArrayList<>();
            this.F = new ArrayList<>();
            this.partition = new Partition();
            this.setQ0(other.getQ0());
            for(int q : other.getQ()){
                this.getQ().add(q);
            }
            for(int q : other.getF()){
                this.getF().add(q);
            }
            Alphabet a = new Alphabet(other.getSigma().getSymboles());
            this.setSigma(a);
            this.partition = other.partition.clone();
            other.delta.forEach((q,v) -> {
            	v.forEach((sym,l) -> {
            		l.forEach(qPrime -> {
            			try {
							this.addTransition(new Pair<>(q,sym), qPrime);
						} catch (BlocException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.exit(1);
						}
            		});
            	});
            });
            return this.fission(this.partition);
        }
        return this.partition;
    }

    /**
     * Check if the automaton is deterministic
     * @return True the automaton is deterministic
     * @throws BlocException
     */
    public boolean checkDeterminism()throws BlocException{
        for(Bloc b : this.partition.getBlocs()){
        	int q = b.min();
            for(Symbol s : getPossibleBlocTransition(q)){
                if(this.deltaBloc.get(b).get(s).size() > 1)
                    return false;
            }
        }
        return true;
    }

    /**
     * Extend the PTA
     * @param x The example to add
     * @param pi The PTA
     * @return The new partition
     * @throws BlocException
     */
    public Partition extension(Example x, Partition pi)
        throws BlocException{
        int q = q0;
        int maxQ = this.partition.max();
        int newQidx = 1;
        for(Symbol s : x.getActionSequences()){
        	//System.out.println(this.getNumberTransitions());
            if(getPossibleTransition(q).contains(s)){
                q = getTransition(q,s);
            }else{
                int qNext = maxQ+(newQidx++);
                F.add(qNext);
                Q.add(qNext);
                if(getPossibleBlocTransition(
                            this.partition.getBloc(q).min()).contains(s)){
                    Bloc b = getBlocTransition(this.partition.getBloc(q),s);
                    b.addState(qNext);
                    //System.out.println("Addition Bloc");
                }else{
                    partition.getBlocs().add(new Bloc(qNext));
                }
                addTransition(new Pair<>(q,s), qNext);
                //System.out.println("Addition "+this.getNumberTransitions());
                q = qNext;
            }
        }
        return this.partition;
    }

    /**
     * String representation of the automaton
     * @return A string
     */
    @Override
    public String toString(){
        StringBuilder res = new StringBuilder("");
        res.append("pi = "+partition+"\n");
        res.append("Q = "+Q+"\n");
        res.append("F = "+F+"\n");
        res.append("q0 = "+q0+"\n");
        res.append("delta = \n");
        this.delta.forEach((q,v) -> {
        	v.forEach((sym,l) -> {
        		l.forEach(qPrime -> {
        			res.append("\t("+q+","+sym+") -> "+qPrime+"\n");
				});
        	});
        });
        return res.toString();
    }

    /**
     * Check if the automaton accept suffixes suf from the state q
     * @param q The state
     * @param suf The set of suffix
     * @return True if the automaton accept suffixes suf from the state q
     * @throws BlocException
     */
    public boolean acceptSuffix(int q, List<Symbol> suf)throws BlocException{
        Queue<Symbol> s = new LinkedList<>();
        Bloc b =  partition.getBloc(q);
        for(Symbol sym : suf){
            s.offer(sym);
        }

        while(! s.isEmpty()){
            Symbol sym = s.poll();
            List<Symbol> p = getPossibleBlocTransition(b.min());
            if(! p.contains(sym)){
                return false;
            }
            b = getBlocTransition(b, sym);
        }
        boolean bool = false;
        for(int state : b.getStates()){
            bool |= this.F.contains(state);
        }
        return bool;
    }

    /**
     * Return all bloc states ie minimal states of each blocs
     * @return  set of states
     */
    public List<Integer> blocStates(){
        List<Integer> res = new ArrayList<>();
        for(Bloc b : partition.getBlocs()) {
            res.add(b.min());
        }
        return res;
    }


    /**
     * The number of transitions in the automaton
     * @return The number of transitions
     */
    public int getNumberTransitions() {
    	int res=0;
    	for(Bloc b : partition.getBlocs()) {
    		try {
				res += this.getPossibleBlocTransition(b.min()).size();
			} catch (BlocException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
    	}
    	return res;
    }

    /**
     * Return all transitions for an action
     * @param a The action
     * @return All Transitions
     */
    public List<Pair<Bloc, Bloc>> getAllTransitions(Symbol a) {
    	List<Pair<Bloc, Bloc>> res = new ArrayList<>();

    	return res;
    }
    
    public Sample getRejected(Sample S) {
    	Sample SNew = new Sample();
    	for(Example e: S.getExamples()) {
    		try {
				if(this.accept(e)) {
					System.out.println(e);
				} else {
					SNew.addExample(e);
				}
			} catch (BlocException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    	}
    	return SNew;
    }
}
