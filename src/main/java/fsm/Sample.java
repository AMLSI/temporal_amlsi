package fsm;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 * This class represents a sample
 * @author Maxence Grand
 */
public class Sample{
    /**
     * Set of example
     */
    private List<Example> examples;
	private BufferedReader br;

    /**
     * Constructs an empty sample
     */
    public Sample(){
        this.examples = new ArrayList<>();
    }

    /**
     * Constructs a sample from another sample
     * @param other A sample
     */
    public Sample(Sample other){
        this();
        for(Example sym : other.getExamples()) {
            Example tmp = new Example();
            for(Symbol s : sym.getActionSequences()) {
                tmp.add(s);
            }
            examples.add(tmp);
        }
    }

    /**
     * Clone the sample
     * @return A sample
     */
    public Sample clone() {
        return new Sample(this);
    }

    /**
     * Constructs a sample from a set of examples
     * @param symbols
     */
    public Sample(List<Example> symbols){
        this.examples = new ArrayList<>(symbols.size());
        for(Example s : symbols){
            this.examples.add(s);
        }
    }

    /**
     * Constructs a sample by reading a dataset on the disk
     * @param file The dataset
     * @param actions The set of actions
     */
    public Sample(String file, List<Symbol> actions) {
        this();
        try {
            br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            int i = 0;
            while(line != null) {
                String[] arr = line.split("::");
                this.examples.add(new Example());
                for(int j =0; j<arr.length; j++) {
                    for(Symbol s : actions) {
                        if(s.toString().equals(arr[j])) {
                            this.examples.get(i).add(s);
                        }
                    }

                }
                line = br.readLine();
                i++;
            }
        }catch(IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Get the set of examples
     * @return A set of examples
     */
    public List<Example> getExamples(){
        return this.examples;
    }

    /**
     * String representation of a sample
     * @return A string
     */
    @Override
    public String toString(){
        String res = "";
        for(Example l : this.examples){
            res += "{";
            for(Symbol s : l.getActionSequences()){
                res+=s;
            }
            res += "}\n";
        }
        return res;
    }

    /**
     * Add an example to the sample
     * @param s A new example
     */
    public void addExample(Example s){
        this.examples.add(s);
    }

    /**
     * Add examples of another sample to the sample
     * @param s the sample
     */
    public void add(Sample s) {
        for(Example example : s.examples) {
            addExample(example);
        }
    }

    /**
     * The size of the sample ie the number of example
     * @return The size of the sample
     */
    public int size() {
        return this.examples.size();
    }

    /**
     * The mean size of the sample ie the mean size of each example in the
     * sample
     * @return The mean size of the sample
     */
    public float meanSize() {
        int l = 0;
        for(Example example : examples) {
            l += example.size();
        }
        return (float) l / size();
    }

    /*
     * Check if two examples are equal
     * @param ex1 An example
     * @param ex2 An example
     * @return True if ex1 == ex2
     *
    private static boolean equalsExample(Example ex1,List<Symbol> ex2) {
    	if(ex1.size() != ex2.size()) {
    		return false;
    	}
    	for(int i = 0; i < ex1.size(); i++) {
    		if(!ex1.get(i).equals(ex2.get(i))) {
    			return false;
    		}
    	}
    	return true;
    }*/

    /**
     * Remove an example
     * @param toRemove An Example
     */
    public void removeExample(Example toRemove) {
    	List<Example> newList = new ArrayList<>();
    	this.examples.forEach(ex -> {
    		if(! ex.equals(toRemove)) {
    			newList.add(ex);
    		}
    	});
    	this.examples.clear();
    	this.examples.addAll(newList);
    }
    
    /**
     * 
     * @param e
     * @return
     */
    public boolean contains(Example e) {
    	for(Example ex : this.examples) {
    		if(e.equals(ex)) {
    			return true;
    		}
    	}
    	return false;
    }
}
