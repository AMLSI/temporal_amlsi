/**
 * 
 */
package temporal.planner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.uga.pddl4j.plan.Plan;
import fr.uga.pddl4j.problem.Problem;

/**
 * @author Maxence Grand
 *
 */
public class STN {
	/**
	 * 
	 */
	private float[][] array;
	/**
	 * 
	 */
	private Map<String, Integer> idx;
	/**
	 * 
	 * Constructs 
	 * @param size
	 */
	public STN(int size, Map<String, Integer> idx) {
		this.array = new float[size][size];
		this.array[0][0] = 0;
		for(int i =1; i < size; i++) {
			this.array[0][i] = TemporalHSP.MAX_VALUE;
			this.array[i][0] = -TemporalHSP.TOLERANCE_VALUE;
			
			for(int j =1; j<size; j++) {
				if(i==j) {
					this.array[i][j]=0;
				} else {
					this.array[i][j]=TemporalHSP.MAX_VALUE;
				}
			}
		}
		this.idx = idx;
	}
	
	/**
	 * 
	 */
	public void floydWarshal() {
		for(int k = 0; k<this.array.length; k++) {
			for(int i = 0; i<this.array.length; i++) {
				for(int j = 0; j<this.array.length; j++) {
					if(this.array[i][j] > this.array[i][k]+this.array[k][j]) {
						this.array[i][j] = this.array[i][k]+this.array[k][j];
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @param idx
	 * @return
	 */
	public List<Float> getStartTime(Plan p, Problem problem, Map<String, Float> duration) {
		List<Float> res = new ArrayList<>();
		StringBuilder buildPlan = new StringBuilder();
		
		p.actions().forEach(op -> {
			if(Translator.isStart(op)) {
				String act = Translator.getAction(problem, op);
				//buildPlan.append("("+act+" "+reverseIdx.get(i).split("\\(")[1])
				String durativeAct = "("+Translator.getDurativeActionName(op)+act.split("\\(")[1];
				float timeStamp = this.array[this.idx.get(act)][0];
				timeStamp = timeStamp < 0 ? -timeStamp : timeStamp;
				buildPlan.append(timeStamp).append(": ");
				buildPlan.append(durativeAct);
				buildPlan.append("[").append(duration.get(Translator.getDurativeActionName(op))).append("] ");
				buildPlan.append("\n");
			}
		});

		System.out.println("Found plan as follows:");
		System.out.println(buildPlan.toString());
		return res;
	}
	
	/***
	 * 
	 * @param l
	 */
	public void addConstraint(List<TemporalConstraint> l) {
		l.forEach(c -> this.addConstraint(c));
	}
	
	/**
	 * 
	 * @param c
	 */
	public void addConstraint(TemporalConstraint c) {
		//System.out.println(c.getX());
		//System.out.println(idx.keySet());
		int i = idx.get(c.getX());
		int j = idx.get(c.getY());
		this.array[j][i]=c.getB();
	}
	
	/**
	 * 
	 */
	public void printMatrix() {
		System.out.println("*************");
		for(int i = 0; i<this.array.length; i++) {
			for(int j = 0; j<this.array.length; j++) {
				System.out.print(this.array[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("*************\n");
	}
}
