/**
 * 
 */
package temporal.planner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.Logger;

import fr.uga.pddl4j.heuristics.graph.PlanningGraphHeuristic;
import fr.uga.pddl4j.parser.PDDLAction;
import fr.uga.pddl4j.parser.PDDLParser;
import fr.uga.pddl4j.plan.Plan;
import fr.uga.pddl4j.plan.TemporalPlan;
import fr.uga.pddl4j.planners.statespace.AbstractStateSpacePlanner;
import fr.uga.pddl4j.planners.statespace.search.AStar;
import fr.uga.pddl4j.planners.statespace.search.Node;
import fr.uga.pddl4j.planners.statespace.search.StateSpaceStrategy;
import fr.uga.pddl4j.problem.Action;
import fr.uga.pddl4j.problem.Problem;

/**
 * @author Maxence Grand
 *
 */
public class TemporalHSP extends AbstractStateSpacePlanner {
	/**
	 * 
	 */
	public static final float TOLERANCE_VALUE = 0.002f;
	/**
	 * 
	 */
	public static final float MAX_VALUE = Float.MAX_VALUE;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1611274263171148871L;

	/**
	 * The A* strategy.
	 */
	private final StateSpaceStrategy astar;

	/**
	 * Get Map Operator -> positive preconditions
	 */
	private Map<String,List<String>> preconditionsPos;
	/**
	 * Get Map Operator -> negative preconditions
	 */
	private Map<String,List<String>> preconditionsNeg;
	/**
	 * Get Map Operator -> positive effects
	 */
	private Map<String,List<String>> effectPos;
	/**
	 * Get Map Operator -> negative effects
	 */
	private Map<String,List<String>> effectNeg;
	/**
	 * 
	 */
	private Map<String,Integer> idx;
	/**
	 * 
	 */
	private Map<Integer,String> reverseIdx;

	/**
	 * 
	 */
	private Map<String, Float> durationOp;

	/**
	 * 
	 * @param arg0
	 */
	public void setTimeOut(int arg0) {
		this.astar.setTimeOut(arg0);
	}
	/**
	 * Constructs 
	 */
	public TemporalHSP() {
		super();
		astar = new AStar();
		this.getStateSpaceStrategies().add(astar);
		// TODO Auto-generated constructor stub

		preconditionsPos = new HashMap<>();
		this.preconditionsNeg = new HashMap<>();
		this.effectPos = new HashMap<>();
		this.effectNeg = new HashMap<>();
		this.idx = new HashMap<>();
		this.reverseIdx = new HashMap<>();
		this.durationOp = new HashMap<>();
	}


	/**
	 * Constructs 
	 * @param statisticState
	 * @param traceLevel
	 */
	public TemporalHSP(boolean statisticState, int traceLevel) {
		super(statisticState, traceLevel);
		// TODO Auto-generated constructor stub
		astar = new AStar();
		this.getStateSpaceStrategies().add(astar);

		preconditionsPos = new HashMap<>();
		this.preconditionsNeg = new HashMap<>();
		this.effectPos = new HashMap<>();
		this.effectNeg = new HashMap<>();
		this.idx = new HashMap<>();
		this.reverseIdx = new HashMap<>();
		this.durationOp = new HashMap<>();
	}

	/**
	 * Constructs
	 *
	 * @param timeout        the time out of the planner.
	 * @param heuristicType  the heuristicType to use to solve the problem.
	 * @param weight         the weight set to the heuristic.
	 * @param statisticState the statistics generation value.
	 * @param traceLevel     the trace level of the planner.
	 */
	public TemporalHSP(final int timeout, final PlanningGraphHeuristic.Type heuristicType, 
			final double weight, final boolean statisticState, 
			final int traceLevel) {
		super(statisticState, traceLevel);

		astar = new AStar(timeout, heuristicType, weight);
		this.getStateSpaceStrategies().add(astar);

		preconditionsPos = new HashMap<>();
		this.preconditionsNeg = new HashMap<>();
		this.effectPos = new HashMap<>();
		this.effectNeg = new HashMap<>();
		this.idx = new HashMap<>();
		this.reverseIdx = new HashMap<>();
		this.durationOp = new HashMap<>();
	}

	/**
	 * 
	 * @return
	 */
	public Plan searchTemporal(Problem arg0, File domainTemporal) {
		//Solve classical
		Plan plan = this.search(arg0);
		if (plan != null) {
			plan.actions().forEach(op -> System.out.println(op.toString()));
		} else {
			System.out.println("No plan found.");
			System.exit(1);
		}

		//Convert to Partial order plan
		List<Ordering> PO = this.convertTotalToPartialOrder(arg0, plan);

		//Create STN
		STN stn = new STN(idx.size(), idx);
		//stn.printMatrix();
		List<TemporalConstraint> l = this.createConstraintMatrix(PO, 
				arg0, domainTemporal, idx, plan);
		stn.addConstraint(l);
		stn.floydWarshal();
		stn.getStartTime(plan, arg0, durationOp);
		
		Plan tempPlan = new TemporalPlan();
		return tempPlan;
	}

	/**
	 * 
	 */
	@Override
	public Plan search(final Problem problem) {
		final Logger logger = this.getLogger();
		Objects.requireNonNull(problem);

		logger.trace("* starting A*\n");
		final Node solutionNode = astar.searchSolutionNode(problem);
		if (isSaveState()) {
			this.getStatistics().setTimeToSearch(astar.getSearchingTime());
			this.getStatistics().setMemoryUsedToSearch(astar.getMemoryUsed());
		}
		if (solutionNode != null) {
			logger.trace("* A* succeeded\n");
			return astar.extractPlan(solutionNode, problem);
		} else {
			logger.trace("* A* failed\n");
			return null;
		}
	}

	/**
	 * 
	 * @param problem
	 */
	private void initTables(final Problem problem, Plan plan) {
		for(Action op : problem.getActions()) {
			preconditionsPos.put(
					Translator.getAction(problem,op), new ArrayList<>());
			preconditionsNeg.put(
					Translator.getAction(problem,op), new ArrayList<>());
			effectPos.put(Translator.getAction(problem,op), new ArrayList<>());
			effectNeg.put(Translator.getAction(problem,op), new ArrayList<>());
			for(int i = 0; i<op.getPrecondition().getPositiveFluents().length(); i++) {

				//Get positive preconditions
				if(op.getPrecondition().getPositiveFluents().get(i)) {
					preconditionsPos.get(Translator.getAction(problem,op)).add(
							Translator.getProposition(problem,i));
				}
			}
			for(int i = 0; i<op.getPrecondition().getNegativeFluents().length(); i++) {
				//Get negative preconditions
				if(op.getPrecondition().getNegativeFluents().get(i)) {
					preconditionsNeg.get(Translator.getAction(problem,op)).add(
							Translator.getProposition(problem,i));
				}
			}
			for(int i = 0; i<op.getUnconditionalEffect().getPositiveFluents().length();
					i++) {
				//Get positive effects
				if(op.getUnconditionalEffect().getPositiveFluents().get(i)) {
					effectPos.get(Translator.getAction(problem,op)).add(
							Translator.getProposition(problem,i));
				}
			}
			for(int i = 0; i<op.getUnconditionalEffect().getNegativeFluents().length(); 
					i++) {
				//Get negative effects
				if(op.getUnconditionalEffect().getNegativeFluents().get(i)) {
					effectNeg.get(Translator.getAction(problem,op)).add(
							Translator.getProposition(problem,i));
				}
			}
		}
		int i = 1;
		for(Action op : plan.actions()) {
			if(!Translator.isInvariant(op) &&
					!idx.keySet().contains(Translator.getAction(problem,op))) {
				idx.put(Translator.getAction(problem,op), i);
				reverseIdx.put(i, Translator.getAction(problem,op));
				i++;
			}
		}
		idx.put("initial", 0);
		reverseIdx.put(0, "initial");
	}
	/**
	 * 
	 * @param plan
	 * @param table
	 */
	private List<Ordering> convertTotalToPartialOrder(Problem problem, Plan plan) {
		this.initTables(problem, plan);
		List<String> goal = new ArrayList<>();
		for(int i = 0; i < problem.getGoal().getPositiveFluents().length();i++) {
			if(problem.getGoal().getPositiveFluents().get(i)) {
				goal.add(Translator.getProposition(problem,i));
			}
		}
		List<String> initial = new ArrayList<>();
		for(int i = 0; i < problem.getInitialState().getPositiveFluents().length();i++) {
			if(problem.getInitialState().getPositiveFluents().get(i)) {
				initial.add(Translator.getProposition(problem,i));
			}
		}
		List<Ordering> PO = new ArrayList<>();

		for(int i = plan.actions().size()-1; i>=0; i--) {

			Action opi = plan.actions().get(i);
			String opiStr = Translator.getAction(problem,opi);
			//Preconditions
			for(String prec : preconditionsPos.get(opiStr)) {
				//boolean b = true;
				for(int j = i-1; j >=0; j--) {
					Action opj = plan.actions().get(j);
					String opjStr = Translator.getAction(problem,opj);
					if(effectPos.get(opjStr).contains(prec)) {
						Ordering o = new Ordering(opj,opi,problem,true);
						if(!PO.contains(o)) {
							PO.add(o);
						}
						break;
					}
				}
			}
			//Negative effects
			for(String prec : effectNeg.get(opiStr)) {
				for(int j = i-1; j >=0; j--) {
					Action opj = plan.actions().get(j);
					String opjStr = Translator.getAction(problem,opj);
					if(preconditionsPos.get(opjStr).contains(prec)) {
						Ordering o = new Ordering(opj,opi,problem,true);
						if(!PO.contains(o)) {
							PO.add(o);
						}
					}
				}
				if(initial.contains(prec)) {
					Ordering o = new Ordering(opi,problem,false);
					if(!PO.contains(o)) {
						PO.add(o);
					}
				}
			}
			//Primary add
			for(String prec : effectPos.get(opiStr)) {
				if(goal.contains(prec)) {
					for(int j =i-1; j >=0; j--) {
						Action opj = plan.actions().get(j);
						String opjStr = Translator.getAction(problem,opj);
						if(effectNeg.get(opjStr).contains(prec)) {
							Ordering o = new Ordering(opj,opi,problem,true);
							if(!PO.contains(o)) {
								PO.add(o);
							}
						}
					}
				}
			}

		}
		PO = Ordering.TransitiveClosure(PO, idx);
		return PO;
	}

	/**
	 * 
	 * @param temporalDomain
	 * @param problem
	 */
	private void initConstraint(File temporalDomain,Problem problem) {
		PDDLParser parser = new PDDLParser();
		try {
			parser.parseDomain(temporalDomain);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
		//Get all durative operators
		List<PDDLAction> operators = parser.getDomain().getActions();
		//Get all duration
		operators.forEach(op -> {
			durationOp.put(op.getName().toString(),
					op.getDuration().getChildren().get(1).getValue().floatValue());
		});
	}

	/**
	 * 
	 * @param A
	 * @param problem
	 * @param temporalDomain
	 * @return
	 */
	private List<TemporalConstraint> createConstraintMatrix(List<Ordering> A, Problem problem, 
			File temporalDomain, Map<String, Integer> table, Plan plan) {
		this.initConstraint(temporalDomain, problem);
		List<TemporalConstraint> l = new ArrayList<>();
		A.forEach(o -> {
			if(o.isStrict()) {
				l.add(TemporalConstraint.getConstraint(o.getAi(), o.getAj()));
			}
		});
		A.forEach(o -> {
			if(!o.isStrict()) {
				l.add(TemporalConstraint.getConstraintEqual(o.getAi(), o.getAj()));
			}
		});
		plan.actions().forEach(act -> {
			if(Translator.isStart(act)) {
				String opi = Translator.getAction(problem, act);
				String opj = Translator.getDurativeActionName(act)+"-end"
										+Translator.getParameters(act, problem);
				l.addAll(TemporalConstraint.getExactly(opi, opj, durationOp.get(Translator.getDurativeActionName(act))));
			}
		});
		return l;
	}

}
