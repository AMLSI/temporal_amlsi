/**
 * 
 */
package temporal.planner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maxence Grand
 *
 */
public class TemporalConstraint {
	/**
	 * 
	 */
	private String x, y;
	/**
	 * 
	 */
	private float b;

	/**
	 * 
	 * Constructs 
	 * @param X
	 * @param Y
	 * @param B
	 */
	public TemporalConstraint(String X, String Y, float B) {
		x = X;
		y = Y;
		b = B;
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static TemporalConstraint getConstraint(String first,String second) {
		return new TemporalConstraint(first, second, -TemporalHSP.TOLERANCE_VALUE);
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static TemporalConstraint getConstraintEqual(String first,
			String second){
		return new TemporalConstraint(first, second, 0);
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @param max
	 * @return
	 */
	public static TemporalConstraint getConstraintMax(String first,
			String second, float max) {
		return new TemporalConstraint(second, first, max);
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @param min
	 * @return
	 */
	public static TemporalConstraint getConstraintMin(String first,
			String second, float min) {
		return new TemporalConstraint(first, second, -min);
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @param value
	 * @return
	 */
	public static List<TemporalConstraint> getExactly(String first, String second,
			float value) {
		List<TemporalConstraint> rList = new ArrayList<>();
		rList.add(getConstraintMax(first, second, value));
		rList.add(getConstraintMin(first, second, value));
		return rList;
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @param max
	 * @param min
	 * @return
	 */
	public static List<TemporalConstraint> getBounds(String first, String second,
			float max, float min) {
		List<TemporalConstraint> rList = new ArrayList<>(2);
		rList.add(getConstraintMax(first, second, max));
		rList.add(getConstraintMin(first, second, min));
		return rList;
	}
	
	/**
	 * Getter of x
	 * @return the x
	 */
	public String getX() {
		return x;
	}

	/**
	 * Setter x
	 * @param x the x to set
	 */
	public void setX(String x) {
		this.x = x;
	}

	/**
	 * Getter of y
	 * @return the y
	 */
	public String getY() {
		return y;
	}

	/**
	 * Setter y
	 * @param y the y to set
	 */
	public void setY(String y) {
		this.y = y;
	}

	/**
	 * Getter of b
	 * @return the b
	 */
	public float getB() {
		return b;
	}

	/**
	 * Setter b
	 * @param b the b to set
	 */
	public void setB(float b) {
		this.b = b;
	}

	/**
	 * 
	 */
	public String toString() {
		return (x.toString() + " - " + y.toString() + " <= " + b);
	}

	/**
	 * 
	 */
	public boolean equals(Object obj) {
		if (obj instanceof TemporalConstraint)
		{
			TemporalConstraint c = (TemporalConstraint) obj;
			return (c.x.equals(x) && c.y.equals(y) && c.b == b);
		}
		return false;
	}

	/**
	 * 
	 */
	public int hashCode() {
		int hash = 2;
		hash = 31 * hash ^ x.hashCode();
		hash = 31 * hash ^ y.hashCode();
		hash = 31 * hash ^ (int)b;
		return hash;
	}
}
