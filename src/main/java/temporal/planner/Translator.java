/**
 * 
 */
package temporal.planner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fr.uga.pddl4j.parser.PDDLAction;
import fr.uga.pddl4j.parser.PDDLExpression;
import fr.uga.pddl4j.parser.PDDLParser;
import fr.uga.pddl4j.parser.PDDLTypedSymbol;
import fr.uga.pddl4j.problem.Action;
import fr.uga.pddl4j.problem.Problem;
import fsm.Pair;
import learning.TypeHierarchy;

/**
 * @author Maxence Grand
 *
 */
public class Translator {

	/**
	 * 
	 * @param temporal
	 * @param filename
	 * @return
	 */
	public static File translate(File temporal, String filename) {
		//Parse temporal domain
		PDDLParser parser = new PDDLParser();
		try {
			parser.parseDomain(temporal);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		//Get all operators
		List<PDDLAction> operators = parser.getDomain().getActions();

		//Map durative operators' name with classical operators' name
		Map<String, List<String>> opNames = new HashMap<>();
		for(PDDLAction op : operators) {
			String str = op.getName().toString();
			List<String> newOp = new ArrayList<>();
			newOp.add(str+"-start");
			newOp.add(str+"-inv");
			newOp.add(str+"-end");
			opNames.put(str, newOp);
		}

		//Get preconditions
		Map<String, List<String>> startPreconditions = new HashMap<>();
		Map<String, List<String>> endPreconditions = new HashMap<>();
		Map<String, List<String>> overAllPreconditions = new HashMap<>();
		for(PDDLAction op : operators) {
			String name = op.getName().toString();

			startPreconditions.put(name, new LinkedList<>());
			endPreconditions.put(name, new LinkedList<>());
			overAllPreconditions.put(name, new LinkedList<>());

			for(PDDLExpression exp : op.getPreconditions().getChildren()) {
				switch(exp.getConnective().getImage()) {
				case "at start":
					startPreconditions.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				case "at end":
					endPreconditions.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				case "over all":
					overAllPreconditions.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				}
			}
		}

		//Get effects
		Map<String, List<String>> startEffects = new HashMap<>();
		Map<String, List<String>> endEffects = new HashMap<>();
		for(PDDLAction op : operators) {
			String name = op.getName().toString();

			startEffects.put(name, new LinkedList<>());
			endEffects.put(name, new LinkedList<>());

			for(PDDLExpression exp : op.getEffects().getChildren()) {
				switch(exp.getConnective().getImage()) {
				case "at start":
					startEffects.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				case "at end":
					endEffects.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				}
			}
		}

		//Get preconditions and effects of classical domain
		Map<String, Pair<List<String>, List<String>>> classical = 
				new HashMap<>();
		for(String str : opNames.keySet()) {
			//str-start action
			//str-start precondition
			List<String> l1 = new LinkedList<>();
			l1.addAll(startPreconditions.get(str));
			l1.addAll(overAllPreconditions.get(str));
			//str-start effect
			List<String> l2 = new LinkedList<>();
			l2.addAll(startEffects.get(str));
			classical.put(opNames.get(str).get(0), new Pair<>(l1,l2));

			//str-end action
			//str-end precondition
			List<String> l3 = new LinkedList<>();
			l3.addAll(endPreconditions.get(str));
			l3.addAll(overAllPreconditions.get(str));
			//str-start effect
			List<String> l4 = new LinkedList<>();
			l4.addAll(endEffects.get(str));
			classical.put(opNames.get(str).get(2), new Pair<>(l3,l4));
			
			//str-in action
			//str-inv precondition
			List<String> l5 = new LinkedList<>();
			l5.addAll(overAllPreconditions.get(str));
			classical.put(opNames.get(str).get(1), new Pair<>(l5,new ArrayList<>()));
		}

		//Get Type Hierarchy
		Map<String, String> hier = new HashMap<>();
		for(PDDLTypedSymbol t : parser.getDomain().getTypes()) {
			if(!t.getTypes().isEmpty()) {
				hier.put(t.getImage().toLowerCase(),
						t.getTypes().get(0).getImage().toLowerCase());
			}
		}
		TypeHierarchy types = new TypeHierarchy(hier);

		//Write Classical domain
		StringBuilder str = new StringBuilder();
		str.append("(define (domain "+parser.getDomain().getName().getImage()+")\n");
		str.append("(:requirements :strips :typing :negative-preconditions)\n");
		str.append("(:types \n"+types+")");
		str.append("(:predicates\n");
		parser.getDomain().getPredicates().forEach(p -> {
			str.append("\t"+p.toString()+"\n");
		});
		parser.getDomain().getActions().forEach(op -> {
			String inv = "("+op.getName().toString()+"-inv";
			String iinv = "(i"+op.getName().toString()+"-inv";
			for(PDDLTypedSymbol p : op.getParameters()) {
				inv += (" "+p.toString());
				iinv += (" "+p.toString());
			}
			inv+=")";
			iinv+=")";
			str.append("\t"+inv+"\n\t"+iinv+"\n");
		});
		str.append(")\n");
		parser.getDomain().getActions().forEach(op -> {
			List<String> actions = opNames.get(op.getName().toString());
			//Start action
			str.append("(:action "+actions.get(0)+"\n");
			str.append("\t:parameters (");
			StringBuilder paramInv = new StringBuilder();
			paramInv.append(op.getName().toString());
			paramInv.append("-inv");
			op.getParameters().forEach(p -> {
				str.append(" "+op.getParameter(p));
				paramInv.append(" "+p.getImage());
			});
			paramInv.append(")");
			//System.out.println(paramInv);
			str.append(" )\n");
			str.append("\t:precondition (and\n");
			classical.get(actions.get(0)).getX().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t)\n");
			str.append("\t:effect (and\n");
			classical.get(actions.get(0)).getY().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t\t("+paramInv+""+"\n");
			str.append("\t)\n)\n");
			//End action
			str.append("(:action "+actions.get(2)+"\n");
			str.append("\t:parameters (");
			op.getParameters().forEach(p -> {
				str.append(" "+op.getParameter(p));
			});
			str.append(" )\n");
			str.append("\t:precondition (and\n");
			classical.get(actions.get(2)).getX().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t\t("+paramInv+""+"\n");
			str.append("\t\t("+"i"+paramInv+""+"\n");
			str.append("\t)\n");
			str.append("\t:effect (and\n");
			classical.get(actions.get(2)).getY().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t\t(not ("+paramInv+")"+"\n");
			str.append("\t\t(not ("+"i"+paramInv+")"+"\n");
			str.append("\t)\n)\n");
			//Invariant action
			str.append("(:action "+actions.get(1)+"\n");
			str.append("\t:parameters (");
			op.getParameters().forEach(p -> {
				str.append(" "+op.getParameter(p));
			});
			str.append(" )\n");
			str.append("\t:precondition (and\n");
			classical.get(actions.get(1)).getX().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t\t("+paramInv+""+"\n");
			str.append("\t)\n");
			str.append("\t:effect (and\n");
			classical.get(actions.get(1)).getY().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t\t("+paramInv+""+"\n");
			str.append("\t\t("+"i"+paramInv+""+"\n");
			str.append("\t)\n)\n");
		});
		str.append(")");

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));

			bw.write(str.toString());
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		File file = new File(filename);
		return file;
	}
	
	/**
	 * 
	 * @param pb
	 * @param op
	 * @return
	 */
	public static String getAction(Problem pb, Action op) {
		StringBuilder str = new StringBuilder();
		str.append(op.getName());
		str.append("(");
		for(int i = 0; i < op.arity(); i++) {
			str.append(" "+pb.getConstantSymbols().get(op.getValueOfParameter(i)));
		}
		str.append(")");
		return str.toString();
	}

	/**
	 * 
	 * @param pb
	 * @param idx
	 * @return
	 */
	public static String getProposition(Problem pb, int idx) {
		StringBuilder str = new StringBuilder();
		int idxPred = pb.getRelevantFluents().get(idx).getSymbol();
		int[] idxParam = pb.getRelevantFluents().get(idx).getArguments();
		str.append("(").append(pb.getPredicateSymbols().get(idxPred));
		for(int param : idxParam) {
			str.append(" ").append(pb.getConstantSymbols().get(param));
		}
		str.append(")");
		return str.toString();
	}

	/**
	 * 
	 * @param op
	 * @return
	 */
	public static boolean isInvariant(Action op) {
		String str = op.getName();
		String[] str2 = str.split("-");
		if(str2[str2.length-1].equals("inv")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param op
	 * @return
	 */
	public static boolean isStart(Action op) {
		String str = op.getName();
		String[] str2 = str.split("-");
		if(str2[str2.length-1].equals("start")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param op
	 * @return
	 */
	public static boolean isEnd(Action op) {
		String str = op.getName();
		String[] str2 = str.split("-");
		if(str2[str2.length-1].equals("end")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	public static String getParameters(Action a, Problem pb) {
		StringBuilder str = new StringBuilder();
		str.append("(");
		for(int i = 0; i < a.arity(); i++) {
			str.append(" "+pb.getConstantSymbols().get(a.getValueOfParameter(i)));
		}
		str.append(")");
		return str.toString();
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	public static String getDurativeActionName(Action a) {
		String res = "";
		String t[] = a.getName().split("-");
		if(t.length < 2) {
			return a.getName();
		}
		res=a.getName();
		res=res.substring(0, res.length() - (t[t.length-1].length()+1));
		return res;
	}
}
