package temporal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import fr.uga.pddl4j.parser.PDDLAction;
import fr.uga.pddl4j.parser.PDDLExpression;
import fr.uga.pddl4j.parser.PDDLParser;
import fr.uga.pddl4j.parser.PDDLTypedSymbol;
import fsm.Pair;
import java.util.List;
import learning.TypeHierarchy;

/**
 * @author Maxence Grand
 *
 * This class contains static method for the conversion of temporal and
 * classical PDDL domains
 * We assume that only the following requirements are presents:
 * <ol>
 * <li> :strips </li> 
 * <li> :types </li>
 * <li> :negative-preconditions </li>
 * <li> :durative-actions (only for temporal domains) </li> 
 * </ol>
 */
public class DomainConverter {

	/**
	 * 
	 * @param temporal The temporal domain to convert
	 * @param filename The name of the file where the classical domain is wrote
	 * @return The classical domain
	 */
	public static File convertTemporalToClassical(File temporal, 
			String filename) {
		//Parse temporal domain
		PDDLParser parser = new PDDLParser();
		try {
			parser.parseDomain(temporal);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		//Get all operators
		List<PDDLAction> operators = parser.getDomain().getActions();

		//Map durative operators' name with classical operators' name
		Map<String, Pair<String, String>> opNames = new HashMap<>();
		for(PDDLAction op : operators) {
			String str = op.getName().toString();
			opNames.put(str, new Pair<>(str+"-start", str+"-end"));
		}

		//Get preconditions
		Map<String, List<String>> startPreconditions = new HashMap<>();
		Map<String, List<String>> endPreconditions = new HashMap<>();
		Map<String, List<String>> overAllPreconditions = new HashMap<>();
		for(PDDLAction op : operators) {
			String name = op.getName().toString();

			startPreconditions.put(name, new LinkedList<>());
			endPreconditions.put(name, new LinkedList<>());
			overAllPreconditions.put(name, new LinkedList<>());

			for(PDDLExpression exp : op.getPreconditions().getChildren()) {
				switch(exp.getConnective().getImage()) {
				case "at start":
					startPreconditions.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				case "at end":
					endPreconditions.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				case "over all":
					overAllPreconditions.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				}
			}
		}

		//Get effects
		Map<String, List<String>> startEffects = new HashMap<>();
		Map<String, List<String>> endEffects = new HashMap<>();
		for(PDDLAction op : operators) {
			String name = op.getName().toString();

			startEffects.put(name, new LinkedList<>());
			endEffects.put(name, new LinkedList<>());

			for(PDDLExpression exp : op.getEffects().getChildren()) {
				switch(exp.getConnective().getImage()) {
				case "at start":
					startEffects.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				case "at end":
					endEffects.get(name).add(
							exp.getChildren().get(0).toString());
					break;
				}
			}
		}

		//Get preconditions and effects of classical domain
		Map<String, Pair<List<String>, List<String>>> classical = 
				new HashMap<>();
		for(String str : opNames.keySet()) {
			//str-start action
			//str-start precondition
			List<String> l1 = new LinkedList<>();
			l1.addAll(startPreconditions.get(str));
			l1.addAll(overAllPreconditions.get(str));
			//str-start effect
			List<String> l2 = new LinkedList<>();
			l2.addAll(startEffects.get(str));
			classical.put(opNames.get(str).getX(), new Pair<>(l1,l2));

			//str-end action
			//str-end precondition
			List<String> l3 = new LinkedList<>();
			l3.addAll(endPreconditions.get(str));
			l3.addAll(overAllPreconditions.get(str));
			//str-start effect
			List<String> l4 = new LinkedList<>();
			l4.addAll(endEffects.get(str));
			classical.put(opNames.get(str).getY(), new Pair<>(l3,l4));
		}

		//Get Type Hierarchy
		Map<String, String> hier = new HashMap<>();
		for(PDDLTypedSymbol t : parser.getDomain().getTypes()) {
			if(!t.getTypes().isEmpty()) {
				hier.put(t.getImage().toLowerCase(),
						t.getTypes().get(0).getImage().toLowerCase());
			}
		}
		//System.out.println(parser.getDomain().toString());
		TypeHierarchy types = new TypeHierarchy(hier);

		//Write Classical domain
		StringBuilder str = new StringBuilder();
		str.append("(define (domain "+parser.getDomain().getName().getImage()+")\n");
		str.append("(:requirements :strips :typing :negative-preconditions)\n");
		str.append("(:types \n"+types+")");
		str.append("(:predicates\n");
		parser.getDomain().getPredicates().forEach(p -> {
			str.append("\t"+p.toString()+"\n");
		});
		str.append(")\n");
		parser.getDomain().getActions().forEach(op -> {
			Pair<String, String> actions = opNames.get(op.getName().toString());
			//Start action
			str.append("(:action "+actions.getX()+"\n");
			str.append("\t:parameters (");
			op.getParameters().forEach(p -> {
				str.append(" "+op.getParameter(p));
			});
			str.append(" )\n");
			str.append("\t:precondition (and\n");
			classical.get(actions.getX()).getX().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t)\n");
			str.append("\t:effect (and\n");
			classical.get(actions.getX()).getY().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t)\n)\n");
			//End action
			str.append("(:action "+actions.getY()+"\n");
			str.append("\t:parameters (");
			op.getParameters().forEach(p -> {
				str.append(" "+op.getParameter(p));
			});
			str.append(" )\n");
			str.append("\t:precondition (and\n");
			classical.get(actions.getY()).getX().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t)\n");
			str.append("\t:effect (and\n");
			classical.get(actions.getY()).getY().forEach(p -> {
				str.append("\t\t"+p+"\n");
			});
			str.append("\t)\n)\n");
		});
		str.append(")");

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));

			bw.write(str.toString());
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		File file = new File(filename);
		return file;

	}

	/**
	 * Convert classical domain containing only a-start and a-end operators to
	 * temporal domain
	 * @param classical The classical domain to convert
	 * @param filename The name of the file where the temoral domain is wrote
	 * @return The temporal domain
	 */
	public static File convertClassicalToTemporal(File classical, 
			String filename) {
		//Parse temporal domain
		PDDLParser parser = new PDDLParser();
		try {
			parser.parseDomain(classical);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		//Get all operators
		List<PDDLAction> operators = parser.getDomain().getActions();
		List<String> opNames = new LinkedList<>();
		Map<String, List<String>> param = new HashMap<>();
		for(PDDLAction op : operators) {
			String str = op.getName().toString().split("-")[0];
			if(!opNames.contains(str)) {
				opNames.add(str);
				param.put(str, new LinkedList<>());
				op.getParameters().forEach(p -> {
					param.get(str).add(op.getParameter(p).toString());
				});
			}
		}

		//Get preconditions
		Map<String, List<String>> startPreconditions = new HashMap<>();
		Map<String, List<String>> endPreconditions = new HashMap<>();
		Map<String, List<String>> overAllPreconditions = new HashMap<>();
		for(PDDLAction op : operators) {
			String name = op.getName().toString().split("-")[0];

			if(op.getName().toString().split("-")[1].equals("start")) {
				startPreconditions.put(name, new LinkedList<>());
			} else {
				endPreconditions.put(name, new LinkedList<>());
			}

			for(PDDLExpression exp : op.getPreconditions().getChildren()) {
				switch(op.getName().toString().split("-")[1]) {
				case "start":
					startPreconditions.get(name).add(exp.toString());
					break;
				case "end":
					endPreconditions.get(name).add(exp.toString());
					break;
				default:
					break;
				}
			}

		}
		opNames.forEach(name -> {
			overAllPreconditions.put(name, new LinkedList<>());
			for(String str : startPreconditions.get(name)) {
				if(endPreconditions.get(name).contains(str)) {
					startPreconditions.get(name).remove(str);
					endPreconditions.get(name).remove(str);
					overAllPreconditions.get(name).add(str);
				}
			}
		});
		//Get effects
		Map<String, List<String>> startEffects = new HashMap<>();
		Map<String, List<String>> endEffects = new HashMap<>();
		for(PDDLAction op : operators) {
			String name = op.getName().toString().split("-")[0];

			if(op.getName().toString().split("-")[1].equals("start")) {
				startEffects.put(name, new LinkedList<>());
			} else {
				endEffects.put(name, new LinkedList<>());
			}
			
			for(PDDLExpression exp : op.getEffects().getChildren()) {
				switch(op.getName().toString().split("-")[1]) {
				case "start":
					startEffects.get(name).add(exp.toString());
					break;
				case "end":
					endEffects.get(name).add(exp.toString());
					break;
				}
				System.out.println(startEffects.get(name));
			}

		}

		//Get Type Hierarchy
		Map<String, String> hier = new HashMap<>();
		for(PDDLTypedSymbol t : parser.getDomain().getTypes()) {
			if(!t.getTypes().isEmpty()) {
				hier.put(t.getImage().toLowerCase(),
						t.getTypes().get(0).getImage().toLowerCase());
			}
		}
		TypeHierarchy types = new TypeHierarchy(hier);

		//Write Classical domain
		StringBuilder str = new StringBuilder();
		str.append("(define (domain "+parser.getDomain().getName().getImage()+")\n");
		str.append("(:requirements :strips :typing :negative-preconditions :durative-actions)\n");
		str.append("(:types \n"+types+")");
		str.append("(:predicates\n");
		parser.getDomain().getPredicates().forEach(p -> {
			str.append("\t"+p.toString()+"\n");
		});
		str.append(")\n");
		opNames.forEach(op -> {

			str.append("(:durative-action "+op+"\n");
			str.append("\t:parameters (");
			param.get(op).forEach(p -> {
				str.append(" "+p);
			});
			str.append(" )\n");
			str.append("\t:duration (= ?duration 1)\n");
			str.append("\t:condition (and\n");
			startPreconditions.get(op).forEach(p -> {
				str.append("\t\t(at start "+p+")\n");
			});
			endPreconditions.get(op).forEach(p -> {
				str.append("\t\t(at end "+p+")\n");
			});
			overAllPreconditions.get(op).forEach(p -> {
				str.append("\t\t(over all "+p+")\n");
			});
			str.append("\t)\n");
			str.append("\t:effect (and\n");
			startEffects.get(op).forEach(p -> {
				str.append("\t\t(at start "+p+")\n");
			});
			endEffects.get(op).forEach(p -> {
				str.append("\t\t(at end "+p+")\n");
			});
			str.append("\t)\n");
			str.append(")\n");
		});
		str.append(")");

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));

			bw.write(str.toString());
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		File file = new File(filename);
		return file;
	}
}
