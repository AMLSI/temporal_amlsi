/**
 * 
 * This package contains all classes and interfaces used to simulate the
 * system that AMLSI have to learn
 * 
 * @author Maxence Grand
 *
 */
package simulator;