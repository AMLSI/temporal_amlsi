package simulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import fsm.Symbol;
import fsm.Action;
import fsm.Predicate;
import fr.uga.pddl4j.parser.PDDLExpression;
import fr.uga.pddl4j.parser.PDDLParser;
import fr.uga.pddl4j.parser.PDDLProblem;
import fr.uga.pddl4j.parser.PDDLTypedSymbol;
import fr.uga.pddl4j.planners.ProblemFactory;
import fr.uga.pddl4j.planners.statespace.search.Node;
import fr.uga.pddl4j.problem.Fluent;
import fr.uga.pddl4j.problem.Problem;
import fr.uga.pddl4j.problem.State;

import java.io.File;

import learning.Observation;
import learning.TypeHierarchy;
import exception.PlanException;

/**
 * This class represents a blackbox
 * @author Maxence Grand
 */
public class BlackBox extends Oracle{
	/**
	 * The initial states
	 */
	protected Node initialState;
	/**
	 * The current state
	 */
	protected Node currentState;
	/**
	 * True if is succesful
	 */
	protected boolean successfull = false;
	/**
	 * A feasible action in the current state
	 */
	protected fr.uga.pddl4j.problem.Action possibleOp;
	/**
	 * The problem encoding
	 */
	protected Problem cp;
	/**
	 * All types
	 */
	protected Map<String, String> types;
	/**
	 * Positive static predicate
	 */
	protected List<Symbol> positiveStaticPredicate;
	/**
	 * Positive static predicate
	 */
	protected List<Symbol> negatives;
	/**
	 *
	 */
	protected List<Symbol> allPredicates;
	/**
	 *
	 */
	public List<Symbol> getAllPredicates() {
		return allPredicates;
	}
	/**
	 * All actions
	 */
	protected List<Symbol> allActions;
	/**
	 * 
	 */
	protected Map<Symbol, fr.uga.pddl4j.problem.Action> tableSymbolInstAction;
	
	/**
	 * 
	 */
	protected Map<Fluent, Symbol> tableFluentSymbol;

	/**
	 * The type hierarchy
	 */
	private TypeHierarchy hier;

	/**
	 * Constructs the blackbox
	 * @param d the domain
	 * @param p the initial state
	 */
	public BlackBox(String d, String p){
		try{

			File domain = new File(d);
			File problem = new File(p);
			ProblemFactory factory = ProblemFactory.getInstance();
			factory.parse(domain, problem);//.printAll();
			cp = factory.encode();
			
			if(cp == null){
				System.out.println("cp null "+d+" "+p);
				System.exit(1);
			}
		} catch(IOException e){
			e.printStackTrace();
			System.exit(1);
		}

		initialState = new Node(this.getInit());
		currentState = new Node(this.getInit());

		/*
		 * Retrieve all actions
		 */
		allActions = new ArrayList<>();
		tableSymbolInstAction = new HashMap<>();
		for (fr.uga.pddl4j.problem.Action  op : cp.getActions()) {
			Map<String, String> paramMap = new LinkedHashMap<>();
			boolean b = true;
			for (int i = 0; i < op.arity(); i++) {
				int index = op.getValueOfParameter(i);
				int index2 = op.getTypeOfParameters(i);
				String param = cp.getConstantSymbols().get(index);
				//System.out.println(p+" "+);
				b &= (!paramMap.keySet().contains(param));
				paramMap.put(param, cp.getTypeSymbols().get(index2));
			}
			if(b) {
				Action act = new Action(op.getName(),paramMap);
				allActions.add(act);
				tableSymbolInstAction.put(act, op);
			}
		}
		
		/*
		 * Retrieve all propositions and types
		 */
		positiveStaticPredicate = new ArrayList<>();
		List<Symbol> allSym = new ArrayList<>();
		List<Symbol> allFluents = new ArrayList<>();
		tableFluentSymbol=new HashMap<>();
		types = new LinkedHashMap<>();
		try{
			PDDLParser parser = new PDDLParser();
			parser.parse(d,p);
			PDDLProblem pb = parser.getProblem();
			
			//Types
			for(PDDLTypedSymbol tt : pb.getObjects()) {
				types.put(tt.getImage(), tt.getTypes().get(0).getImage());
			}
			
			//Propositions fluents
			for(Fluent fluent : cp.getRelevantFluents()) {
				String name = BlackBox.getPredicateName(cp.toString(fluent));
				Map<String, String> paramMap = new LinkedHashMap<>();
				boolean b = true;
				//System.out.println(cp.getConstantSymbols()+" "+pb.getObjects());
				for(int idx : fluent.getArguments()) {
					String param = cp.getConstantSymbols().get(idx);
					String paramType = this.types.get(param);
					b &= (!paramMap.keySet().contains(param));
					paramMap.put(param, paramType);
				}
				if(b) {
					Symbol pred = new Predicate(name, paramMap);
					allSym.add(pred);
					switch(cp.getInertia().get(cp.getPredicateSymbols().indexOf(name))) {
					case INERTIA:
						positiveStaticPredicate.add(pred);
						break;
					default:
						allFluents.add(pred);
						break;
					}
					//System.out.println(pred);
					tableFluentSymbol.put(fluent, pred);
				}		
			}
			//Proposition static
			/*for(PDDLExpression exp : pb.getInit()) {
				Map<String, String> paramMap = new LinkedHashMap<>();
				String name = exp.getAtom().get(0).toString();
				for(int i = 1; i < exp.getAtom().size(); i++) {
					String param = exp.getAtom().get(i).toString();
					String paramType = this.types.get(param);
					paramMap.put(param, paramType);
				}
				Symbol pred = new Predicate(name, paramMap);
				if(! allSym.contains(pred)) {
					positiveStaticPredicate.add(pred);
					allSym.add(pred);
				};
			}*/
			
		} catch(IOException e){
			e.printStackTrace();
			System.exit(1);
		}
		
		/**
		 * Compute type hierarchy
		 */
		hier = new TypeHierarchy();
		hier.infere(allSym, this.getParamTypes());
		allActions = hier.compute_litterals_operators
				(allActions, this.getParamTypes());
		positiveStaticPredicate = hier.compute_litterals_operators
				(positiveStaticPredicate, this.getParamTypes());
		allPredicates = hier.compute_litterals_operators
				(allFluents, this.getParamTypes());

		//System.out.println(allActions.get(23));
		//System.out.println(this.tableSymbolInstAction.get(allActions.get(23)).getPrecondition());
		//System.out.println(this.positiveStaticPredicate);
		//System.exit(1);
	}

	/**
	 * Reinit the current state
	 */
	@Override
	public void reInit(){
		initialState = new Node(this.getInit());
		currentState = new Node(this.getInit());
	}

	/**
	 * Get the initial state
	 * @return the initial state
	 */
	@Override
	public Node getInitialState() {
		return initialState;
	}

	/**
	 * Get the current state
	 * @return the current state
	 */
	@Override
	public Node getCurrentState() {
		return currentState;
	}

	/**
	 * Test if an action is feasible in the current state
	 * @param op an action
	 * @return True the action is feasible
	 */
	@Override
	public boolean isApplicable(Symbol op){
		//System.out.println(op);
		fr.uga.pddl4j.problem.Action ops = this.tableSymbolInstAction.get(op);
		/*System.out.println("Precondition "+op);
		System.out.println(currentState);
		System.out.println(ops.getPrecondition().getPositiveFluents());
		System.out.println(ops.getPrecondition().getNegativeFluents());*/
		if(ops != null && ops.isApplicable(currentState)) {
			possibleOp = ops;
			return true;
		}
		possibleOp = null;
		return false;
	}

	/**
	 * Check if an example is feasible starting by the initial state
	 * @param example to test
	 * @return True if the example is feasible
	 */
	@Override
	public boolean accept(List<Symbol> example) {
		for(Symbol op : example) {
			if(isApplicable(op)){
				try {
					apply();
				} catch (PlanException e) {
					return false;
				}
			}else{
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if an action is feasible in the current state
	 * @param op action to test
	 * @return True if the action is feasible
	 */
	@Override
	public boolean accept(Symbol op) {
		if(isApplicable(op)){
			try {
				apply();
			} catch (PlanException e) {
				return false;
			}
			return true;
		}else{
			return false;
		}

	}

	/**
	 * 
	 * @param op
	 * @return
	 */
	public Node apply(Symbol op) {
		fr.uga.pddl4j.problem.Action ops = this.tableSymbolInstAction.get(op);
		currentState.apply(ops.getUnconditionalEffect());
		return currentState;
	}
	
	/**
	 * 
	 */
	public Node apply(fr.uga.pddl4j.problem.Action op) {
		currentState.apply(op.getUnconditionalEffect());
		return currentState;
	}
	
	/**
	 * Apply the selected action
	 * @return the resulting state
	 * @throws PlanException 
	 */
	@Override
	public Node apply() throws PlanException{
		if(possibleOp == null){
			throw new exception.PlanException();
		}

		/*Map<String, String> paramMap = new LinkedHashMap<>();
		for (int i = 0; i < possibleOp.arity(); i++) {
			int index = possibleOp.getValueOfParameter(i);
			int index2 = possibleOp.getTypeOfParameters(i);
			String p = cp.getConstantSymbols().get(index);
			paramMap.put(p, cp.getTypeSymbols().get(index2));
		}
		Action newA = new Action(possibleOp.getName(),paramMap);
		System.out.println(newA+" "+possibleOp.getUnconditionalEffects());
		Observation obs = new Observation(this.getAllPredicates());
		for(Symbol s : this.getSymbolsState(currentState)){
			obs.addTrueObservation(s);
		}
		for(Symbol s : this.getPositiveStaticPredicate()){
			obs.addTrueObservation(s);
		}
		System.out.println("Before "+obs);;*/
		currentState.apply(possibleOp.getUnconditionalEffect());
		//possibleOp.getConditionalEffects().stream().forEach(ce -> state.apply(ce.getEffects())); 
		/*obs = new Observation(this.getAllPredicates());
		for(Symbol s : this.getSymbolsState(currentState)){
			obs.addTrueObservation(s);
		}
		for(Symbol s : this.getPositiveStaticPredicate()){
			obs.addTrueObservation(s);
		}*/
		//System.out.println("After "+obs);

		/*System.out.println("Effect  "+possibleOp);
		System.out.println(possibleOp.getUnconditionalEffect().getPositiveFluents());
		System.out.println(possibleOp.getUnconditionalEffect().getNegativeFluents());
		System.out.println(currentState);*/
		return currentState;
	}

	/**
	 * Test action's feasability
	 * @param op an action
	 * @throws PlanException
	 */
	@Override
	public void testAction(fr.uga.pddl4j.problem.Action  op) throws PlanException{
		String str = cp.toShortString(op);
		for(fr.uga.pddl4j.problem.Action  op2 : cp.getActions()){
			if(str.equals(cp.toShortString(op2))) {
				if(op2.isApplicable(new Node(currentState))) {
					currentState.apply(op2.getUnconditionalEffect());
					return;
				}
			}
		}
		throw new PlanException("");
	}

	/**
	 * Return all actions
	 * @return all actions
	 */
	@Override
	public List<Symbol> getAllActions(){
		return allActions;
	}

	/**
	 * Get all predicate present in the state
	 * @param state the state
	 * @return a set of predicate
	 */
	@Override
	public List<Symbol> getSymbolsState(Node state){
		List<Symbol> res = new ArrayList<>();
		for (int i = state.nextSetBit(0); i >= 0; i = state.nextSetBit(i + 1)) {
			Symbol p = tableFluentSymbol.get(cp.getRelevantFluents().get(i));
			res.add(p);
		}
		return res;
	}

	/**
	 * Get the name of an read predicate
	 * @param predicate a predicate
	 * @return predicate's name
	 */
	public static String getPredicateName(String predicate){
		String tmp = predicate.substring(1);
		String name = tmp.substring(0,tmp.length()-1).split(" ")[0];
		return name;
	}

	/**
	 * Get positive static predicates
	 * @return positive static predicates
	 */
	@Override
	public List<Symbol> getPositiveStaticPredicate(){
		return positiveStaticPredicate;
	}

	/**
	 * Get all types
	 * @return all types
	 */
	@Override
	public List<String> getTypes() {
		List<String> res = new ArrayList<>();
		types.forEach((s1, s2) ->{
			if(! res.contains(s2)) {
				res.add(s2);
			}
		});
		return res;
	}

	/**
	 * Return the parameters' type
	 * @return The parameters' type
	 */
	public Map<String, String> getParamTypes(){
		return types;
	}

	/**
	 * Check if the selected action is feasible
	 * @return true if the action is feasible
	 */
	@Override
	public boolean availableActions() {
		boolean b = false;
		for(Symbol s : allActions){
			b |= isApplicable(s);
		}
		return b;
	}

	/**
	 * Return the type hierarchy
	 * @return The type hierarchy
	 */
	@Override
	public TypeHierarchy typeHierarchy() {
		return hier;
	}


	/**
	 * 
	 * @return
	 */
	private Node getInit() {
		State init = new State(cp.getInitialState());                                                                                                                        
		Node root = new Node(init);
		return root;
	}
	
	/**
     * 
     * @param act
     * @param p
     * @return
     */
	@Override
    protected boolean consistantPrecondition(Symbol act, Observation p) {
		fr.uga.pddl4j.problem.Action op = this.tableSymbolInstAction.get(act);
		List<Symbol> pos = new ArrayList<>(), neg = new ArrayList<>();
		for(int i = 0; i < op.getPrecondition()
				.getPositiveFluents().cardinality(); i++) {
			Symbol prop = this.tableFluentSymbol.get(cp.getRelevantFluents().get(i));
			pos.add(prop);
		}
		for(int i = 0; i < op.getPrecondition()
				.getNegativeFluents().cardinality(); i++) {
			Symbol prop = this.tableFluentSymbol.get(cp.getRelevantFluents().get(i));
			neg.add(prop);
		}
		for(Symbol prop : p.getPredicatesSymbols()) {
			switch(p.getValue(prop)) {
			case TRUE:
				if(! pos.contains(prop)) {
					if(! this.positiveStaticPredicate.contains(prop)) {
						return false;
					}			
				}
				break;
			case FALSE:
				if(! neg.contains(prop)) {
					return false;
				}
				break;
			default:
				break;
			}
		}
    	return true;
    }
    
    /**
     * 
     * @param act
     * @param e
     * @return
     */
	@Override
    protected boolean consistantEffect(Symbol act, Observation e) {
		fr.uga.pddl4j.problem.Action op = this.tableSymbolInstAction.get(act);
		List<Symbol> pos = new ArrayList<>(), neg = new ArrayList<>();
		for(int i = 0; i < op.getUnconditionalEffect()
				.getPositiveFluents().cardinality(); i++) {
			Symbol prop = this.tableFluentSymbol.get(cp.getRelevantFluents().get(i));
			pos.add(prop);
		}
		for(int i = 0; i < op.getUnconditionalEffect()
				.getNegativeFluents().cardinality(); i++) {
			Symbol prop = this.tableFluentSymbol.get(cp.getRelevantFluents().get(i));
			neg.add(prop);
		}
		for(Symbol prop : e.getPredicatesSymbols()) {
			switch(e.getValue(prop)) {
			case TRUE:
				if(! pos.contains(prop)) {
					return false;
				}
				break;
			case FALSE:
				if(! neg.contains(prop)) {
					return false;
				}
				break;
			default:
				break;
			}
		}
    	return true;
    }
}
