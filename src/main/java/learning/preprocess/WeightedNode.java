/**
 * 
 */
package learning.preprocess;

import learning.Observation;
import learning.Observation.Value;
import fsm.Symbol;

import java.util.Objects;

import fsm.Pair;
/**
 * @author Maxence Grand
 *
 */
public class WeightedNode {
	/**
	 * 
	 */
	private int bloc;
	/**
	 * 
	 */
	private Symbol action;
	/**
	 * 
	 */
	private Symbol proposition;
	/**
	 * 
	 */
	private Pair<Integer, Integer> frequency;
	/**
	 * 
	 */
	private Observation.Value value;
	/**
	 * 
	 */
	private boolean ante;
	
	/**
	 * Constructs 
	 * @param bloc
	 * @param action
	 * @param proposition
	 * @param frequency
	 * @param value
	 */
	public WeightedNode(int bloc, Symbol action, Symbol proposition, Pair<Integer, Integer> frequency, Value value, boolean ante) {
		this.bloc = bloc;
		this.action = action;
		this.proposition = proposition;
		this.frequency = frequency;
		this.value = value;
		this.ante = ante;
	}

	/**
	 * 
	 * Constructs 
	 * @param other
	 */
	public WeightedNode(WeightedNode other) {
		this(other.getBloc(), other.getAction(), other.getProposition(), other.getFrequency(), other.getValue(), other.isAnte());
	}
	
	/**
	 * 
	 */
	public WeightedNode clone() {
		return new WeightedNode(this);
	}
	
	/**
	 * Getter of bloc
	 * @return the bloc
	 */
	public int getBloc() {
		return bloc;
	}

	/**
	 * Setter bloc
	 * @param bloc the bloc to set
	 */
	public void setBloc(int bloc) {
		this.bloc = bloc;
	}

	/**
	 * Getter of action
	 * @return the action
	 */
	public Symbol getAction() {
		return action;
	}

	/**
	 * Setter action
	 * @param action the action to set
	 */
	public void setAction(Symbol action) {
		this.action = action;
	}

	/**
	 * Getter of proposition
	 * @return the proposition
	 */
	public Symbol getProposition() {
		return proposition;
	}

	/**
	 * Setter proposition
	 * @param proposition the proposition to set
	 */
	public void setProposition(Symbol proposition) {
		this.proposition = proposition;
	}

	/**
	 * Getter of frequency
	 * @return the frequency
	 */
	public Pair<Integer, Integer> getFrequency() {
		return frequency;
	}

	/**
	 * Setter frequency
	 * @param frequency the frequency to set
	 */
	public void setFrequency(Pair<Integer, Integer> frequency) {
		this.frequency = frequency;
	}

	/**
	 * Getter of value
	 * @return the value
	 */
	public Observation.Value getValue() {
		return value;
	}

	/**
	 * Setter value
	 * @param value the value to set
	 */
	public void setValue(Observation.Value value) {
		this.value = value;
	}

	
	/**
	 * Getter of ante
	 * @return the ante
	 */
	public boolean isAnte() {
		return ante;
	}

	/**
	 * Setter ante
	 * @param ante the ante to set
	 */
	public void setAnte(boolean ante) {
		this.ante = ante;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		return Objects.hash(action, bloc, proposition, value, ante);
	}

	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof WeightedNode))
			return false;
		WeightedNode other = (WeightedNode) obj;
		return Objects.equals(action, other.action) && bloc == other.bloc && Objects.equals(frequency, other.frequency)
				&& Objects.equals(proposition, other.proposition) && value == other.value && ante == other.ante;
	}
	
	/**
	 * 
	 * @return
	 */
	public float weight() {
		if(this.getFrequency().getX() == this.getFrequency().getY()) {
			return (float)0.5;
		}
		switch(this.getValue()) {
		case TRUE:
			return (float) this.getFrequency().getX() /
					(this.getFrequency().getX() + this.getFrequency().getY());
		case FALSE:
			return (float) this.getFrequency().getY() /
					(this.getFrequency().getX() + this.getFrequency().getY());
		default:
			return 1;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WeightedNode [bloc=");
		builder.append(bloc);
		builder.append(", action=");
		builder.append(action);
		builder.append(", proposition=");
		builder.append(proposition);
		builder.append(", frequency=");
		builder.append(frequency);
		builder.append(", value=");
		builder.append(value);
		builder.append(", ante=");
		builder.append(ante);
		builder.append(" w="+this.weight());
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
