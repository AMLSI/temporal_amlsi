/**
 * 
 */
package learning.preprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import exception.BlocException;
import fsm.FiniteStateAutomata;
import fsm.Pair;
import fsm.Symbol;
import learning.Mapping;
import learning.Observation;

/**
 * @author Maxence Grand
 *
 */
public class Preprocessor {
	/**
	 * 
	 */
	private List<WeightedNode> anteNodes;
	/**
	 * 
	 */
	private List<WeightedNode> postNodes;

	/**
	 * 
	 * Constructs
	 */
	public Preprocessor() {
		this.anteNodes = new ArrayList<>();
		this.postNodes = new ArrayList<>();
	}

	/**
	 * 
	 * @param A
	 * @param P
	 * @param fsa
	 * @param data
	 * @throws BlocException 
	 */
	public void preprocessing(List<Symbol> A, List<Symbol> P,
			FiniteStateAutomata fsa, Map<List<Symbol>, List<Observation>> data) throws BlocException {
		//Build First structure : (Action x Proposition) -> Map(Bloc -> observations) x Map(Bloc -> observations)
		Map<Pair<Symbol,Symbol>, Pair<Map<Integer, List<Observation>>, Map<Integer, List<Observation>>>> map;
		map = new HashMap<>();
		for(Symbol act : A) {
			for(Symbol prop : P) {
				if(!act.compatible(prop)) {
					continue;
				}
				Pair<Symbol, Symbol> p = new Pair<>(act, prop);
				map.put(p, new Pair<>(new HashMap<>(), new HashMap<>()));
			}
		}
		//Play examples in the automaton
		for(Map.Entry<List<Symbol>, List<Observation>> e : data.entrySet()) {
			List<Symbol> example = e.getKey();
			List<Observation> observations = e.getValue();
			int ante = 0, post=0;
			for(int i = 0; i<example.size(); i++) {
				Symbol act = example.get(i);
				Observation
				obsAnte = observations.get(i),
				obsPost = observations.get(i+1);
				post = fsa.getBlocTransition(fsa.getPartition().getBloc(ante), act).min();
				for(Symbol prop : P) {
					if(act.compatible(prop)) {
						Pair<Symbol, Symbol> p = new Pair<>(act, prop);
						if(! map.get(p).getX().containsKey(ante)) {
							map.get(p).getX().put(ante, new ArrayList<>());
						}
						map.get(p).getX().get(ante).add(obsAnte);
						if(! map.get(p).getY().containsKey(post)) {
							map.get(p).getY().put(post, new ArrayList<>());
						}
						map.get(p).getY().get(post).add(obsPost);
					}
				}
				ante=post;
			}
		}

		//Build Second structure : (Action x Proposition) -> Map(Bloc -> frequency) x Map(Bloc -> frequency)
		Map<Pair<Symbol,Symbol>, Pair<Map<Integer, Pair<Integer, Integer>>, Map<Integer, Pair<Integer, Integer>>>> map2;
		map2 = new HashMap<>();
		for(Symbol act : A) {
			for(Symbol prop : P) {
				if(!act.compatible(prop)) {
					continue;
				}
				Pair<Symbol, Symbol> p = new Pair<>(act, prop);
				//Compute frequency for ante nodes
				Map<Integer, Pair<Integer, Integer>> ante = new HashMap<>();
				for(int i : map.get(p).getX().keySet()) {
					int nbTrue = 0, nbFalse=0;
					for(Observation obs : map.get(p).getX().get(i)) {
						switch(obs.getValue(prop)) {
						case FALSE:
							nbFalse++;
							break;
						case TRUE:
							nbTrue++;
							break;
						default:
							break;

						}
					}
					ante.put(i, new Pair<>(nbTrue, nbFalse));
				}
				//Compute frequency for post nodes
				Map<Integer, Pair<Integer, Integer>> post = new HashMap<>();
				for(int i : map.get(p).getY().keySet()) {
					int nbTrue = 0, nbFalse=0;
					for(Observation obs : map.get(p).getY().get(i)) {
						switch(obs.getValue(prop)) {
						case FALSE:
							nbFalse++;
							break;
						case TRUE:
							nbTrue++;
							break;
						default:
							break;

						}
					}
					post.put(i, new Pair<>(nbTrue, nbFalse));
				}
				map2.put(p, new Pair<>(ante, post));
			}
		}
		//System.out.println(map2);
		//Map Operator -> [Compatible Predicates]
		Map<Symbol, List<Symbol>> operators = new HashMap<>();
		for(Symbol act : A) {
			Symbol op = act.generalize();
			if(operators.containsKey(op)) {
				continue;
			} else {
				operators.put(op, new ArrayList<>());
			}
			for(Symbol prop : P) {
				if(!act.compatible(prop)) {
					continue;
				}
				Symbol pred = prop.generalize(act.mapping());
				if(! operators.get(op).contains(pred)) {
					operators.get(op).add(pred);
				}
			}
		}

		//Build third structure : (Operator x Predicate) -> Best Candidate
		Map<Pair<Symbol, Symbol>, Candidate> candidates = new HashMap<>();
		for(Symbol op : operators.keySet()) {
			for(Symbol pred : operators.get(op)) {
				LinkedList<Candidate> allCandidates = new LinkedList<>();
				Candidate c = new Candidate();
				allCandidates.add(c);
				for(Symbol act : A) {
					for(Symbol prop : P) {
						if(!act.compatible(prop)) {
							continue;
						}
						if(! act.generalize().equals(op)) {
							continue;
						}
						if(! prop.generalize(act.mapping()).equals(pred)) {
							continue;
						}
						//System.out.println(act+" "+prop+" "+map.keySet()+" \n");
						for(int i : map2.get(new Pair<>(act, prop)).getX().keySet()) {
							int j = fsa.getBlocTransition(fsa.getPartition().getBloc(i), act).min();
							//Create all possible nodes
							WeightedNode
							anteTrue = new WeightedNode(i, act, prop, map2.get(new Pair<>(act, prop)).getX().get(i), Observation.Value.TRUE, true),
							anteFalse = new WeightedNode(i, act, prop, map2.get(new Pair<>(act, prop)).getX().get(i), Observation.Value.FALSE, true),
							postTrue = new WeightedNode(j, act, prop, map2.get(new Pair<>(act, prop)).getY().get(j), Observation.Value.TRUE, false),
							postFalse = new WeightedNode(j, act, prop, map2.get(new Pair<>(act, prop)).getY().get(j), Observation.Value.FALSE, false);
							//Create all possible transitions
							WeightedTransition
							trueTrue = new WeightedTransition(anteTrue, postTrue),
							trueFalse = new WeightedTransition(anteTrue, postFalse),
							falseTrue = new WeightedTransition(anteFalse, postTrue),
							falseFalse = new WeightedTransition(anteFalse, postFalse);
							//System.out.println(trueTrue);
							//Check valid transitions (weight > 0)
							List<WeightedTransition> transitions = new ArrayList<>();
							if(trueTrue.feasible()) {
								transitions.add(trueTrue);
							}
							if(trueFalse.feasible()) {
								transitions.add(trueFalse);
							}
							if(falseTrue.feasible()) {
								transitions.add(falseTrue);
							}
							if(falseFalse.feasible()) {
								transitions.add(falseFalse);
							}
							//Update all previous candidate with the new transitions
							LinkedList<Candidate> allCandidates2 = new LinkedList<>();
							while(! allCandidates.isEmpty()) {
								Candidate tmp = allCandidates.poll();
								for(WeightedTransition t : transitions) {
									if(tmp.isCompatible(t)) {
										Candidate tmp2 = tmp.clone();
										tmp2.add(t);
										if(tmp2.feasible()) {
											allCandidates2.add(tmp2);
										}
									}
									//If for a previous candidate no transitions are compatible/feasible
									//Then the previous candidate is removed
								}
							}
							allCandidates = allCandidates2;
						}
					}
				}
				//Find best candidate
				c =null;
				System.out.println(allCandidates.size());
				float weightMax = 0;
				while(! allCandidates.isEmpty()) {
					Candidate cc = allCandidates.poll();
					if(cc.weight() >= weightMax) {
						c = cc;
						weightMax = c.weight();
					}
				}
				candidates.put(new Pair<>(op, pred), c);
				System.out.println(c);
			}
		}

		//Build anteNodes / postNodes
		for(Pair<Symbol, Symbol> p : candidates.keySet()) {
			if(candidates.get(p) == null) {
				continue;
			}
			this.anteNodes.addAll(candidates.get(p).anteNodes());
			this.postNodes.addAll(candidates.get(p).postNodes());
		}
	}

	/**
	 * 
	 * @param A
	 * @param P
	 * @return
	 */
	public Mapping getMappingAnte(List<Symbol> A, List<Symbol> P) {
		Mapping m = new Mapping();
		Map<Symbol, Map<Integer, Observation>> map = new HashMap<>();
		for(Symbol act : A) {
			map.put(act, new HashMap<>());
		}
		for(WeightedNode node : anteNodes) {
			if(! map.get(node.getAction()).containsKey(node.getBloc())) {
				map.get(node.getAction()).put(node.getBloc(), new Observation());
			}
			map.get(node.getAction()).get(node.getBloc()).addObservation(node.getProposition(), node.getValue());
		}
		m.setMap(map);
		return m;
	}

	/**
	 * 
	 * @param A
	 * @param P
	 * @return
	 */
	public Mapping getMappingPost(List<Symbol> A, List<Symbol> P) {
		Mapping m = new Mapping();
		Map<Symbol, Map<Integer, Observation>> map = new HashMap<>();
		for(Symbol act : A) {
			map.put(act, new HashMap<>());
		}
		for(WeightedNode node : postNodes) {	
			if(! map.get(node.getAction()).containsKey(node.getBloc())) {
				map.get(node.getAction()).put(node.getBloc(), new Observation());
			}
			map.get(node.getAction()).get(node.getBloc()).addObservation(node.getProposition(), node.getValue());
		}
		m.setMap(map);
		return m;
	}
}
