/**
 * 
 */
package learning.preprocess;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maxence Grand
 *
 */
public class Candidate {
	/**
	 * 
	 */
	private List<WeightedTransition> transitions;
	
	/**
	 * 
	 * Constructs
	 */
	public Candidate() {
		this.transitions = new ArrayList<>();
	}
	
	/**
	 * 
	 * Constructs 
	 * @param other
	 */
	private Candidate(Candidate other) {
		this();
		for(WeightedTransition t : other.transitions) {
			this.transitions.add(t);
		}
	}
	
	/**
	 * 
	 */
	public Candidate clone() {
		return new Candidate(this);
	}

	/**
	 * @param e
	 * @return
	 * @see java.util.List#add(java.lang.Object)
	 */
	public boolean add(WeightedTransition e) {
		if(this.isCompatible(e)) {
			return transitions.add(e);
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param t
	 * @return
	 */
	public boolean isCompatible(WeightedTransition e) {
		boolean b = true;
		for(WeightedTransition t : this.transitions) {
			b &= t.compatible(e);
			if(!b) {
				break;
			}
		}
		return b;
	}
	
	/**
	 * 
	 * @return
	 */
	public float weight() {
		if(this.transitions.isEmpty()) {
			return 1;
		}
		float w = 0, f=0;
		for(WeightedTransition t : this.transitions) {
			w += t.weight();
			f += t.frequency();
		}
		if(f==0) {
			return 0;
		}
		return w/f;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean feasible() {
		if(this.weight() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public List<WeightedNode> anteNodes() {
		List<WeightedNode> res = new ArrayList<>();
		for(WeightedTransition t : this.transitions) {
			res.add(t.getAnte());
		}
		return res;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<WeightedNode> postNodes() {
		List<WeightedNode> res = new ArrayList<>();
		for(WeightedTransition t : this.transitions) {
			res.add(t.getPost());
		}
		return res;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Candidate [transitions=");
		builder.append(transitions);
		builder.append(" w="+this.weight());
		builder.append("]");
		return builder.toString();
	}
	
	
}
