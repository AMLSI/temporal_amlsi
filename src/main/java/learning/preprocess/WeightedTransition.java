/**
 * 
 */
package learning.preprocess;

import java.util.Objects;

import learning.Observation;

/**
 * @author Maxence Grand
 *
 */
public class WeightedTransition {
	/**
	 * 
	 */
	private WeightedNode ante;
	/**
	 * 
	 */
	private WeightedNode post;


	/**
	 * Constructs 
	 * @param ante
	 * @param post
	 */
	public WeightedTransition(WeightedNode ante, WeightedNode post) {
		this.ante = ante;
		this.post = post;
	}
	/**
	 * Getter of ante
	 * @return the ante
	 */
	public WeightedNode getAnte() {
		return ante;
	}
	/**
	 * Setter ante
	 * @param ante the ante to set
	 */
	public void setAnte(WeightedNode ante) {
		this.ante = ante;
	}
	/**
	 * Getter of post
	 * @return the post
	 */
	public WeightedNode getPost() {
		return post;
	}
	/**
	 * Setter post
	 * @param post the post to set
	 */
	public void setPost(WeightedNode post) {
		this.post = post;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		return Objects.hash(ante, post);
	}

	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof WeightedTransition))
			return false;
		WeightedTransition other = (WeightedTransition) obj;
		return Objects.equals(ante, other.ante) && Objects.equals(post, other.post);
	}

	/**
	 * 
	 * @return
	 */
	public float weight() {
		float w1 = this.getAnte().weight();
		float w2 = this.getPost().weight();;
		return (float) (w1*w2);
	}
	
	/**
	 * 
	 * @return
	 */
	public int frequency() {
		int f1 =  (this.getAnte().getValue() == Observation.Value.TRUE) ?
				this.getAnte().getFrequency().getX() : this.getAnte().getFrequency().getY();
		int f2 =  (this.getPost().getValue() == Observation.Value.TRUE) ?
						this.getPost().getFrequency().getX() : this.getPost().getFrequency().getY();
		return f1+f2;
	}

	/**
	 * 
	 * @param other
	 * @return
	 */
	public boolean compatible(WeightedTransition other) {
		switch(this.getAnte().getValue()) {
		case TRUE:
			switch(this.getPost().getValue()) {
			case TRUE:
				switch(other.getAnte().getValue()) {
				case TRUE:
					switch(other.getPost().getValue()) {
					//l1 & l1' & l2 & !l2' Impossible
					case FALSE:
						return false;
						//l1 & l1' & l2 & l2' Possible
					case TRUE:
						return true;
					default:
						return true;
					}
				case FALSE:
					switch(other.getPost().getValue()) {
					//l1 & l1' & !l2 & !l2' Possible
					case FALSE:
						return true;
						//l1 & l1' & !l2 & l2' Possible
					case TRUE:
						return true;
					default:
						return true;
					}
				default:
					return true;
				}
			case FALSE:
				switch(other.getAnte().getValue()) {
				case TRUE:
					switch(other.getPost().getValue()) {
					//l1 & !l1' & l2 & !l2' Possible
					case FALSE:
						return true;
						//l1 & !l1' & l2 & l2' Impossible
					case TRUE:
						return false;
					default:
						return true;
					}
				case FALSE:
					switch(other.getPost().getValue()) {
					//l1 & !l1' & !l2 & !l2' Impossible
					case FALSE:
						return false;
						//l1 & !l1' & !l2 & l2' Impossible
					case TRUE:
						return false;
					default:
						return true;
					}
				default:
					return true;
				}
			default:
				return true;
			}
		case FALSE:
			switch(this.getPost().getValue()) {
			case TRUE:
				switch(other.getAnte().getValue()) {
				case TRUE:
					switch(other.getPost().getValue()) {
					//!l1 & l1' & l2 & !l2' Impossible
					case FALSE:
						return false;
						//!l1 & l1' & l2 & l2' Possible
					case TRUE:
						return true;
					default:
						return true;
					}
				case FALSE:
					switch(other.getPost().getValue()) {
					//!l1 & l1' & !l2 & !l2' Possible
					case FALSE:
						return true;
						//!l1 & l1' & !l2 & l2' Possible
					case TRUE:
						return true;
					default:
						return true;
					}
				default:
					return true;
				}
			case FALSE:
				switch(other.getAnte().getValue()) {
				case TRUE:
					switch(other.getPost().getValue()) {
					//!l1 & !l1' & l2 & !l2' Impossible
					case FALSE:
						return false;
						//!l1 & !l1' & l2 & l2' Possible
					case TRUE:
						return true;
					default:
						return true;
					}
				case FALSE:
					switch(other.getPost().getValue()) {
					//!l1 & !l1' & !l2 & !l2' Possible
					case FALSE:
						return true;
						//!l1 & !l1' & !l2 & l2' Impossible
					case TRUE:
						return false;
					default:
						return true;
					}
				default:
					return true;
				}
			default:
				return true;
			}
		default:
			return true;
		}
	}

	/**
	 * 
	 * @return
	 */
	public boolean feasible() {
		if(this.weight() > 0) {
			return true;
		} else {
			return false;
		}
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WeightedTransition [ante=");
		builder.append(ante);
		builder.append(", post=");
		builder.append(post);
		builder.append(" w="+this.weight());
		builder.append("]");
		return builder.toString();
	}


}
