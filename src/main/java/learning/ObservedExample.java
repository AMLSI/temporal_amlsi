/**
 * 
 */
package learning;

import java.util.List;

import fsm.Example;
import fsm.Symbol;

/**
 * @author Maxence Grand
 *
 */
public class ObservedExample extends Example{
	/**
	 * 
	 */
	private List<Observation> observations;
	
	/**
	 * Constructs 
	 * @param actionSequences
	 */
	public ObservedExample(List<Symbol> actionSequences, 
			List<Observation> observations) {
		super(actionSequences);
		if(this.size() + 1 != observations.size()) {
			throw new IllegalArgumentException("Observation sequence size must "
									+"be equals to action sequence size + 1");
		}
		this.observations = observations;
	}

	/**
	 * Getter of observations
	 * @return the observations
	 */
	public List<Observation> getObservations() {
		return observations;
	}
	
	/**
	 * 
	 * @return
	 */
	public Observation getInitialState() {
		return this.observations.get(0);
	}
	
	/**
	 * 
	 * @param a
	 * @return
	 */
	public Observation ante(int a) {
		return this.observations.get(a);
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	public Observation post(int a) {
		return this.observations.get(a+1);
	}
}
