package learning;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import fsm.Symbol;
import fsm.FiniteStateAutomata;
import fsm.Sample;
import fsm.Bloc;
import fsm.Example;
import exception.BlocException;

/**
 * This class represent the mapping "Action" "state in automaton, observed state"
 * @author Maxence Grand
 */
public class Mapping {
    /**
     * The mapping
     */
    private Map<Symbol, Map<Integer, Observation>> map;
    private Map<Symbol, Map<Integer, List<Observation>>> mapNotReduced;
    
    /**
     * Constructs the mapping
     */
    public Mapping(){
        map = new HashMap<>();
        mapNotReduced = new HashMap<>();
    }

    /**
     * Construct the mapping
     * @param mapping the mapping
     * @param preds all predicates
     */
    public Mapping(Map<Symbol, Map<Integer, List<Observation>>> mapping,
                   List<Symbol> preds){
        this();
        for(Map.Entry<Symbol, Map<Integer, List<Observation>>> entry :
                mapping.entrySet()) {
            map.put(entry.getKey(), new HashMap<>());
            mapNotReduced.put(entry.getKey(), new HashMap<>());
            for(Map.Entry<Integer, List<Observation>> entry2 :
                    entry.getValue().entrySet()) {
                map.get(entry.getKey()).put(
                		entry2.getKey(),
                        new Observation(entry2.getValue(), preds));
                mapNotReduced.get(entry.getKey()).put(
                		entry2.getKey(),
                        entry2.getValue());
            }
        }
    }
    
    /**
     * Get the mapping ante
     * @param seqObs All observed sequences
     * @param A the automaton
     * @param actions all actions
     * @param preds all predicates
     * @return A mapping
     * @throws BlocException 
     */
    public static Mapping getMappingAnte (Sample pos, FiniteStateAutomata A,
    		List<Symbol> actions, List<Symbol> preds ) throws BlocException{

        Map<Symbol, Map<Integer, List<Observation>>> mapping = new HashMap<>();
        for(Symbol act : actions) {
            mapping.put(act, new HashMap<>());
        }
        
        for(Example example : pos.getExamples()){
        	ObservedExample observation = (ObservedExample) example;
        	//System.out.println(entry.getKey());
            Bloc b = A.getPartition().getBloc(A.getQ0());
            for(int i= 0; i < observation.size(); i++){
            	//System.out.println(b+" "+entry.getKey().get(i));
                if(!mapping.get(observation.get(i)).containsKey(b.min())){
                    mapping.get(observation.get(i)).put(b.min(), new ArrayList<>());
                }
                mapping.get(observation.get(i)).get(b.min()).add(observation.ante(i));
                
                b = A.getBlocTransition(b, observation.get(i));
            }
        }
        
        return new Mapping(mapping, preds);
    }
    
    /**
     * Get the mapping post
     * @param seqObs All observed sequences
     * @param A the automaton
     * @param actions all actions
     * @param preds all predicates
     * @return A mapping
     * @throws BlocException 
     */
    public static Mapping getMappingPost (Sample pos, FiniteStateAutomata A, 
    		List<Symbol> actions, List<Symbol> preds ) throws BlocException{

        Map<Symbol, Map<Integer, List<Observation>>> mapping = new HashMap<>();
        for(Symbol act : actions) {
            mapping.put(act, new HashMap<>());
        }
        
        for(Example example : pos.getExamples()){
        	ObservedExample observation = (ObservedExample) example;
            Bloc b = A.getPartition().getBloc(A.getQ0());
            for(int i= 0; i < observation.size(); i++){
                Bloc b2 = A.getBlocTransition(b, observation.get(i));
                if(!mapping.get(observation.get(i)).containsKey(b2.min())){
                    mapping.get(observation.get(i)).put(b2.min(), new ArrayList<>());
                }
                mapping.get(observation.get(i)).get(b2.min()).add(observation.post(i));
                
                b = b2;
            }
        }
        
        return new Mapping(mapping, preds);
    }
        
    /**
     * Get the observed state for a given action and automaton state
     * @param q the automaton state
     * @param act the action
     * @return an observed state
     */
    public Observation getStates(int q, Symbol act){
        return map.get(act).get(q);
    }

    public List<Observation> getSetStates(int q, Symbol act){
        return mapNotReduced.get(act).get(q);
    }
    /**
     * Get the set of mapping "state automaton" "observed state" for a given
     * action
     * 
     * @param act the action
     * @return a mapping
     */
    public Map<Integer,Observation> getSet(Symbol act){
        return map.get(act);
    }
    
    /**
     * String representation of the mapping
     * @return A string
     */
    @Override
    public String toString() {
            return ""+map;
    }

	/**
	 * Setter map
	 * @param map the map to set
	 */
	public void setMap(Map<Symbol, Map<Integer, Observation>> map) {
		this.map = map;
	}
    
	public boolean contains(Symbol a) {
		return map.containsKey(a);
	}
    
}
