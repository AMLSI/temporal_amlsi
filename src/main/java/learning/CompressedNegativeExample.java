/**
 * 
 */
package learning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fsm.Example;
import fsm.Symbol;

/**
 * @author Maxence Grand
 *
 */
public class CompressedNegativeExample {
	/**
	 * 
	 */
	private Example prefix;
	/**
	 * 
	 */
	private Map<Integer, List<Example>> negatives;
	
	/**
	 * Constructs 
	 * @param prefix
	 * @param negatives
	 */
	public CompressedNegativeExample(Example prefix, List<Example> negatives) {
		this.prefix = prefix.clone();
		this.negatives = new HashMap<>();
		negatives.forEach( neg -> {
			int suff = CompressedNegativeExample.sizeCommonPrefix(prefix, neg);
			if(! this.negatives.containsKey(suff)) {
				this.negatives.put(suff, new ArrayList<>());
			}
			this.negatives.get(suff).add(neg.getSuffix(suff));
		});
	}

	/**
	 * Getter of prefix
	 * @return the prefix
	 */
	public List<Symbol> getPrefix() {
		return prefix.getActionSequences();
	}

	/**
	 * Setter prefix
	 * @param prefix the prefix to set
	 */
	public void setPrefix(List<Symbol> prefix) {
		this.prefix = new Example(prefix);
	}

	/**
	 * Getter of negatives
	 * @return the negatives
	 */
	/*public List<List<Symbol>> getNegatives() {
		return negatives;
	}*/

	/**
	 * Setter negatives
	 * @param negatives the negatives to set
	 */
	public void setNegatives(Map<Integer, List<Example>> negatives) {
		this.negatives = negatives;
	}
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private static int sizeCommonPrefix(Example a, Example b) {
		int res = 0;
		for(int i = 0; i < a.size() && i < b.size(); i++) {
			if(a.get(i).equals(b.get(i))) {
				res ++;
			}
		}
		return res;
	}
	
	/**
	 * 
	 * @param i
	 * @return
	 */
	public List<Example> getNegativeIndex(int i) {
		if(!this.negatives.containsKey(i)) {
			//System.out.println(i+" 0");
			return new ArrayList<>();
		}
		//System.out.println(i+" "+this.negatives.get(i).size());
		return this.negatives.get(i);
	}
}
