package learning;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;
import fsm.*;
import exception.BlocException;

/**
 * This class implements the action model learning module.
 *
 * @author Maxence Grand
 * @version 1.0
 * @since   10-2019
 */
public class DomainLearning {
	/**
	 * The size of the buffer for tabu search
	 */
	public static final int bufferSize = 10;
	/**
	 * The size of the tabu list
	 */
	public static final int tabouSize = 10;
	/**
	 * The number of epochs for the tabu search
	 */
	public static int tabouEpoch = 200;
	/**
	 * The set of predicates
	 */
	protected List<Symbol> predicates;
	/**
	 * The set of predicates
	 */
	protected List<Symbol> actions;
	/**
	 * The positive test sample
	 */
	protected Sample testPositive;
	/**
	 * The negative test sample
	 */
	protected Sample testNegative;
	/**
	 * The directory where we save logs
	 */
	protected String directory;
	/**
	 * The planning domain's name
	 */
	protected String name;
	/**
	 * The initial state
	 */
	protected String initialState;
	/**
	 * A set of fsa
	 */
	protected FiniteStateAutomata As;
	/**
	 * A mapping
	 */
	protected Mapping mappings;
	/**
	 * A mapping
	 */
	protected Mapping mappingPosts;
	/**
	 * The file where the learned domain is saved
	 */
	protected String learnedDomain;

	/**
	 * The individual learned
	 */
	protected Individual learned;
	/**
	 * The genarator
	 */
	protected Generator generator;

	/**
	 * The type hierarchy
	 */
	protected TypeHierarchy types;
	
	/**
	 * 
	 */
	protected Map<Individual, Float> fitnessScores;

	/**
	 * The type hierarchy
	 * @return The type hierarchy
	 */
	public TypeHierarchy getTypes() {
		return types;
	}

	/**
	 * Step types hierarchy
	 * @param types
	 */
	public void setTypes(TypeHierarchy types) {
		this.types = types;
	}

	/**
	 * The constructor of the object DomainLearning
	 */
	public DomainLearning() {
		predicates = new ArrayList<>();
		actions = new ArrayList<>();
		types = new TypeHierarchy();
		fitnessScores = new ConcurrentHashMap<>();
	}

	/**
	 * The constructor of the object DomainLearning
	 *
	 * @param predicates The set of predicates
	 * @param actions The set of actions
	 * @param directory The directory where the learned domain is saved
	 * @param name The planning domain's name
	 * @param realDomain The domain to copy
	 * @param initialState The initial state
	 */
	public DomainLearning(
			List<Symbol> predicates,
			List<Symbol> actions,
			String directory,
			String name,
			String realDomain,
			String initialState,
			Generator generator
			) {
		this();
		this.name=name;
		this.generator = generator;
		for(Symbol s : predicates) {
			this.predicates.add(s);
		}
		for(Symbol s : actions) {
			this.actions.add(s);
		}
		this.initialState = initialState;
		this.directory = directory;
		learnedDomain = directory+"/domain.pddl";
	}

	/**
	 * Set test samples
	 * @param pos The positive test sample
	 * @param neg The negative test sample
	 */
	public void setSamples(Sample pos, Sample neg) {
		this.testPositive = pos;
		this.testNegative = neg;
	}

	/**
	 * Generate PDDL operator
	 * @param A The automaton
	 * @param reduceMapping The mapping ante
	 * @param reduceMappingPost The mapping post
	 * @return The action model
	 * @throws BlocException
	 */
	public  Individual generatePDDLOperator(
			FiniteStateAutomata A,
			Mapping reduceMapping,
			Mapping reduceMappingPost) throws BlocException {
		As = A;
		mappingPosts = reduceMappingPost;
		mappings = reduceMapping;

		List<Symbol> symbols = new ArrayList<>();

		//Map actions with preconditions/effects
		Map<Symbol, List<Observation>> preconditions = new HashMap<>();
		for(Symbol s : this.getActionTypes()){
			preconditions.put(s, new ArrayList<>());
		}

		//For each operator
		for(Symbol s : this.getActionTypes()){

			//Learn instanciate
			for(Symbol a : getActionOfType(actions, s)) {
				Observation tmp = new Observation();
				if(mappings.contains(a)) {
					tmp = learnPrecondition(
							a,
							mappings.getSet(a),
							As);
				}
				Map<String, String> map = a.mapping();
				//Gen contains all preconditions for action a for each states
				//where a was feasible

				Observation gen =  new Observation();
				for(Symbol pred : tmp.getPredicatesSymbols()) {
					switch(tmp.getValue(pred)) {
					case TRUE:
						if(!symbols.contains(pred.generalize(map))) {
							symbols.add(pred.generalize(map));
						}
						gen.addTrueObservation(pred.generalize(map));
						break;
					case FALSE:
						if(!symbols.contains(pred.generalize(map))) {
							symbols.add(pred.generalize(map));
						}
						gen.addFalseObservation(pred.generalize(map));
						break;
					case MISSED:
						if(!symbols.contains(pred.generalize(map))) {
							symbols.add(pred.generalize(map));
						}
						gen.missingPredicate(pred.generalize(map));
					default:
						break;
					}
				}
				if(! gen.isEmpty()) {
					preconditions.get(s).add(gen);
				}
			}//Instanciate
		}

		//Generalize preconditions
		Map<Symbol, Observation> genPreconditions = new HashMap<>();
		for(Symbol s : getActionTypes()) {
			genPreconditions.put(s, new Observation(preconditions.get(s), symbols));
		}

		//Learn postconditions
		Map<Symbol, List<Observation>> postconditions = new HashMap<>();
		for(Symbol s : this.getActionTypes()){
			postconditions.put(s, new ArrayList<>());
		}
		symbols = new ArrayList<>();
		for(Symbol s : this.getActionTypes()){
			//Learn instanciate
			for(Symbol a : getActionOfType(actions, s)) {
				Observation tmp = new Observation();
				if(mappings.contains(a)) {
					tmp = learnPostcondition(a,
							mappings.getSet(a),
							mappingPosts.getSet(a),
							As);
				}
				Map<String, String> map = a.mapping();
				//Gen contains all postconditions for action a for each states
				//where a was feasible
				Observation gen =  new Observation();
				for(Symbol pred : tmp.getPredicatesSymbols()) {
					switch(tmp.getValue(pred)) {
					case TRUE:
						if(!symbols.contains(pred.generalize(map))) {
							symbols.add(pred.generalize(map));
						}
						gen.addTrueObservation(pred.generalize(map));
						break;
					case FALSE:
						if(!symbols.contains(pred.generalize(map))) {
							symbols.add(pred.generalize(map));
						}
						gen.addFalseObservation(pred.generalize(map));
						break;
					case MISSED:
						if(!symbols.contains(pred.generalize(map))) {
							symbols.add(pred.generalize(map));
						}
						gen.missingPredicate(pred.generalize(map));
					default:
						break;
					}
				}
				if(! gen.isEmpty()) {
					postconditions.get(s).add(gen);
				}
			}//Instanciate
		}//All instanciate

		//Generalize postconditions
		Map<Symbol, Observation> genPostconditions = new HashMap<>();
		for(Symbol s : getActionTypes()) {
			genPostconditions.put(s, new Observation(postconditions.get(s),
					symbols));
		}

		List<Symbol> genAct = new ArrayList<>();
		for(Symbol a : this.actions) {
			if(! genAct.contains(a)) {
				genAct.add(a);
			}

		}
		return new Individual(this.predicates, genAct,
				genPreconditions,
				genPostconditions);
	}
	
	
	/**
	 * Refine operator
	 * @param ind The action model to refine
	 * @param A The automaton
	 * @param reduceMapping The mapping ante
	 * @param reduceMappingPost The mapping post
	 * @return The refined action model
	 * @throws BlocException
	 */
	public Individual refineOperator(
			Individual ind,
			FiniteStateAutomata A,
			Mapping reduceMapping,
			Mapping reduceMappingPost) throws BlocException {
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>> p = ind.decode();
		Map<Symbol, Observation> preconditions = p.getX();
		Map<Symbol, Observation> postconditions = p.getY();
		int maxIter = 100;
		boolean b = true;
		while(b && maxIter > 0) {
			b = false;
			maxIter--;

			//Refine postcondition
			for(Symbol s : this.getActionTypes()){

				Observation post = postconditions.get(s);
				Observation newPost = refinePostcondition(
						s,reduceMapping,preconditions,
						post.clone(), A);
				newPost.missedToAny();
				postconditions.put(s, newPost);
				b |= !(newPost.equals(post));
			}

			//Refine precondition
			for(Symbol s : this.getActionTypes()){
				Observation prec_ = preconditions.get(s);
				Observation post = postconditions.get(s);
				Observation newPrec = refinePrecondition(
						prec_.clone(), post);
				newPrec.missedToAny();
				preconditions.put(s, newPrec);
				b |= !(newPrec.equals(prec_));
			}

		}

		List<Symbol> genAct = new ArrayList<>();
		for(Symbol a : this.actions) {
			if(! genAct.contains(a)) {
				genAct.add(a);
			}

		}

		return new Individual(
				this.predicates,
				genAct,
				preconditions,
				postconditions);
	}

	/**
	 * compute all operator's parameters
	 *
	 * @return List of operator's parameters
	 */
	public List<Symbol> getActionTypes(){
		List<Symbol> types = new ArrayList<>();
		for(Symbol action : this.actions){
			Symbol a = action.generalize();
			if(! types.contains(a)){
				types.add(a);
			}
		}
		return types;
	}

	/**
	 * compute all predicate's parameters
	 *
	 * @return List of predicate's parameters
	 */
	public List<Symbol> getPredicateTypes(){
		List<Symbol> types = new ArrayList<>();
		for(Symbol pred : this.predicates){
			Symbol a = pred.generalize();
			if(! types.contains(a)){
				types.add(a);
			}
		}
		return types;
	}

	/**
	 * Learn precondition from Mapping
	 *
	 * @param a The action
	 * @param reduceMapping The mapping
	 * @param A fsa
	 * @return a's precondtions
	 * @throws BlocException
	 */
	public Observation learnPrecondition(Symbol a, Map<Integer,
			Observation> reduceMapping,
			FiniteStateAutomata A)
					throws BlocException{
		List<Symbol> lp = new ArrayList<>();
		List<Observation> Q = new ArrayList<>();
		//System.out.println(a+" "+reduceMapping);
		for(Map.Entry<Integer, Observation> entry : reduceMapping.entrySet()){
			int q = entry.getKey();
			List<Symbol> list = A.getPossibleBlocTransition(q);
			if(list.contains(a)){
				boolean bool = false;
				Observation obs = new Observation();
				for(Symbol predicate : entry.getValue().
						getPredicatesSymbols()){

					if(a.compatible(predicate)){
						bool = true;
						obs.addObservation(predicate,
								entry.getValue().getValue
								(predicate));
						if(!lp.contains(predicate)){
							lp.add(predicate);
						}
					}
				}
				if(bool){
					if(! Q.contains(obs) && !obs.isEmpty()){
						Q.add(obs);
					}
				}
			}
		}
		if(Q.isEmpty()) {
			return new Observation();
		}
		Observation tmp = new Observation(Q, lp);
		return tmp;
	}

	/**
	 *
	 * Learn effects from Mappings
	 *
	 * @param action The action
	 * @param reduceMapping The mapping ante
	 * @param reduceMappingPost The mapping post
	 * @param A fsa
	 * @return a's effects
	 *
	 * @throws BlocException
	 */
	public Observation learnPostcondition(
			Symbol action,
			Map<Integer,Observation>reduceMapping,
			Map<Integer,Observation>reduceMappingPost,
			FiniteStateAutomata A)
					throws BlocException{
		List<Symbol> lp = new ArrayList<>();
		List<Observation> Q = new ArrayList<>();
		for(Map.Entry<Integer, Observation> entry : reduceMapping.entrySet()){
			int q = entry.getKey();
			List<Symbol> list = A.getPossibleBlocTransition(q);
			if(list.contains(action)){
				boolean bool = false;
				Observation obs = new Observation();
				for(Symbol predicate : entry.getValue().
						getPredicatesSymbols()){
					if(action.compatible(predicate)){
						bool = true;
						obs.addObservation(predicate,
								entry.getValue().getValue
								(predicate));
						if(!lp.contains(predicate)){
							lp.add(predicate);
						}
					}
				}
				if(bool){
					int i = A.getBlocTransition(A.getPartition().
							getBloc(entry.getKey()),action).min();
					Observation o = reduceMappingPost.get(i);
					Observation o2 = obs.diff(o);
					if(! Q.contains(o2) && !o2.isEmpty()){
						Q.add(o2);
					}
				}
			}
		}
		Observation postcondition = new Observation(Q, lp);
		return postcondition;
	}

	/**
	 * Effects refinement from fsa
	 * @param action the operator
	 * @param reduceMapping Mapping post
	 * @param preconditions all preconditions
	 * @param postcondition action's effects
	 * @param A fsa
	 * @return Refined action's effect
	 * @throws BlocException
	 */
	public Observation refinePostcondition(Symbol action,
			Mapping reduceMapping,
			Map<Symbol, Observation>preconditions,
			Observation postcondition,
			FiniteStateAutomata A)
					throws BlocException{
		for(int q : A.blocStates()){
			List<Symbol> list = A.getPossibleBlocTransition(q);
			if(getListActionType(list).contains(action)){
				for(Symbol action2 : getActionOfType(list, action)){
					Observation effect = postcondition.
							instanciate(reverseParamMap(action2.mapping()));
					Observation prec = preconditions.get(action).
							instanciate(reverseParamMap(action2.mapping()));
					Observation currentState = reduceMapping.getStates(q, action2);
					int qNext = A.getBlocTransition
							(A.getPartition().getBloc(q), action2).min();
					for(Symbol nextAction : A.getPossibleBlocTransition(qNext)) {
						Observation precNext = preconditions.get(nextAction.generalize()).
								instanciate(reverseParamMap(nextAction.mapping()));
						//System.out.println(currentState);
						currentState = currentState.addEffect(prec);
						currentState = currentState.addEffect(effect);
						for(Symbol p : precNext.getPredicatesSymbols()) {
							if(action2.compatible(p)) {
								switch(precNext.getValue(p)) {
								case TRUE:
									switch(currentState.getValue(p)) {
									case FALSE:
										postcondition.addTrueObservation(p.generalize(
												action2.mapping()));
										break;
									default:
										break;
									}
									break;
								case FALSE:
									switch(currentState.getValue(p)) {
									case TRUE:
										postcondition.addFalseObservation(p.generalize(
												action2.mapping()));
										break;
									default:
										break;
									}
									break;
								default:
									break;
								}
							}
						}
					}
				}
			}
		}
		return postcondition;
	}

	/**
	 * Preconditions refinement
	 *
	 * @param precondition A precondition
	 * @param postcondition An effect

	 * @return Refined preconditions
	 * @throws BlocException
	 */
	public Observation refinePrecondition(
			Observation precondition,
			Observation postcondition)
					throws BlocException{
		Observation obs = new Observation(precondition);

		for(Symbol p : postcondition.getPredicatesSymbols()) {
			switch(postcondition.getValue(p)) {
			case FALSE:
				obs.addTrueObservation(p);
				break;
			default:
				break;
			}
		}

		return obs;
	}

	/**
	 * Return the set of operators
	 * @param actions Operators
	 * @return  All actions
	 */
	public List<Symbol> getListActionType(List<Symbol> actions) {
		List<Symbol> res = new ArrayList<>();

		for(Symbol action : actions) {
			Symbol type = action.generalize();
			if(! res.contains(type)) {
				res.add(type);
			}
		}

		return res;
	}

	/**
	 * Return all actions instanciating type
	 * @param actions set of actions
	 * @param type the operators
	 * @return A set of actions
	 */
	public List<Symbol> getActionOfType(List<Symbol> actions, Symbol type) {
		List<Symbol> res = new ArrayList<>();

		for(Symbol action : actions) {
			Symbol actionType = action.generalize();
			if(type.equals(actionType)) {
				res.add(action);
			}
		}

		return res;
	}

	/**
	 * Return the key pointing to the value
	 * @param map The map
	 * @param value The value
	 * @return The key
	 */
	public String getKey(Map<String, String> map, String value) {
		for(Map.Entry<String, String> entry : map.entrySet()) {
			if(entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

	/**
	 * Generate the content of the pddl action model file
	 * @param preconditions All preconditions
	 * @param postconditions All effects
	 * @return Pdd action model
	 */
	public String generation(Map<Symbol, Observation> preconditions,
			Map<Symbol, Observation> postconditions) {

		String res = "(define (domain "+this.name+")\n";
		res += "(:requirements :strips :typing :negative-preconditions)\n";

		//Types
		res += "(:types\n";
		res += this.types.toString();
		res += ")\n";
		//Predicates
		res += "(:predicates\n";
		List<Symbol> tmp = new ArrayList<>();
		for(Symbol s : this.predicates) {
			Symbol ss = s.generalize();
			if(! tmp.contains(ss)) {
				res += "\t"+ss.toStringType()+"\n";
				tmp.add(ss);
			}
		}
		res += ")\n";

		//Actions
		tmp = new ArrayList<>();
		for(Symbol action : this.actions){
			Symbol actionType = action.generalize();
			if(! tmp.contains(actionType)){
				tmp.add(actionType);
				res += "(:action "+actionType.getName()+"\n";
				//Parameters
				res += "\t:parameters (";
				for(String s : actionType.getListParametersType()) {
					res += s+" ";
				}
				res += ")\n";

				//Precondition
				res += "\t:precondition (and";
				for(Symbol pred : preconditions.get(actionType).
						getPredicatesSymbols()) {
					switch(preconditions.get(actionType).
							getValue(pred)) {
							case TRUE:
								res += "\n\t"+pred;
								break;
							case FALSE:
								res += "\n\t(not"+pred+")";
								break;
							case ANY:
								break;
							default:
								break;
					}
				}
				res += ")\n";

				//Postcondition
				res += "\t:effect (and";
				for(Symbol post : postconditions.get(actionType).
						getPredicatesSymbols()) {
					switch(postconditions.get(actionType).
							getValue(post)) {
							case TRUE:
								res += "\n\t"+post;
								break;
							case FALSE:
								res += "\n\t(not"+post+")";
								break;
							case ANY:
								break;
							default:
								break;
					}
				}
				res += ")\n";

				res += ")\n";
			}
		}

		res +=")\n";
		return res;
	}

	/**
	 * Reverse Key Value of a Map<String String>
	 * @param param The map
	 * @return A reversed map
	 */
	public static Map<String, String> reverseParamMap(Map<String, String> param) {
		Map<String, String> newParam = new HashMap<>();
		for(Map.Entry<String, String> entry : param.entrySet()) {
			newParam.put(entry.getValue(), entry.getKey());
		}
		return newParam;
	}

	/**
	 * Compute the fitness of an individual
	 * @param indiv the individual
	 * @param data the observations
	 * @param pos I+
	 * @param neg I-
	 * @param is The initial state
	 * @return fitness score
	 */
	public float fitness(
			Individual indiv, Sample pos, Sample neg, 
			List<CompressedNegativeExample> compressed, String is) {
		//System.out.println("PASS");
		float fit;

		ObservedExample obs = (ObservedExample) pos.getExamples().get(0);
		Observation initial = obs.getInitialState();
		
		float fitPos = this.fitPositive(indiv, pos, initial);
		float fitNeg = this.fitNegative(indiv, neg, compressed, initial);
		float fitPrec = this.fitPrecondition(indiv, pos);
		float fitPost = this.fitPostcondition(indiv, pos);
		
		fit = (fitPos + fitNeg + fitPrec + fitPost);
		return fit;
	}

	public void printFitnessComponents(
			Individual indiv, Sample pos, Sample neg, 
			List<CompressedNegativeExample> compressed, String is) {

		float fit;

		ObservedExample obs = (ObservedExample) pos.getExamples().get(0);
		Observation initial = obs.getInitialState();
		
		float fitPos = this.fitPositive(indiv, pos, initial);
		float fitNeg = this.fitNegative(indiv, neg, compressed, initial);
		float fitNeg2 = this.fitNegative(indiv, neg, initial);
		float fitPrec = this.fitPrecondition(indiv, pos);
		float fitPost = this.fitPostcondition(indiv, pos);
		//System.out.println(fitPos+" "+fitNeg2+" "+fitPrec+" "+fitPost);
		System.out.println((fitPos+fitNeg+fitPrec+fitPost)+": "+fitPos+" "+fitNeg+" "+fitPrec+" "+fitPost);
		//return fit;
	}
	/**
	 * Compute fitness for preconditions
	 *
	 * @param individual The model
	 * @param data The observations
	 * @return fitness score
	 */
	public float fitPrecondition(
			Individual individual,
			Sample data) {
		Pair<Float, Float> p = new Pair<>((float)0, (float)0);
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		Map<Symbol, Observation> preconditions2 = new HashMap<>();
		Map<Symbol, Observation> postconditions2 = new HashMap<>();
		for(Symbol act : this.actions) {
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(reverseParamMap( act.mapping()));
			prec.removeAny();
			post.removeAny();
			preconditions2.put(act, prec);
			postconditions2.put(act, post);
		}
		
		int fit=0, err=0;
		for(Example plan : data.getExamples()) {
			ObservedExample obs = (ObservedExample) plan;
			for(int i = 0; i< obs.size(); i++) {
				Symbol act = obs.get(i);
				Observation ante = obs.ante(i);
				Observation prec = preconditions2.get(act);
				for(Symbol prop : prec.getPredicatesSymbols()) {
					switch(prec.getValue(prop)) {
					case TRUE:
						switch(ante.getValue(prop)) {
						case TRUE:
							fit++;
							break;
						case FALSE:
							err++;
							break;
						default:
							break;
						}
						break;
					case FALSE:
						switch(ante.getValue(prop)) {
						case FALSE:
							fit++;
							break;
						case TRUE:
							err++;
							break;
						default:
							break;
						}
						break;
					default:
						break;
					}
				}
				
			}
		}
		
		/*data.getExamples().forEach((plan) ->{
			ObservedExample obs = (ObservedExample) plan;
			for(int i = 0; i< obs.size(); i++) {
				Symbol act = obs.get(i);
				Observation ante = obs.ante(i);
				Observation prec = preconditions2.get(act);
				p.setX(prec.countEqual(ante) + p.getX());
				p.setY(prec.countDifferent(ante) + p.getY());
			}
		});*/
		//return p.getX() - p.getY();
		return fit-err;
	}

	/**
	 * Compute fitness for positive samples
	 *
	 * @param individual The model
	 * @param data The observations
	 * @return fitness score
	 */
	public int fitPositive(
			Individual individual,Sample data, Observation initial) {
		int res = 0;
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		Map<Symbol, Observation> preconditions2 = new HashMap<>();
		Map<Symbol, Observation> postconditions2 = new HashMap<>();
		for(Symbol act : this.actions) {
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(reverseParamMap( act.mapping()));
			prec.removeAny();
			post.removeAny();
			preconditions2.put(act, prec);
			postconditions2.put(act, post);
		}
		for(Example plan : data.getExamples()){
			Observation current = initial.clone();
			for(int i = 0; i< plan.size(); i++) {
				Symbol act = plan.get(i);
				Observation prec = preconditions2.get(act);
				//System.out.println(act+" # "+prec+" # "+current+" # "+current.contains(prec));
				if(current.contains(prec)) {
					res++;
					Observation eff = postconditions2.get(act);
					for(Symbol e : eff.getPredicatesSymbols()) {
						switch(eff.getValue(e)) {
						case TRUE:
							//System.out.println("true "+e);
							current.addTrueObservation(e);
							break;
						case FALSE:
							//System.out.println("false "+e);
							current.addFalseObservation(e);
							break;
						default:
							break;
						}
					}
				}else {
					break;
				}
			}
		}
		return res;
	}

	/**
	 * Compute fitness for negative samples
	 *
	 * @param individual The model
	 * @param data The observations
	 * @return fitness score
	 */
	public int fitNegative(
			Individual individual,Sample data, Observation initial) {
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		Map<Symbol, Observation> preconditions2 = new HashMap<>();
		Map<Symbol, Observation> postconditions2 = new HashMap<>();
		for(Symbol act : this.actions) {
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(reverseParamMap( act.mapping()));
			prec.removeAny();
			post.removeAny();
			preconditions2.put(act, prec);
			postconditions2.put(act, post);
		}
		int res = 0;
		//List<List<Symbol>> dejaVu = new ArrayList<>();
		for(Example plan : data.getExamples()){
			/*if(!dejaVu.contains(plan)) {
				dejaVu.add(plan);
			}*/
			Observation current = initial.clone();
			for(int i = 0; i< plan.size(); i++) {
				Symbol act = plan.get(i);
				Observation prec = preconditions2.get(act);
				if(current.contains(prec)) {
					if(i == plan.size() - 1) {
						break;
					}
					Observation eff = postconditions2.get(act);
					for(Symbol e : eff.getPredicatesSymbols()) {
						switch(eff.getValue(e)) {
						case TRUE:
							current.addTrueObservation(e);
							break;
						case FALSE:
							current.addFalseObservation(e);
							break;
						default:
							break;
						}
					}
				}else {
					if(i == plan.size() - 1) {
						res++;
					}else {
						break;
					}
				}
			}
		}
		return res;
	}

	/**
	 * Compute fitness for negative samples
	 *
	 * @param individual The model
	 * @param data The observations
	 * @return fitness score
	 */
	public int fitNegative(
			Individual individual, Sample data,
			List<CompressedNegativeExample> compressed, Observation initial) {
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		//System.out.println(compressed.size());
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		Map<Symbol, Observation> preconditions2 = new HashMap<>();
		Map<Symbol, Observation> postconditions2 = new HashMap<>();
		//System.out.println(preconditions.keySet());
		//Map<Symbol, List<Observation>> infeasible = new HashMap<>();
		for(Symbol act : DomainLearning.getAllActions(data)) {
			//infeasible.put(act, new ArrayList<>());
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(reverseParamMap( act.mapping()));
			prec.removeAny();
			prec.removeMissed();
			post.removeAny();
			post.removeMissed();
			preconditions2.put(act, prec);
			postconditions2.put(act, post);
		}
		int res = 0;
		for(CompressedNegativeExample plan : compressed){
			Observation current = initial.clone();
			//System.out.println(plan.getPrefix());
			for(int i = 0; i < plan.getPrefix().size(); i++) {
				//Test Negatives
				//System.out.println(i);
				for(Example n : plan.getNegativeIndex(i)) {
					/*System.out.println(n);
					Scanner sc = new Scanner(System.in);
					sc.next();*/
					Observation current2 = current.clone();
					for(Symbol a : n.getActionSequences()) {
						Observation prec = preconditions2.get(a);
						/*if(infeasible.get(a).contains(current2)) {
							res++;
							break;
						}else */
						if(!current2.contains(prec)) {
							//System.out.println(a);
						//	infeasible.get(a).add(current2);
							res++;
							break;
						} else {
							Observation eff = postconditions2.get(a);
							for(Symbol e : eff.getPredicatesSymbols()) {
								switch(eff.getValue(e)) {
								case TRUE:
									current2.addTrueObservation(e);
									break;
								case FALSE:
									current2.addFalseObservation(e);
									break;
								default:
									break;
								}
							}
						}
					}
				}
				if(i < plan.getPrefix().size()) {
					//Test Positive
					Symbol a = plan.getPrefix().get(i);
					Observation prec = preconditions2.get(a);
					if(!current.contains(prec)) {
						break;
					}
					//Apply Effect
					Observation eff = postconditions2.get(a);
					for(Symbol e : eff.getPredicatesSymbols()) {
						switch(eff.getValue(e)) {
						case TRUE:
							current.addTrueObservation(e);
							break;
						case FALSE:
							current.addFalseObservation(e);
							break;
						default:
							break;
						}
					}
				}
			}
		}
		return res;
	}
	
	/*public int fitNegative(
			Individual individual,List<Symbol> plan, Observation initial) {
		int res = 0;
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		Map<Symbol, Observation> preconditions2 = new HashMap<>();
		Map<Symbol, Observation> postconditions2 = new HashMap<>();
		for(Symbol act : this.actions) {
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(reverseParamMap( act.mapping()));
			preconditions2.put(act, prec);
			postconditions2.put(act, post);
		}
		Observation current = initial.clone();
		for(int i = 0; i< plan.size(); i++) {
			Symbol act = plan.get(i);
			Observation prec = preconditions2.get(act);
			if(current.contains(prec)) {
				Observation eff = postconditions2.get(act);
				for(Symbol e : eff.getPredicatesSymbols()) {
					switch(eff.getValue(e)) {
					case TRUE:
						current.addTrueObservation(e);
						break;
					case FALSE:
						current.addFalseObservation(e);
						break;
					default:
						break;
					}
				}
			}else {
				if(i == plan.size() - 1) {
					res++;
				}else {
					break;
				}
			}
		}

		return res;
	}*/

	/**
	 * Compute fitness for negative samples
	 *
	 * @param individual The model
	 * @param data The observations
	 * @return fitness score
	 * @throws IOException 
	 */
	/*public int fitNegative2(Individual individual,Sample data, Observation initial)
			throws IOException {
		String tmpDomain = this.directory+"/tmp"+this.name+".pddl";
		BufferedWriter bw = new BufferedWriter(	new FileWriter(tmpDomain));
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		bw.write(generation(preconditions, postconditions));
		bw.close();
		BlackBox simulator = new BlackBox(tmpDomain,initialState);
		List<Symbol> instance = simulator.getAllActions();
		Map<Symbol, Observation> preconditions2 = new HashMap<>();
		Map<Symbol, Observation> postconditions2 = new HashMap<>();
		for(Symbol act : instance) {
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(reverseParamMap( act.mapping()));
			preconditions2.put(act, prec);
			postconditions2.put(act, post);
		}
		boolean simulatorUsable = simulator.consistant(preconditions2, 
															postconditions2);
		//System.out.println("Coucou");
		//System.exit(1);
		int res = 0;
		//System.out.println((new BlackBox(tmpDomain,initialState)).getAllActions());
		for(List<Symbol> example : data.getSymbols()){
			if(simulatorUsable && simulator.testable(example)) {
				System.out.println("you");
				simulator.reInit();
				for(int i = 0; i< example.size(); i++) {
					if(simulator.accept(example.get(i))) {
						//System.out.println(i+" "+example.get(i));
						/*Node currentState = new Node(simulator.getCurrentState());
		            			Observation obs = new Observation(simulator.getAllPredicates());
		            			for(Symbol s : simulator.getSymbolsState(currentState)){
		            				obs.addTrueObservation(s);
		            			}
		            			for(Symbol s : simulator.getPositiveStaticPredicate()){
		            				obs.addTrueObservation(s);
		            			}
		            			System.out.println(obs);*/
						/*continue;
					} else {
						//System.out.println("you");
						if(i < example.size()-1) {
							res += 0f;
						} else {
							res += 1f;
						}
					}
				}
				res += 0f;
			} else {
				res += this.fitNegative(individual, example, initial);
			}
		}
		return res;
	}*/

	/*public int fitNegative3(Individual individual,Sample data, Observation initial)
			throws IOException {
		String tmpDomain = this.directory+"/tmp"+this.name+".pddl";
		BufferedWriter bw = new BufferedWriter(	new FileWriter(tmpDomain));
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		bw.write(generation(preconditions, postconditions));
		bw.close();
		TestPlanner.init(new BlackBox(tmpDomain,initialState));
		int res = 0;
		for(Example example : data.getExamples()){
			try {
				res += TestPlanner.negFit(tmpDomain, initialState, example);
			} catch (InstantiationException e) {
				res += 10000000;
			}
		}
		return res;
	}*/

	/**
	 * Compute fitness for effects
	 *
	 * @param individual The model
	 * @param data The observations
	 * @return fitness score
	 */
	public float fitPostcondition(
			Individual individual,
			Sample data) {
		Pair<Float, Float> p = new Pair<>((float)0, (float)0);
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		Map<Symbol, Observation> preconditions2 = new HashMap<>();
		Map<Symbol, Observation> postconditions2 = new HashMap<>();
		for(Symbol act : this.actions) {
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(reverseParamMap( act.mapping()));
			prec.removeAny();
			post.removeAny();
			preconditions2.put(act, prec);
			postconditions2.put(act, post);
		}
		
		int fit=0, err=0;
		for(Example plan : data.getExamples()) {
			ObservedExample obs = (ObservedExample) plan;
			for(int i = 0; i< obs.size(); i++) {
				Symbol act = obs.get(i);
				Observation ante = obs.ante(i);
				Observation next = obs.post(i);
				Observation post = postconditions2.get(act);
				for(Symbol eff : post.getPredicatesSymbols()) {
					switch(post.getValue(eff)) {
					case TRUE:
						switch(next.getValue(eff)) {
						case TRUE:
							if(ante.getValue(eff)!=post.getValue(eff) /*&&
								ante.getValue(eff)!=Observation.Value.MISSED*/) {
								fit++;
							}
							break;
						case FALSE:
							err++;
							break;
						default:
							break;
						}
						break;
					case FALSE:
						switch(next.getValue(eff)) {
						case FALSE:
							if(ante.getValue(eff)!=post.getValue(eff)/*&&
								ante.getValue(eff)!=Observation.Value.MISSED*/) {
								fit++;
							}
							break;
						case TRUE:
							err++;
							break;
						default:
							break;
						}
						break;
					default:
						break;
					}
				}
				/*p.setX(post.countEqual(next) + p.getX() - post.countEqual(ante.common(next)));
				p.setY(post.countDifferent(next) + p.getY());*/
			}
		}
		data.getExamples().forEach((plan) ->{
			ObservedExample obs = (ObservedExample) plan;
			for(int i = 0; i< obs.size(); i++) {
				Symbol act = obs.get(i);
				Observation ante = obs.ante(i);
				Observation next = obs.post(i);
				Observation post = postconditions2.get(act);
				p.setX(post.countEqual(next) + p.getX() - post.countEqual(ante.common(next)));
				p.setY(post.countDifferent(next) + p.getY());
			}
		});
		//return p.getX() - p.getY();
		return fit-err;
	}

	/**
	 * Compute the tabu search
	 *
	 * @param indiv initial candidate
	 * @param data the observtions
	 * @param pos I+
	 * @param neg I-
	 * @param is The initial states
	 * @param size The buffer size
	 * @param epoch The number of epochs
	 * @param tabou The tabu list
	 * @return Best candidate
	 */
	public Individual localSearch(
			Individual indiv, Sample pos, Sample neg, 
			List<CompressedNegativeExample> compressed, String is,
			int size, int epoch,
			List<Individual> tabou) {
		IndividualsQueue queue = new IndividualsQueue(size, tabou);
		float fit = this.fitness(indiv, pos, neg, compressed, is);
		queue.add(indiv, fit);
		tabou.remove(indiv);
		int i =0;
		//this.printFitnessComponents(indiv, pos, neg, compressed, is);
		for(i =0; i < epoch; i ++) {
			try {
				Individual choosen = queue.next();

				List<Individual> neigh = choosen.neighbors();
				//System.err.println(i+" "+neigh.size()+" "+this.fitness(choosen, pos, neg, compressed, is));
				for(Individual ind : neigh) {
					if(! queue.inTabou(ind)) {
						//long a1 = System.currentTimeMillis();
						fit = this.fitness(ind, pos, neg, compressed, is);
						queue.add(ind, fit);
						//long a2 = System.currentTimeMillis();
						//System.out.println((a2-a1));
						/*if(this.fitnessScores.containsKey(ind)) {
							queue.add(ind, this.fitnessScores.get(ind));
						} else {
							fit = this.fitness(ind, pos, neg, compressed, is);
							this.fitnessScores.put(ind, fit);
							queue.add(ind, fit);
						}*/
					}
				}
			}catch(IllegalArgumentException e){
				break;
			}
		}
		try {
			queue.next();
		}catch(IllegalArgumentException e){

		}
		Individual res = tabou.get(0);
		float fitRef = this.fitness(res, pos, neg, compressed, is);
		for(Individual ind : tabou) {
			fit = this.fitness(ind, pos, neg, compressed, is);
			if(fit > fitRef) {
				res = ind;
				fitRef = fit;
			}
		}
		//this.printFitnessComponents(res, pos, neg, compressed, is);
		//System.out.println(fitRef);
		return res;
	}

	/**
	 * Compute the tabu search
	 *
	 * @param indiv initial candidate
	 * @param data the observtions
	 * @param pos I+
	 * @param neg I-
	 * @param is The initial states
	 * @param size The buffer size
	 * @param epoch The number of epochs
	 * @param tabou The tabu list
	 * @return Best candidate
	 */
	public Individual localSearchThread(
			Individual indiv, Sample pos, Sample neg, 
			List<CompressedNegativeExample> compressed, String is,
			int size, int epoch,
			List<Individual> tabou) {
		IndividualsQueue queue = new IndividualsQueue(size, tabou);
		float fit = this.fitness(indiv, pos, neg, compressed, is);
		queue.add(indiv, fit);
		tabou.remove(indiv);
		int i =0;
		//this.printFitnessComponents(indiv, pos, neg, compressed, is);
		for(i =0; i < epoch; i ++) {
			try {
				Individual choosen = queue.next();

				List<Individual> neigh = choosen.neighbors();
				//System.err.println(i+" "+neigh.size());
				for(Individual ind : neigh) {
					if(! queue.inTabou(ind)) {
						if(this.fitnessScores.containsKey(ind)) {
							queue.add(ind, this.fitnessScores.get(ind));
						} else {
							fit = this.fitness(ind, pos, neg, compressed, is);
							this.fitnessScores.put(ind, fit);
							queue.add(ind, fit);
						}
					}
				}
			}catch(IllegalArgumentException e){
				break;
			}
		}
		try {
			queue.next();
		}catch(IllegalArgumentException e){

		}
		Individual res = tabou.get(0);
		float fitRef = this.fitness(res, pos, neg, compressed, is);
		for(Individual ind : tabou) {
			fit = this.fitness(ind, pos, neg, compressed, is);
			if(fit > fitRef) {
				res = ind;
				fitRef = fit;
			}
		}
		//this.printFitnessComponents(res, pos, neg, compressed, is);
		//System.out.println(fitRef);
		return res;
	}
	/**
	 * 
	 */
	public void initFitness() {
		this.fitnessScores = new ConcurrentHashMap<>();
	}
	
	/**
	 * 
	 * @param individual
	 * @param f
	 */
	public void addFitness(Individual individual, float f) {
		this.fitnessScores.put(individual, f);
	}
	
	/**
	 * 
	 * @param i
	 * @return
	 */
	public boolean hasIndividual(Individual i) {
		return this.fitnessScores.containsKey(i);
	}
	
	/**
	 * 
	 * @param i
	 * @return
	 */
	public float getFitness(Individual i) {
		return this.fitnessScores.get(i);
	}
	
	/**
	 * 
	 * @return
	 */
	public int sizeFitness() {
		return this.fitnessScores.keySet().size();
	}
	
	/**
	 * 
	 * @param S
	 * @return
	 */
	public static List<Symbol> getAllActions(Sample S) {
		List<Symbol> l = new ArrayList<>();
		S.getExamples().forEach(ex -> {
			ex.getActionSequences().forEach(s -> {
				if(!l.contains(s)) {
					l.add(s);
				}
			});
		});
		return l;
	}
}
