/**
 * 
 */
package learning;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fsm.Sample;
import fsm.Symbol;
import learning.Individual;
import learning.Observation;
import learning.temporal.BufferTemporal;
import simulator.temporal.TemporalExample;

/**
 * @author Maxence Grand
 *
 */
public class DomainLearningThread extends Thread{
	/**
	 * 
	 */
	private DomainLearning learner;
	/**
	 * 
	 */
	private List<Individual> indiv;
	/**
	 * 
	 */
	private Map<List<Symbol>,List<Observation>> data;
	/**
	 * 
	 */
	private Sample pos;
	/**
	 * 
	 */
	private Sample neg;
	/**
	 * 
	 */
	private List<CompressedNegativeExample> compressed;
	/**
	 * 
	 */
	private String is;
	/**
	 * 
	 */
	private Map<Symbol,Float> duration;
	/**
	 * 
	 */
	private boolean twoOp;
	/**
	 * 
	 */
	private BufferTemporal buffer;
	int nb;
	
	/**
	 * Constructs 
	 * @param learner
	 * @param indiv
	 * @param data
	 * @param pos
	 * @param neg
	 * @param compressed
	 * @param is
	 * @param duration
	 * @param twoOp
	 */
	public DomainLearningThread(
			DomainLearning learner, 
			List<Individual> indiv,
			Sample pos, 
			Sample neg,
			List<CompressedNegativeExample> compressed, 
			String is, 
			Map<Symbol, Float> duration, 
			boolean twoOp,
			BufferTemporal buffer, 
			int nb) {
		this.learner = learner;
		this.indiv = indiv;
		this.pos = pos;
		this.neg = neg;
		this.compressed = compressed;
		this.is = is;
		this.duration = duration;
		this.twoOp = twoOp;
		this.buffer = buffer;
		this.nb=nb;
	}


	/**
	 * 
	 * @return
	 */
	public float fitness(Individual i) {

		float fit=0f;

		try {

			ObservedExample obs = (ObservedExample) pos.getExamples().get(0);
			Observation initial = obs.getInitialState();
			
			float fitPos = learner.fitPositive(i, pos, initial);
			float fitNeg = learner.fitNegative(i, neg, compressed, initial);
			float fitPrec = learner.fitPrecondition(i, pos);
			float fitPost = learner.fitPostcondition(i, pos);
			
			fit = (fitPos + fitNeg + fitPrec + fitPost);
			return fit;
		} catch(Exception ioe) {
			return Float.MIN_VALUE;
		}
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		for(int i = 0; i<indiv.size(); i++) {
			if(this.buffer.indivWating(indiv.get(i))) {
				float fit = this.fitness(indiv.get(i));
				this.learner.addFitness(indiv.get(i), fit);
			}
		}
	}
}
