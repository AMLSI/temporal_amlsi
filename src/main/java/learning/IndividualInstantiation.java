/**
 * 
 */
package learning;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fsm.Pair;
import fsm.Symbol;

/**
 * @author Maxence Grand
 *
 */
public class IndividualInstantiation {
	/**
	 * 
	 */
	private Map<Symbol, Observation> precondition;
	/**
	 * 
	 */
	private Map<Symbol, Observation> effect;
	
	/**
	 * 
	 * Constructs 
	 * @param individual
	 * @param actions
	 */
	public IndividualInstantiation(Individual individual, List<Symbol> actions) {
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = individual.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		this.precondition = new HashMap<>();
		this.effect = new HashMap<>();
		//System.out.println(actions);
		for(Symbol act : actions) {
			Observation prec = preconditions.get(act.generalize());
			prec = prec.instanciate(DomainLearning.reverseParamMap( act.mapping()));
			Observation post = postconditions.get(act.generalize());
			post = post.instanciate(DomainLearning.reverseParamMap( act.mapping()));
			prec.removeAny();
			post.removeAny();
			precondition.put(act, prec);
			effect.put(act, post);
		}
		//toString();
	}
	
	/**
	 * 
	 * @param s
	 * @return
	 */
	public Observation getPrecond(Symbol s) {
		return precondition.get(s);
	}
	
	/**
	 * 
	 * @param e
	 * @return
	 */
	public Observation getEffect(Symbol s) {
		return this.effect.get(s);
	}
	
	/**
	 * 
	 * @param s
	 * @param o
	 * @return
	 */
	public Observation apply(Symbol s, Observation o) {
		Observation newObs = o.clone();
		Observation e = this.getEffect(s);
		e.getPredicates().forEach((k,v) -> {
			switch(v) {
			case TRUE:
			case FALSE:
				newObs.addObservation(k,v);
				break;
			default:
				break;
			}
		});
		return newObs;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Precondition\n");
		this.precondition.forEach((act, obs) -> {
			builder.append("\t"+act+" : ");
			obs.getPredicates().forEach((k,v)-> {
				switch(v) {
				case TRUE:
					builder.append(k+" ");
					break;
				case FALSE:
					builder.append("not"+k+" ");
					break;
				default:
					break;
				}
			});
			builder.append("\n");
		});
		builder.append("Effect\n");
		this.effect.forEach((act, obs) -> {
			builder.append("\t"+act+" : ");
			obs.getPredicates().forEach((k,v)-> {
				switch(v) {
				case TRUE:
					builder.append(k+" ");
					break;
				case FALSE:
					builder.append("not"+k+" ");
					break;
				default:
					break;
				}
			});
			builder.append("\n");
		});
		return builder.toString();
	}
}
