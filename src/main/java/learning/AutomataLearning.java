package learning;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import fsm.*;
import exception.BlocException;
import simulator.Oracle;

/**
 * This class implements the automata learning module.
 *
 * @author Maxence Grand
 * @version 1.0
 * @since   10-2019
 */
public class AutomataLearning {
	/**
	 * The set of predicates
	 */
	protected List<Symbol> predicates;
	/**
	 * The set of actions
	 */
	protected List<Symbol> actions;
	/**
	 * The oracle used to generate positive and negative samples.
	 */
	protected Oracle sim;
	/**
	 * A set of positive sequences. Used for experimentations.
	 */
	protected Sample testPositive;
	/**
	 * A set of negative sequences. Used for experimentations.
	 */
	protected Sample testNegative;
	/**
	 * The generator used to generate positive and negative samples
	 */
	protected Generator generator;
	/**
	 * The set of pairewise constraints R
	 */
	protected Map<Symbol, List<Symbol>> rules;
	/**
	 * The action model learning module.
	 */
	protected DomainLearning domainLearner;

	/**
	 * The constructor of the object AucleatomataLearning
	 */
	public AutomataLearning(){

	}

	/**
	 * The constructor of the object AutomataLearning
	 * @param act The set of actions.
	 *  */
	public AutomataLearning(List<Symbol> act){
		this.actions = new ArrayList<>();
		for(Symbol a : act){
			this.actions.add(a);
		}

		//Create all pairs
		this.rules = new HashMap<>();
		for(Symbol s1 : this.actions){
			rules.put(s1, new ArrayList<>());
			for(Symbol s2 : this.actions){
				this.rules.get(s1).add(s2);
			}
		}
	}

	/**
	 * The constructor of the object AutomataLearning
	 *
	 * @param pred The set of predicates
	 * @param act The set of actions
	 * @param g The generator
	 * @param domainLearner The action model learning module
	 */
	public AutomataLearning(
			List<Symbol> pred,
			List<Symbol> act,
			Generator g,
			DomainLearning domainLearner
			){

		//The set of predicates
		this.predicates = new ArrayList<>();
		for(Symbol a : pred){
			this.predicates.add(a);
		}

		//The set of actions
		this.actions = new ArrayList<>();
		for(Symbol a : act){
			this.actions.add(a);
		}

		//The genrator
		this.generator = g;

		//All rules
		//Create all pairs
		this.rules = new HashMap<>();
		for(Symbol s1 : this.actions){
			rules.put(s1, new ArrayList<>());
			for(Symbol s2 : this.actions){
				this.rules.get(s1).add(s2);
			}
		}

		//Learning module
		this.domainLearner = domainLearner;
	}

	/**
	 * Set test samples
	 * @param pos A positive test sample
	 * @param neg A negative test sample
	 */
	public void setSamples(Sample pos, Sample neg) {
		this.testPositive = pos;
		this.testNegative = neg;
	}



	/**
	 * Remove obsolete rules
	 * @param example Positive example used to update rules
	 */
	public void removeRules(Example example){
		if(example.size()>1){
			for(int i=1; i<example.size(); i++){
				if(this.rules.containsKey(example.get(i-1))) {
					//System.out.println("Remove "+example.get(i-1)+" "+example.get(i));
					this.rules.get(example.get(i-1)).remove(example.get(i));
					if(this.rules.get(example.get(i-1)).isEmpty()) {
						this.rules.remove(example.get(i-1));
					}
				}
			}
		}
	}

	/**
	 * Remove obsolete rules
	 * @param x Positive example used to update rules
	 * @param y Positive example used to update rules
	 */
	public void removeRules(Symbol x, Symbol y){
		if(this.rules.containsKey(x)) {
			this.rules.get(x).remove(y);
			//System.out.println("Remove "+x+" "+y);
			if(this.rules.get(x).isEmpty()) {
				this.rules.remove(x);
			}
		}
	}
	/**
	 * Print rules
	 */
	public void printRules(){
		this.rules.forEach((k,v) -> {
			v.forEach(a -> System.out.println(k+" "+a));
		});
	}
	
	/**
	 * Check if the automaton rejects all rules
	 *
	 * @param A The fsa
	 * @return True if fsa rejects all rules
	 * @throws BlocException
	 */
	public boolean rejectRules(FiniteStateAutomata A) throws BlocException{

		if(this.rules.isEmpty()) {
			return true;
		}
		for(Bloc b : A.getPartition().getBlocs()){
			List<Symbol> l = A.getPossibleBlocTransition(b.min());
			for(Symbol s : l){

				Bloc b2 = A.getBlocTransition(b,s);
				if(this.rules.containsKey(s)) {
					List<Symbol> l2 = A.getPossibleBlocTransition(b2.min());
					for(Symbol s2 : l2){
						if(this.rules.get(s).contains(s2)){
							return false;
						}
					}
				}

			}
		}
		return true;
	}

	/**
	 *
	 * Return all rules that fsa doesn't reject
	 * @param A the fsa
	 * @return A set of actions pair
	 * @throws BlocException
	 */
	public List<Pair<Symbol, Symbol>> getAcceptedRules(FiniteStateAutomata A)
			throws BlocException{
		List<Pair<Symbol, Symbol>> res = new ArrayList<>();
		for(Bloc b : A.getPartition().getBlocs()){
			List<Symbol> l = A.getPossibleBlocTransition(b.min());
			for(Symbol s : l){
				Bloc b2 = A.getBlocTransition(b,s);
				if(this.rules.containsKey(s)) {
					List<Symbol> l2 = A.getPossibleBlocTransition(b2.min());
					for(Symbol s2 : l2){
						if(this.rules.get(s).contains(s2)){
							res.add(new Pair<>(s,s2));
						}
					}
				}
			}
		}
		return res;
	}

	/**
	 * return the state responsible of the acceptance of e rule
	 * @param A the fsa
	 * @param r the accepted rule
	 * @return A state
	 * @throws BlocException
	 */
	public int stateAcceptorRules(FiniteStateAutomata A, Pair<Symbol,
			Symbol> r) throws BlocException {
		for(Bloc b : A.getPartition().getBlocs()){
			List<Symbol> l = A.getPossibleBlocTransition(b.min());
			for(Symbol s : l){
				Bloc b2 = A.getBlocTransition(b,s);
				if(this.rules.containsKey(s)) {
					List<Symbol> l2 = A.getPossibleBlocTransition(b2.min());
					for(Symbol s2 : l2){
						if(this.rules.get(s).contains(s2)){
							return b2.min();
						}
					}
				}
			}
		}
		return A.getQ().size()-1;
	}

	/**
	 * Learn an automaton
	 * @param pos The positive learning sample
	 * @param neg The positive learning sample
	 * @return A pair containing an automaton and the corresponding partition
	 * @throws BlocException
	 */
	public Pair<FiniteStateAutomata, Partition>RPNI(Sample pos, Sample neg)
			throws BlocException{
		FiniteStateAutomata A = FSAFactory.PTA(pos, new Alphabet(actions));
		neg = A.getRejected(neg);
		int N = A.getQ().size();
		Partition pi = new Partition(N);
		Map<Integer, List<Integer>> testPairBloc = new HashMap<>();
		for(int i=1; i<N; i++){
			List<Integer> testedBloc = new ArrayList<>();
			for(int j =0; j<i; j++){
				//System.err.println(i+" "+j+" "+N);
				//Check if we have already test these blocs
				if(pi.getBloc(i).min() == pi.getBloc(j).min()){
					continue;
				}
				if(testedBloc.contains(pi.getBloc(j).min())){
					continue;
				}
				if(testPairBloc.containsKey(pi.getBloc(i).min()) &&
						testPairBloc.get(pi.getBloc(i).min()).contains(pi.getBloc(j).min())) {
					continue;
				}
				if(! testPairBloc.containsKey(pi.getBloc(i).min())) {
					testPairBloc.put(pi.getBloc(i).min(), new ArrayList<>());
				}
				if(! testPairBloc.containsKey(pi.getBloc(j).min())) {
					testPairBloc.put(pi.getBloc(j).min(), new ArrayList<>());
				}
				testPairBloc.get(pi.getBloc(j).min()).add(pi.getBloc(i).min());
				testPairBloc.get(pi.getBloc(i).min()).add(pi.getBloc(j).min());
				testedBloc.add(pi.getBloc(j).min());

				Partition piPrime = pi.clone();
				piPrime.merge(j,i);

				//Determinist merge
				FiniteStateAutomata APrime = FSAFactory.derive(A, piPrime, pi);
				Partition piPrimePrime = APrime.determinization();

				//Check if I- and R are reject
				if(this.rejectRules(APrime) && ! APrime.acceptOneOf(neg)
						){
					pi = piPrimePrime;
					A = APrime;
					//System.exit(1);
				}
			}
		}
		//System.out.println(A.toString());
		return new Pair<>(A,pi);
	}

	/**
	 * Learn an automaton incrementally
	 * @param pos The positive learning sample already used
	 * @param neg The new learning sample already used
	 * @param newPos All new positive examples
	 * @param newNeg All new negative examples
	 * @param A The current fsa
	 * @param pi The current partition
	 * @return A pair containing the update automaton and the corresponding partition
	 * @throws BlocException
	 */
	public Pair<FiniteStateAutomata, Partition>RPNI2(Sample pos, Sample neg,
			Sample newPos,
			Sample newNeg,
			FiniteStateAutomata A,
			Partition pi)
					throws BlocException{
		boolean b = true;

		for(Example x : newPos.getExamples()) {
			if(A.accept(x)){
				pi = A.extension(x, pi);
				b &= true;
			}else{
				pi = A.extension(x, pi);
				b = false;
			}
		}
		for(Example x : newNeg.getExamples()) {
			if(! A.accept(x)){
				b &= true;
			}else{
				b = false;
			}
			if(!neg.getExamples().contains(x)) {
				neg.addExample(x);
			}
		}
		if(b) {
			return new Pair<>(A,pi);
		}

		int N = pi.max();
		List<List<Symbol>> acceptedNeg = new ArrayList<>();
		for(Example ex : neg.getExamples()){
			if(A.accept(ex)){
				acceptedNeg.add(ex.getActionSequences());
			}
		}
		FiniteStateAutomata APrime;
		if(! acceptedNeg.isEmpty()){
			//Collect prefix for each negative example accepted by A
			FiniteStateAutomata pta = FSAFactory.PTA(pos,new Alphabet(actions));
			int sizeMaxV = A.getPartition().max();
			List<List<Symbol>> prefixes = new ArrayList<>();
			for(List<Symbol> ex : acceptedNeg){
				List<Symbol> pref = new ArrayList<>();
				int q = A.getQ0();
				for(Symbol s : ex){
					if(pta.getPossibleTransition(q).contains(s)){
						pref.add(s);
						q = pta.getTransition(q, s);
					}else{
						break;
					}
				}
				if(sizeMaxV > pref.size()){
					sizeMaxV = pref.size();
				}
				prefixes.add(pref);
			}
			//Construct u
			List<Symbol> u = new ArrayList<>();
			for(int k = 0; k < sizeMaxV; k++){
				boolean sameSym = true;
				Symbol sym = prefixes.get(0).get(k);
				for(List<Symbol> ex : prefixes){
					sameSym &= sym.equals(ex.get(k));
				}
				if(sameSym){
					u.add(sym);
				}else{
					break;
				}
			}
			//Compute qu
			Bloc qu = A.getPartition().getBloc(A.getQ0());
			for(Symbol s : u){
				qu = A.getBlocTransition(qu, s);
			}
			//Compute {qv | qu in b & qv in v & qv -> suffix}
			List<Integer> bu = pi.getBloc(qu.min()).getStates();
			List<Integer> Qv = new ArrayList<>();
			for(int i=0; i<acceptedNeg.size(); i++){
				//Compute suffixes
				List<Symbol> suff = new ArrayList<>();
				int k = u.size();
				for(int j=k; j<acceptedNeg.get(i).size(); j++){
					suff.add(acceptedNeg.get(i).get(j));
				}
				for(int q : bu){
					if(A.acceptSuffix(q, suff)){
						Qv.add(q);
					}
				}
			}
			//Compute i
			int i = (new Bloc(Qv)).min();
			APrime = A;
			for(int j = N; j>=i; j--){
				Bloc bj = pi.getBloc(j);
				if(bj.getStates().size() < 2){
					continue;
				}
				Partition newPi = pi.clone();
				newPi.fission(j);
				APrime = FSAFactory.derive(A, newPi, pi);
				pi = APrime.fission(newPi);
				A = APrime;
			}
		}

		//Fission for R
		List<Pair<Symbol, Symbol>> acceptedRules = getAcceptedRules(A);
		if(! acceptedRules.isEmpty()) {
			int i = stateAcceptorRules(A, acceptedRules.get(0));
			for(Pair<Symbol, Symbol> r : acceptedRules){
				int j = stateAcceptorRules(A, r);
				if(j < i) {
					i = j;
				}
			}

			APrime = A;
			for(int j = N; j>=i; j--){
				Bloc bj = pi.getBloc(j);
				if(bj.getStates().size() < 2){
					continue;
				}
				Partition newPi = pi.clone();
				newPi.fission(j);
				APrime = FSAFactory.derive(A, newPi, pi);
				pi = APrime.fission(newPi);
				A = APrime;
			}
		}

		//States merges
		Map<Integer, List<Integer>> testPairBloc = new HashMap<>();
		for(int i=1; i<N; i++){
			List<Integer> testedBloc = new ArrayList<>();
			for(int j =0; j<i; j++){
				//Check if we have already test these blocs
				if(pi.getBloc(i).min() == pi.getBloc(j).min()){
					continue;
				}
				if(testedBloc.contains(pi.getBloc(j).min())){
					continue;
				}
				if(testPairBloc.containsKey(pi.getBloc(i).min()) &&
						testPairBloc.get(pi.getBloc(i).min()).contains(
                                                        pi.getBloc(j).min())) {
					continue;
				}
				if(! testPairBloc.containsKey(pi.getBloc(i).min())) {
					testPairBloc.put(pi.getBloc(i).min(), new ArrayList<>());
				}
				if(! testPairBloc.containsKey(pi.getBloc(j).min())) {
					testPairBloc.put(pi.getBloc(j).min(), new ArrayList<>());
				}
				testPairBloc.get(pi.getBloc(j).min()).add(pi.getBloc(i).min());
				testPairBloc.get(pi.getBloc(i).min()).add(pi.getBloc(j).min());
				testedBloc.add(pi.getBloc(j).min());

				//Compute pi'
				Partition piPrime = pi.clone();
				piPrime.merge(j,i);
				APrime = FSAFactory.derive(A, piPrime, pi);
				Partition piPrimePrime = APrime.determinization();

				//Check if the new automaton reject I- and R
				if(! APrime.acceptOneOf(neg)
						&& this.rejectRules(APrime)){
					A  = APrime;
					pi = piPrimePrime;
					break;
				}
			}
		}

		return new Pair<>(A,pi);
	}

	/**
	 * Test an automaton
	 * @param A The automaton to test
	 * @return the fscore
	 * @throws BlocException
	 */
	public float test(FiniteStateAutomata A) throws BlocException {
		int posAcc = 0, negAcc = 0;

		//Compute the number of accepted positive test example
		for(Example posExample : testPositive.getExamples()) {
			posAcc += A.accept(posExample) ? 1 : 0;
		}

		//Compute the number of accepted negative test example
		for(Example negExample : testNegative.getExamples()) {
			negAcc += A.accept(negExample) ? 1 : 0;
		}
		System.out.println(posAcc+" "+negAcc);
		System.out.println(posAcc+" "+negAcc);
		System.out.println("Recall = "+Measure.R(posAcc,
				testPositive.getExamples().size()));
		System.out.println("Precision = "+Measure.P(posAcc, negAcc));

		//Compute the fscore
		return Measure.FScore(Measure.R(posAcc, testPositive.getExamples().
				size()),
				Measure.P(posAcc, negAcc));
	}

	/**
	 * Update R
	 * @param examples Positive sample
	 */
	public void constructOptimalR(Sample examples) {
		for(Example s : examples.getExamples()) {
			this.removeRules(s);
		}
	}

	/**
	 * Clean R to use RPNI for regular grammar induction
	 */
	public void emptyR() {
		this.rules = new HashMap<>();
	}

	/**
	 * Decompose all positive example
	 * @param pos I+
	 * @return A positive sample
	 */
	public Sample decompose(Sample pos) {
		List<Example> p = new ArrayList<>();
		for(Example seq : pos.getExamples()){
			int M = seq.size();
			for(int i=1; i< M+1; i++){
				Example x = new Example();
				for(int j =0 ; j<i; j++){
					x.add(seq.get(j));
				}
				p.add(x);
			}
		}
		return new Sample(p);
	}

	/**
	 * Give compression data
	 * @param A The automaton
	 * @param pos The positive sample
	 */
	public void writeDataCompression(FiniteStateAutomata A, Sample pos) {
		float nbObs = ( pos.size() * pos.meanSize());
		int nbState = A.getPartition().getBlocs().size();
		float compressionLevel = (float) nbObs / nbState;
		System.out.println("#Observed states : "+nbObs);
		System.out.println("#States : "+nbState);
		System.out.println("#Transitions : "+A.getNumberTransitions());
		System.out.println("Compression level : "+compressionLevel);
	}
}
