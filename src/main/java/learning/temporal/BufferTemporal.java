/**
 * 
 */
package learning.temporal;

import java.util.ArrayList;
import java.util.List;

import learning.Individual;

/**
 * @author Maxence Grand
 *
 */
public class BufferTemporal {
	/**
	 * 
	 */
	List<Individual> indiv;
	/**
	 * 
	 */
    private boolean available	;
    
    /**
     * 
     * Constructs
     */
    public BufferTemporal () {
    	this.indiv= new ArrayList<>();
    	available=true;
    }
    
    /**
     * 
     */
    public synchronized boolean indivWating(Individual i) {
    	boolean b = false;
        while (!available) {
            try {
            	wait();
            	available=true;}
            catch (InterruptedException e) { }
        }
        available = false;
        b = !this.indiv.contains(i);
        if(b) {
        	this.indiv.add(i);
        }
        available=true;
        notifyAll();
        return b;
    }

}
