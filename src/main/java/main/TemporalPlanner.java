/**
 * 
 */
package main;

import java.io.File;

import fr.uga.pddl4j.parser.PDDLProblem;
import fr.uga.pddl4j.plan.Plan;
import fr.uga.pddl4j.planners.ProblemFactory;
import fr.uga.pddl4j.problem.Problem;
import temporal.planner.TemporalHSP;
import temporal.planner.Translator;

/**
 * @author Maxence Grand
 *
 */
public class TemporalPlanner {
	/**
	 * 
	 */
	private static final int TIME_OUT = 3600;
	/**
	 * 
	 * @param args
	 */
	public static void run() {
		File domain = new File(Argument.getDomain());
		File problem = new File(Argument.getProblem());
		//System.out.println(domain+"!!!!"+problem);
		File classical = Translator.translate(domain, "tmp.pddl");
		
		final ProblemFactory factory = ProblemFactory.getInstance();
		try {
			factory.parse(classical, problem);
		} catch (Exception e) {
			System.out.println("Unexpected error when parsing the PDDL planning problem description.");
			System.exit(0);
		}
		final Problem p = factory.encode();
		
		TemporalHSP planner = new TemporalHSP();
		planner.setTimeOut(TIME_OUT*1000);
		Plan plan = planner.searchTemporal(p, domain);
	}
}
