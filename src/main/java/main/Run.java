/**
 *
 */
package main;

import main.experiment.AMLSI;
import main.experiment.Incremental;
import main.experiment.Convergent;
import main.experiment.HybridTempAMLSI;
import main.experiment.Tabu;
import main.experiment.TempAMLSI;
import main.experiment.Temporal;
import main.experiment.TemporalTabu;
import main.experiment.TestLSONIO;

/**
 * The runnable class
 * @author Maxence Grand
 *
 */
public class Run {

	/**
	 * Main class
	 * @param args
	 */
	public static void main(String[] args) {
		Argument.read(args);
		
		if(Argument.isLsonio()) {
			//Run LSO-NIO
			try {
				Properties.read(Argument.getPropertiesFile());
				TestLSONIO.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		} else if(Argument.isIncremental()) {
			//Run Incremental AMLSI
			try {
				Properties.read(Argument.getPropertiesFile());
				Incremental.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}else if(Argument.isConvergent()) {
			//Run Convergent AMLSI
			try {
				Properties.read(Argument.getPropertiesFile());
				Convergent.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		} else  if (Argument.isOnlyTabu() && !Argument.isTemporal()) {
			//Run Tabu search alone
			try {
				Properties.read(Argument.getPropertiesFile());
				Tabu.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		} else  if (Argument.isTemporal()) {
			//Run Tabu search alone
			try {
				Properties.read(Argument.getPropertiesFile());
				if(Argument.isOnlyTabu()) {
					TemporalTabu.run();
				} else {
					TempAMLSI.run();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}else  if (Argument.isHybrid()) {
			//Run Tabu search alone
			try {
				Properties.read(Argument.getPropertiesFile());
				HybridTempAMLSI.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		} else if(Argument.isPlanner()) {
			TemporalPlanner.run();
		} else {
			//Run vanilla AMLSI
			try {
				Properties.read(Argument.getPropertiesFile());
				AMLSI.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
