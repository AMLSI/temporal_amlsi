package main.experiment;

import java.util.List;
import java.util.Map;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import fsm.Symbol;
import fsm.Sample;
import fsm.Example;
import fsm.FiniteStateAutomata;
import fsm.Pair;
import fsm.Partition;
import java.util.Random;

import exception.BlocException;
import learning.AutomataLearning;
import learning.Generator;
import learning.Individual;
import learning.Mapping;
import learning.Observation;
import learning.ObservedExample;
import main.Argument;
import main.Properties;
import simulator.BlackBox;
import simulator.Oracle;
import simulator.temporal.TemporalBlackBox;
import simulator.temporal.TemporalOracle;
import learning.preprocess.Preprocessor;
import learning.temporal.TemporalDomainLearning;
import learning.temporal.TemporalGenerator;
import learning.temporal.TemporalIndividual;
import learning.temporal.translator.Translator;

/**
 * Main class for Temporal AMLSI
 *
 * @author Maxence Grand
 *
 */
public class TemporalTabu {
	/**
	 * Number of positive test examples
	 */
	public static final int nbTest = 100;
	/**
	 * Size of positive test examples
	 */
	public static final int sizeTest = 30;
	/**
	 * Number of positive learning examples
	 */
	public static final int nbLearn = 30;
	/**
	 * Minimal size of positive leaning examples
	 */
	public static final int minLearn = 5;
	/**
	 * Maximal size of positive leaning examples
	 */
	public static final int maxLearn = 15;

    /**
     *
     * Run
     */
	public static void run () throws Exception{
		long[] seeds = Properties.getSeeds();

		String directory = Argument.getDirectory();
		String reference = Argument.getDomain();
		String initialState = Argument.getProblem();
		String name = Argument.getName();

		TemporalOracle tempSim = new TemporalBlackBox(reference, initialState);
		
		/*
		 * All actions
		 */
		List<Symbol> tempActions = tempSim.getAllActions();
		
		/*
		 * All predicates
		 */
		List<Symbol> tempPred = new ArrayList<>();
		for(Symbol a : tempSim.getAllPredicates()){
			tempPred.add(a);
		}
		for(Symbol a : tempSim.getPositiveStaticPredicate()){
			tempPred.add(a);
		}
		
		File file_temporal = new File(reference);
		if(Argument.isTwoOp()) {
			Translator.translate2Op(file_temporal, 
					"classical_"+Argument.getName()+".pddl");
		} else {
			Translator.translate(file_temporal, 
					"classical_"+Argument.getName()+".pddl");
		}
		
		System.out.println("# actions "+tempActions.size());
		System.out.println("# predicate "+tempPred.size());

		System.out.println("Initial state : "+initialState);
		System.out.println(reference);
		for(int seed = 0; seed < Argument.getRun() ; seed++) {
			System.out.println("\n"+Argument.getType()+" run : "+seed);
			tempSim = new TemporalBlackBox(reference, initialState);
			Random random = new Random();
			random.setSeed(seeds[seed]);
			Random random2 = new Random();
			random2.setSeed(seeds[seed]);
			TemporalGenerator generator = new TemporalGenerator(tempSim,random);

			TemporalGenerator generatorTest = new TemporalGenerator
					(tempSim,random);
			Pair<Sample, Sample> testSet = generatorTest.generate(
					minLearn, sizeTest,	100, nbTest);
			//Pair<Sample, Sample> testSet = new Pair<>(new Sample(), new Sample());
			Oracle sim = new BlackBox("classical_"+Argument.getName()+".pddl",
					initialState);
			
			/*
			 * All actions
			 */
			List<Symbol> actions = tempSim.getSeqActions();
			/*
			 * All predicates
			 */
			List<Symbol> pred = new ArrayList<>();
			for(Symbol a : tempSim.getAllPredicates()){
				pred.add(a);
			}
			for(Symbol a : tempSim.getPositiveStaticPredicate()){
				pred.add(a);
			}
			
			Map<String, Float> durationString = 
					Translator.getDurations(file_temporal);
			Map<Symbol, Float> duration = new HashMap<>();
			for(Symbol action : tempSim.getAllActions()) {
				if(!duration.containsKey(action.generalize())) {
					duration.put(action.generalize(), durationString.get(
							action.getName()));
				}
			}
			
			Generator generatorSeq = new Generator(sim,random);
			
			//Generate learning set
			Pair<Sample, Sample> D = generator.generate(minLearn, maxLearn,
					100, nbLearn);
			Sample pos = D.getX();
			Sample neg = D.getY();
			for(Example e : pos.getExamples()) {
				if(neg.contains(e)) {
					System.out.println(e);
				}
			}
			
			System.out.println("I+ size : "+pos.size());
			System.out.println("I- size : "+neg.size());
			System.out.println("x+ mean size : "+pos.meanSize());
			System.out.println("x- mean size : "+neg.meanSize());
			System.out.println("E+ size : "+testSet.getX().size());
			System.out.println("E- size : "+testSet.getY().size());
			System.out.println("e+ mean size : "+testSet.getX().meanSize());
			System.out.println("e- mean size : "+testSet.getY().meanSize());

			for(Symbol a : TemporalDomainLearning.getAllActions(pos)) {
				if(!actions.contains(a)) {
					actions.add(a);
				}
			}
			for(Symbol a : TemporalDomainLearning.getAllActions(neg)) {
				if(!actions.contains(a)) {
					actions.add(a);
				}
			}
			TemporalDomainLearning learningModule = new TemporalDomainLearning(
					pred,
					actions,
					directory,
					name,
					reference,
					initialState,
					generatorSeq);

			learningModule.setSamples(testSet.getX(), testSet.getY());
			learningModule.setTypes(sim.typeHierarchy());

			//System.exit(1);
			for(float thresh : Properties.getPartial()) {
				System.out.println(
						"############################################");
				System.out.println("### Fluent = "+(thresh)+"% ###");
				Generator.LEVEL = ( (float) thresh / 100);
				for(float noise : Properties.getNoise()){
					Generator.THRESH = ((float)noise / 100);

					System.out.println("\n*** Noise = "+(noise)+"% ***");
					String domainName = "";
					domainName = directory+"/amlsi_rpnir."
							+((int)thresh)+"."+((int)noise)+"."
							+seed;
					pos = generatorSeq.map(pos, tempSim);
					long startTime = System.currentTimeMillis();
					Individual currentPDDL = null;
					learningModule.initFitness();

					Map<Symbol, Observation> emptyPrec = new HashMap<>();
					Map<Symbol, Observation> emptyPost = new HashMap<>();
					for(Symbol act : actions) {
						emptyPrec.put(act.generalize(), new Observation());
						emptyPost.put(act.generalize(), new Observation());
					}
					currentPDDL =  new Individual(pred, actions, emptyPrec, emptyPost);
					TemporalIndividual.initClassicalSym(currentPDDL);
					//TemporalIndividual.neighbors(currentPDDL, Argument.isTwoOp());
					//System.out.println(currentPDDL.getDomain());
					//System.exit(1);
					currentPDDL = learningModule.localSearch(
							currentPDDL,
							pos,
							neg,
							generator.getCompressedSequential(),
							initialState,
							10,
							200,
							new ArrayList<>(),
							duration, 
							tempActions);
					
					//Step 4: Write PDDL domain and Dot automaton
					//Step 4.1: Write PDDL domain
					Observation initial2 = ((ObservedExample) 
							pos.getExamples().get(0)).getInitialState();
					
					currentPDDL = TemporalIndividual.convert2(
							currentPDDL.clone(), duration, Argument.isTwoOp(), 
							pred, actions, tempActions);
					BufferedWriter bw = new BufferedWriter(
							new FileWriter(domainName+"_intermediate.pddl"));
					
					TemporalIndividual tempCurrent = 
							new TemporalIndividual(currentPDDL, duration,
									Argument.isTwoOp(), pred, tempActions);
					
					Pair<Map<Symbol, Observation>,Map<Symbol, Observation>> pair
														= currentPDDL.decode();
					Map<Symbol, Observation> preconditions = pair.getX();
					Map<Symbol, Observation> postconditions = pair.getY();
					bw.write(learningModule.generation(
							preconditions, postconditions));
					bw.close();
					File f = new File(domainName+"_intermediate.pddl");
					if(Argument.isTwoOp()) {
						Translator.translate2Op(f, 
								Translator.getDurations(file_temporal),
								domainName+".pddl");
					} else {
						Translator.translate(f, 
								Translator.getDurations(file_temporal), 
								domainName+".pddl");
					}
					long endTime = System.currentTimeMillis();
					float time = (float)(endTime-startTime)/1000;
					System.out.println("Time : "+time);
					f = new File(domainName+".pddl");
					Translator.translate(f, "tmp.pddl");
					Translator.translate(file_temporal, "tmp_ref.pddl");
					TemporalTabu.test("tmp_ref.pddl","tmp.pddl");
					float fscore = 
							tempCurrent.fscore(
									generatorTest.getPositive(), 
									generatorTest.getNegative(), 
									initial2);
					System.out.println("Intermediate FSCORE : "+
									currentPDDL.fscore(initial2, testSet.getX(), 
											testSet.getY()));
					System.out.println("Temoral FSCORE : "+fscore);
				}
			}
		}
	}
	
	/**
	 * Test the learnt domain
	 * @param i The action model
	 * @param posTest The positive test sample
	 * @param negTest The negative test sample
	 * @param ref The reference domain
	 * @param s0 The initial state
	 * @param domainName The domain name
	 * @param learningModule The action model learner
	 * @param actions The action set
	 * @param generatorTest The generator for test samples
	 * @throws IOException
	 */
	private static void test (String ref, String domainName)
					throws IOException {
		try {
			TestMetrics.test(ref,domainName);
		} catch (BlocException e) {
			// TODO Auto-generated catch block
			System.exit(1);
			e.printStackTrace();
		}
	}
}
