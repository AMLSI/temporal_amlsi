/**
 * 
 */
package main.experiment;

import java.io.File;
import java.util.Random;

import fsm.Pair;
import fsm.Sample;
import learning.temporal.TemporalGenerator;
import learning.temporal.translator.Translator;
import simulator.BlackBox;
import simulator.Oracle;
import simulator.temporal.TemporalBlackBox;
import simulator.temporal.TemporalOracle;

/**
 * @author Maxence Grand
 *
 */
public class TestTemporal {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String rep = "/home/maxence/Documents/amlsi/pddl/temporal/";
		//String dom = "/home/maxence/Documents/amlsi/amlsi_rpnir.100.0.0.pddl";
		String dom = rep+"match/domain.pddl";
		String init = rep+"match/instances/instance-1.pddl";
		//String init = rep+"floortile/initial_states/initial1.pddl";
		
		TemporalOracle simulator = new TemporalBlackBox(dom, init);
		//simulator.getAllActions().forEach( a -> System.out.println(a));
		simulator.reInit();
		/*simulator.getAllActions().forEach( a -> {
			System.out.println(a+" "+simulator.isApplicable(a, 0));
		});*/
		//simulator.getAllPredicates().forEach(p -> System.out.println(p));
		//simulator.getPositiveStaticPredicate().forEach(p -> System.out.println(p));
		System.out.println();
		
		File temporal=new File(dom);
		Translator.translate(temporal, rep+"test.pddl");
		File f = Translator.translate2Op(temporal, rep+"test.pddl");
		Translator.translate2Op(f, Translator.getDurations(temporal), rep+"test2.pddl");
		Oracle sim2 = new BlackBox(rep+"test.pddl", init);
		TemporalGenerator generator = new TemporalGenerator(simulator, new Random());
		generator.generate(1, 5, 1000f, 1);
		/*generator.getPositive().forEach(tEx -> {
			System.out.println(tEx);
			System.out.println(tEx.convertIntoSequential3Op(simulator));
		});*/
		generator.getNegative().forEach(tEx -> {
			System.out.println(tEx);
			System.out.println(tEx.convertIntoSequential3Op(simulator));
		});
		//System.out.println(p.getX());
		//p.getY().getExamples().forEach(ex -> System.out.println(ex));
		//generator.getPositive().forEach(ex -> System.out.println(ex));
		System.out.println("Fin");
	}

}
