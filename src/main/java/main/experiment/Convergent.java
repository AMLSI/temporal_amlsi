/**
 *
 */
package main.experiment;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import exception.BlocException;
import fsm.Example;
import fsm.FiniteStateAutomata;
import fsm.Pair;
import fsm.Partition;
import fsm.Sample;
import fsm.Symbol;
import learning.AutomataLearning;
import learning.DomainLearning;
import learning.Generator;
import learning.Individual;
import learning.Mapping;
import learning.Observation;
import learning.ObservedExample;
import main.Argument;
import main.Properties;
import simulator.BlackBox;
import simulator.Oracle;

/**
 * Main class for convergent AMLSI
 * @author Maxence Grand
 *
 */
public class Convergent{
	/**
	 * The  number of positive test  examples
	 */
	public static final int nbTest = 100;
	/**
	 * The maximal size of positive test examples
	 */
	public static final int sizeTest = 100;
	/**
	 * The number of generated example at each iterations
	 */
	public static final int nbLearn = 1;
	/**
	 * The number of epochs for each tabu search
	 */
	public static final int epochs = 200;
	/**
	 * The number of generation
	 */
	public static final int T = 100;
	/**
	 * The minimal size of generated positive example
	 */
	public static final int minLearn = 10;
	/**
	 * The maximal size of generated positive example
	 */
	public static final int maxLearn = 20;
	/**
	 * The pseudo random number generator used by our example generator
	 */
	private static Random random ;

    /**
     *
     * Run
     */
	public static void run() throws BlocException, CloneNotSupportedException, IOException {
		long[] seeds = Properties.getSeeds();

		String directory = Argument.getDirectory();
		String reference = Argument.getDomain();
		String initialState = Argument.getProblem();
		String name = Argument.getName();

		int T = Argument.getT();
		int Delta = Argument.getDelta();

		/*
		 * The blackbox
		 */
		Oracle sim = new BlackBox(reference, initialState);
		/*
		 * All actions
		 */
		List<Symbol> actions = sim.getAllActions();
		/*
		 * All predicates
		 */
		List<Symbol> pred = new ArrayList<>();
		for(Symbol a : sim.getAllPredicates()){
			pred.add(a);
		}
		for(Symbol a : sim.getPositiveStaticPredicate()){
			pred.add(a);
		}

		/**
		 * For logs
		 */
		System.out.println("# actions "+sim.getAllActions().size());
		System.out.println("# predicate "+pred.size());

		System.out.println("Initial state : "+initialState);
		System.out.println("Delta="+Argument.getDelta());
		System.out.println(reference);

		for(int seed = 0; seed < Argument.getRun() ; seed++) {
			/*
			 * The blackbox
			 */
			sim = new BlackBox(reference, initialState);
			/*
			 * Pseudo random number generator for test set generation
			 */
			Random randomTest = new Random();
			randomTest.setSeed(seeds[seed]);

			/*
			 * Example generator used for the test set
			 */
			Generator generatorTest = new Generator(sim,randomTest);
			/*
			 * Generate test set
			 */
			Pair<Sample, Sample> testSet = generatorTest.interactPartial(nbTest, 1,sizeTest);

			/*
			 * For logs
			 */
			System.out.println("E+ size : "+testSet.getX().size());
			System.out.println("E- size : "+testSet.getY().size());
			System.out.println("e+ mean size : "+testSet.getX().meanSize());
			System.out.println("e- mean size : "+testSet.getY().meanSize());

			for(float thresh : Properties.getPartial()) {
				System.out.println("############################################"
						+ "\n### Fluent = "+(thresh)+"% ###");
				/*
				 * Set the level of observable fluents
				 */
				Generator.LEVEL = ( (float) thresh / 100);
				for(float noise : Properties.getNoise()){
					Generator.THRESH = ((float)noise / 100);
					System.out.println("\n*** Noise = "+(noise)+"% ***");

					String domainName = directory+"/amlsi_rpnir."
							+((int)thresh)+"."+((int)noise)+"."
							+seed;

					/*
					 * Pseudo random number generator for test set generation
					 */
					random = new Random(seeds[seed]);
					/*
					 * Example generator for learning samples
					 */
					Generator generator = new Generator(sim,random);
					Observation initial = generator.getInitialState();
					DomainLearning learningModule = new DomainLearning(
							pred,
							actions,
							directory,
							name,
							reference,
							initialState,
							generator);

					learningModule.setSamples(testSet.getX(), testSet.getY());
					learningModule.setTypes(sim.typeHierarchy());

					AutomataLearning learner;
					learner = new AutomataLearning(
							pred,
							actions,
							generator,
							learningModule);
					learner.setSamples(testSet.getX(), testSet.getY());
					long startTime = System.currentTimeMillis(), endTime=0;
					//First iteration
					//System.out.println("Iteration 1");
					Pair<Sample, Sample> samples = generator.interactPartial
							(1, minLearn, maxLearn);
					Sample pos = samples.getX();
					Sample neg = samples.getY();
					//System.out.println("Iteration 1");


					//Step 1: grammar induction
					//Step 1.1: Adding prefixes
					Sample posPrefixes = pos.clone();
					posPrefixes = learner.decompose(posPrefixes);
					//Step 1.2: pairewise constraints
					for(Example x : pos.getExamples()) {
						learner.removeRules(x);
					}
					//Step 1.3: Automaton learning
					Pair<FiniteStateAutomata, Partition> p =
							learner.RPNI(posPrefixes, neg);
					FiniteStateAutomata A = p.getX();

					//Step 2: PDDL Generation
					//Step 2.1: Mapping
					Mapping mapAnte = Mapping.getMappingAnte(
							pos,
							p.getX(),
							actions,
							pred);
					Mapping mapPost = Mapping.getMappingPost(
							pos,
							p.getX(),
							actions,
							pred);
					//Step 2.2: Operator Generation
					Individual initialPDDL = learningModule.generatePDDLOperator(
							A, mapAnte, mapPost);
					//System.out.println("Fscore automaton "+learner.test(A));

					//Step 3: Refinement
					//Step 3.1: First prec/post refinement
					Individual refinedPDDL = learningModule.refineOperator(
							initialPDDL.clone(), A, mapAnte, mapPost);
					//Step 3.2: Tabu refinement
					Individual currentPDDL =  refinedPDDL.clone();
					float previous = learningModule.fitness(
							currentPDDL,  pos, neg, 
							generator.getCompressedNegativeExample(),initialState);
					float current = previous;
					boolean b = false;
					List<Individual> tabou = new ArrayList<>();
					do {
						if(b) {
							previous=current;
							currentPDDL = learningModule.refineOperator(
									currentPDDL, A, mapAnte, mapPost);
						}
						currentPDDL = learningModule.localSearch(
								currentPDDL,
								pos,
								neg,
								generator.getCompressedNegativeExample(),
								initialState,
								10,
								200,
								tabou);
						b = true;
						current = learningModule.fitness(
								currentPDDL,  pos, neg, 
								generator.getCompressedNegativeExample(),initialState);
					}while(current > previous);
					//Iteration 2:T
					int delta = 0;
					for(int t = 2; t <= T; t++ ) {
						
						//Generate new data
						//Update I+ nand I-
						Sample previousPos = pos.clone();
						Sample previousNeg = neg.clone();
						samples = generator.interactPartial(
								1,minLearn,maxLearn);
						//Update R
						//Delete obsolete rules
						for(Example x : samples.getX().getExamples()) {
							learner.removeRules(x);
						}

						//Update A
						//Decompose new positive examples
						Sample tmp = new Sample();
						for(Example xPos : samples.getX().getExamples()) {
							previousPos.addExample(xPos);
							tmp.addExample(xPos);
						}
						tmp = learner.decompose(tmp);
						p = learner.RPNI2(previousPos, previousNeg, tmp, samples.getY(), A,
								p.getY());
						A = p.getX();

						pos = previousPos;
						neg = previousNeg;

						//Rebuilt I+
						/*data = generator.getData();
						for(Map.Entry<List<Symbol>,List<Observation>> entry :
								previousData.entrySet()) {
							data.put(entry.getKey(), entry.getValue());
						}
						//generator.testNoiseAndPartiality(data);
						Map<List<Symbol>,List<Observation>> obsPost =
								new HashMap<>();
						Map<List<Symbol>,List<Observation>> obsAnte =
								new HashMap<>();
						Sample posRec = new Sample();
						data.forEach((k,v) -> {
							List<Symbol> ex = new ArrayList<>();
							List<Observation> obsA = new ArrayList<>();
							List<Observation> obsP = new ArrayList<>();
							for(int i = 0; i<k.size(); i++) {
								ex.add(k.get(i));
								obsA.add(v.get(i));
								obsP.add(v.get(i+1));
							}
							obsAnte.put(ex, obsA);
							obsPost.put(ex, obsP);
							posRec.addExample(ex);
						});*/

						//Update mapping
						mapAnte = Mapping.getMappingAnte(
								pos,
								p.getX(),
								actions,
								pred);
						mapPost = Mapping.getMappingPost(
								pos,
								p.getX(),
								actions,
								pred);

						//Backtrack
						Individual previousPDDL = currentPDDL.clone();

						Individual tmpPDDL = learningModule.generatePDDLOperator(A, mapAnte, mapPost);
						tmpPDDL = learningModule.refineOperator(tmpPDDL, A, mapAnte, mapPost);

						currentPDDL.backtrackUncompatibleEffects(
								A, mapAnte, mapPost);
						currentPDDL.backtrackUncompatiblePreconditions(
								A, mapAnte, mapPost);
						currentPDDL.backtrackNegativeEffects();
						do {
							while(! currentPDDL.acceptAll(initial, pos)) {
								currentPDDL.backtrackPrecondition(initial, pos);
								currentPDDL.backtrackNegativeEffects();
							};
							currentPDDL.backtrackEffects(initial, pos, neg);
						}while(! currentPDDL.acceptAll(initial, pos));
						currentPDDL = currentPDDL.merge(tmpPDDL);

						//Update D
						previous = learningModule.fitness(
								currentPDDL, pos, neg,
								generator.getCompressedNegativeExample(), initialState);
						current = previous;
						tabou = new ArrayList<>();
						do {
							previous=current;
							currentPDDL = learningModule.refineOperator(
									currentPDDL, A, mapAnte, mapPost);
							currentPDDL = learningModule.localSearch(
									currentPDDL,
									pos,
									neg,
									generator.getCompressedNegativeExample(),
									initialState,
									10,
									200,
									tabou);
							current = learningModule.fitness(
									currentPDDL, pos, neg, 
									generator.getCompressedNegativeExample(),initialState);
						}while(current > previous);
						if(currentPDDL.acceptAll(initial, pos) &&
								currentPDDL.rejectAll(initial, neg)) {
							if(previousPDDL.equals(currentPDDL)) {
								delta++;
							}else {
								delta=1;
							}
						} else {
							delta=0;
						}

						if(t == T || delta >= Delta) {
							System.out.println("Iteration "+t);
							System.out.println("Size I+ "+pos.size());
							System.out.println("Size I- "+neg.size());
							endTime= System.currentTimeMillis();
							float time = (float)(endTime-startTime)/1000;
							System.out.println("Time : "+time);
							System.out.println(Argument.isHasSpecificTestInit()+" "+Argument.getSpecificTestInit());
							Convergent.test(
									currentPDDL,testSet.getX(), testSet.getY(),
									reference,
									Argument.isHasSpecificTestInit() ?
											Argument.getSpecificTestInit() :
												initialState, domainName+".pddl",
									learningModule, actions, generatorTest);
							List<List<Symbol>> examples = new ArrayList<>();
							Observation initial2 = ((ObservedExample) pos.getExamples().get(0)).getInitialState();
							float fscore = currentPDDL.fscore(initial2, testSet.getX(), testSet.getY());
							System.out.println("FSCORE : "+fscore);
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * Test the learnt domain
	 * @param i The action model
	 * @param posTest The positive test sample
	 * @param negTest The negative test sample
	 * @param ref The reference domain
	 * @param s0 The initial state
	 * @param domainName The domain name
	 * @param learningModule The action model learner
	 * @param actions The action set
	 * @param generatorTest The generator for test samples
	 * @throws IOException
	 */
	private static void test (
			Individual i, Sample posTest, Sample negTest, String ref,
			String s0, String domainName, DomainLearning learningModule,
			List<Symbol> actions, Generator generatorTest)
					throws IOException {
		BufferedWriter bw = new BufferedWriter(
				new FileWriter(domainName));
		Pair<Map<Symbol, Observation>,Map<Symbol, Observation>>
		pair = i.decode();
		Map<Symbol, Observation> preconditions = pair.getX();
		Map<Symbol, Observation> postconditions = pair.getY();
		bw.write(learningModule.generation(
				preconditions, postconditions));
		bw.close();
		try {
			TestMetrics.test(ref, s0, domainName,generatorTest,actions,new Pair<>(posTest, negTest));
		} catch (BlocException e) {
			// TODO Auto-generated catch block
			System.exit(1);
			e.printStackTrace();
		}
	}
}
