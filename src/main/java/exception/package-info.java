/**
 * 
 * This package contains all classes dealing with exceptions
 * 
 * @author Maxence Grand
 *
 */
package exception;
