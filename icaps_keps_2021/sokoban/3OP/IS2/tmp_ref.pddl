(define (domain sokoban-temporal)
(:requirements :strips :typing :negative-preconditions)
(:types 
location thing direction - object
player stone - thing
)(:predicates
	(clear ?l - LOCATION)
	(at ?t - THING ?l - LOCATION)
	(move-dir ?from - LOCATION ?to - LOCATION ?dir - DIRECTION)
)
(:action move-start
	:parameters ( ?p - PLAYER ?from - LOCATION ?to - LOCATION ?dir - DIRECTION )
	:precondition (and
		(at ?p ?from)
		(clear ?to)
	)
	:effect (and
		(not (at ?p ?from))
		(not (clear ?to))
	)
)
(:action move-end
	:parameters ( ?p - PLAYER ?from - LOCATION ?to - LOCATION ?dir - DIRECTION )
	:precondition (and
	)
	:effect (and
		(at ?p ?to)
		(clear ?from)
	)
)
(:action move-inv
	:parameters ( ?p - PLAYER ?from - LOCATION ?to - LOCATION ?dir - DIRECTION )
	:precondition (and
		(move-dir ?from ?to ?dir)
	)
	:effect (and
	)
)
(:action push-start
	:parameters ( ?p - PLAYER ?s - STONE ?ppos - LOCATION ?from - LOCATION ?to - LOCATION ?dir - DIRECTION )
	:precondition (and
		(at ?p ?ppos)
		(at ?s ?from)
		(clear ?to)
	)
	:effect (and
		(not (at ?p ?ppos))
		(not (at ?s ?from))
		(not (clear ?to))
	)
)
(:action push-end
	:parameters ( ?p - PLAYER ?s - STONE ?ppos - LOCATION ?from - LOCATION ?to - LOCATION ?dir - DIRECTION )
	:precondition (and
	)
	:effect (and
		(at ?p ?from)
		(at ?s ?to)
		(clear ?ppos)
	)
)
(:action push-inv
	:parameters ( ?p - PLAYER ?s - STONE ?ppos - LOCATION ?from - LOCATION ?to - LOCATION ?dir - DIRECTION )
	:precondition (and
		(move-dir ?ppos ?from ?dir)
		(move-dir ?from ?to ?dir)
	)
	:effect (and
	)
)
)