(define (domain sokoban-temporal)
(:requirements :strips :typing :negative-preconditions)
(:types
location direction T1 - object
stone player - T1
)
(:predicates
	(at ?x1 - T1 ?x2 - location)
	(clear ?x1 - location)
	(move-dir ?x1 - location ?x2 - location ?x3 - direction)
)
(:action move-start
	:parameters (?x1 - player ?x2 - location ?x3 - location ?x4 - direction )
	:precondition (and
	(clear ?x3)
	(not(clear ?x2))
	(at ?x1 ?x2)
	(not(at ?x1 ?x3)))
	:effect (and
	(not(clear ?x3))
	(not(at ?x1 ?x2)))
)
(:action move-end
	:parameters (?x1 - player ?x2 - location ?x3 - location ?x4 - direction )
	:precondition (and)
	:effect (and
	(move-dir ?x2 ?x3 ?x4)
	(clear ?x2)
	(at ?x1 ?x3))
)
(:action push-start
	:parameters (?x1 - player ?x2 - stone ?x3 - location ?x4 - location ?x5 - location ?x6 - direction )
	:precondition (and
	(not(at ?x2 ?x5))
	(at ?x2 ?x4)
	(not(clear ?x3))
	(clear ?x5)
	(not(at ?x1 ?x4))
	(at ?x1 ?x3))
	:effect (and
	(not(at ?x2 ?x4))
	(move-dir ?x4 ?x5 ?x6)
	(not(clear ?x5))
	(not(at ?x1 ?x3)))
)
(:action push-end
	:parameters (?x1 - player ?x2 - stone ?x3 - location ?x4 - location ?x5 - location ?x6 - direction )
	:precondition (and
	(move-dir ?x4 ?x5 ?x6))
	:effect (and
	(at ?x2 ?x5)
	(clear ?x3)
	(at ?x1 ?x4)
	(move-dir ?x3 ?x4 ?x6))
)
(:action move-inv
	:parameters (?x1 - player ?x2 - location ?x3 - location ?x4 - direction )
	:precondition (and
	(move-dir ?x2 ?x3 ?x4))
	:effect (and)
)
(:action push-inv
	:parameters (?x1 - player ?x2 - stone ?x3 - location ?x4 - location ?x5 - location ?x6 - direction )
	:precondition (and
	(not(at ?x2 ?x3))
	(not(clear ?x4))
	(not(at ?x1 ?x5))
	(move-dir ?x3 ?x4 ?x6))
	:effect (and)
)
)
