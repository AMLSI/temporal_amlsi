# actions 28
# predicate 44
Initial state : pddl/temporal/sokoban/initial_states/initial3.pddl
pddl/temporal/sokoban/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 5696
x+ mean size : 27.6
x- mean size : 19.196629
E+ size : 100
E- size : 36307
e+ mean size : 51.81
e- mean size : 35.363594
Automaton Time : 66.414
30 13
30 13
Recall = 0.3
Precision = 0.6976744
Automaton Fscore : 0.41958043
#Observed states : 828.0
#States : 105
#Transitions : 131
Compression level : 7.885714
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 8064.0 current = 8066.0
previous = 8066.0 current = 8066.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4), (move-dir ?x3 ?x4 ?x6)]}
Time : 3058.026
Syntactical distance : 0.0148148155
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 7811.0 current = 8111.0
previous = 8111.0 current = 8111.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4), (move-dir ?x3 ?x4 ?x6)]}
Time : 2981.09
Syntactical distance : 0.0037037039
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1861.0 current = 7729.0
previous = 7729.0 current = 7729.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4), (move-dir ?x3 ?x4 ?x6)]}
Time : 6050.579
Syntactical distance : 0.0037037039
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 12248.0 current = 12248.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 945.771
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 6483.0 current = 12116.0
previous = 12116.0 current = 12116.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 4551.481
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1443.0 current = 10923.0
previous = 10923.0 current = 10965.0
previous = 10965.0 current = 10965.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 8149.622
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 6351
x+ mean size : 31.3
x- mean size : 20.798141
E+ size : 100
E- size : 38692
e+ mean size : 56.16
e- mean size : 36.371395
Automaton Time : 84.733
33 17
33 17
Recall = 0.33
Precision = 0.66
Automaton Fscore : 0.44000003
#Observed states : 939.0
#States : 100
#Transitions : 127
Compression level : 9.39
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 8998.0 current = 8998.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 1568.071
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2174.0 current = 8993.0
previous = 8993.0 current = 8993.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 4335.236
Syntactical distance : 0.022222223
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1531.0 current = 8705.0
previous = 8705.0 current = 8705.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 8916.113
Syntactical distance : 0.011111111
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 13717.0 current = 13717.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 1311.256
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 4055.0 current = 13617.0
previous = 13617.0 current = 13617.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 5202.842
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1882.0 current = 9872.0
previous = 9872.0 current = 12411.0
previous = 12411.0 current = 12411.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 11553.14
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 5784
x+ mean size : 29.1
x- mean size : 20.573133
E+ size : 100
E- size : 35393
e+ mean size : 51.39
e- mean size : 35.30701
Automaton Time : 72.482
37 22
37 22
Recall = 0.37
Precision = 0.62711865
Automaton Fscore : 0.46540883
#Observed states : 873.0
#States : 99
#Transitions : 125
Compression level : 8.818182
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 8236.0 current = 8241.0
previous = 8241.0 current = 8241.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (move-dir ?x4 ?x5 ?x6), (clear ?x3), (at ?x1 ?x4)]}
Time : 3341.778
Syntactical distance : 0.0037037039
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1300.0 current = 1620.0
previous = 1620.0 current = 1620.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4), (move-dir ?x3 ?x4 ?x6)]}
Time : 4325.367
Syntactical distance : 0.022222223
Intermediate FSCORE : 0.0
Temoral FSCORE : 0.0

*** Noise = 10.0% ***
previous = 1706.0 current = 7923.0
previous = 7923.0 current = 7924.0
previous = 7924.0 current = 7924.0
{move=[(move-dir ?x2 ?x3 ?x4), (clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 8264.653
Syntactical distance : 0.024074076
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 12626.0 current = 12626.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 1132.86
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2541.0 current = 12499.0
previous = 12499.0 current = 12499.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 5216.487
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 2557.0 current = 11372.0
previous = 11372.0 current = 11372.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 7810.025
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 6450
x+ mean size : 31.1
x- mean size : 20.57938
E+ size : 100
E- size : 36260
e+ mean size : 51.51
e- mean size : 35.400303
Automaton Time : 66.539
50 16
50 16
Recall = 0.5
Precision = 0.75757575
Automaton Fscore : 0.60240966
#Observed states : 933.0
#States : 92
#Transitions : 116
Compression level : 10.141304
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 9150.0 current = 9151.0
previous = 9151.0 current = 9151.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 3660.076
Syntactical distance : 0.011111111
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 8564.0 current = 9050.0
previous = 9050.0 current = 9050.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 4326.789
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 2896.0 current = 8576.0
previous = 8576.0 current = 8576.0
{move=[(clear ?x2), (not (at ?x1 ?x2)), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4), (move-dir ?x3 ?x4 ?x6)]}
Time : 9818.112
Syntactical distance : 0.042592596
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.17064847
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 13832.0 current = 13832.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 1591.07
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2923.0 current = 13703.0
previous = 13703.0 current = 13703.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 6701.171
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1589.0 current = 12418.0
previous = 12418.0 current = 12459.0
previous = 12459.0 current = 12459.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 13692.129
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 6374
x+ mean size : 30.8
x- mean size : 19.241137
E+ size : 100
E- size : 36428
e+ mean size : 52.17
e- mean size : 33.934555
Automaton Time : 87.406
39 25
39 25
Recall = 0.39
Precision = 0.609375
Automaton Fscore : 0.47560972
#Observed states : 924.0
#States : 105
#Transitions : 132
Compression level : 8.8
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 9006.0 current = 9006.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 945.252
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2667.0 current = 3078.0
previous = 3078.0 current = 3078.0
{move=[(clear ?x2)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 2376.667
Syntactical distance : 0.035185184
Intermediate FSCORE : 0.004842615
Temoral FSCORE : 0.004842615

*** Noise = 10.0% ***
previous = 1922.0 current = 8659.0
previous = 8659.0 current = 8659.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 6456.785
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 13690.0 current = 13690.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 940.901
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 6554.0 current = 13567.0
previous = 13567.0 current = 13567.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 4223.447
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 2725.0 current = 12261.0
previous = 12261.0 current = 12261.0
{move=[(clear ?x2), (at ?x1 ?x3)], push=[(at ?x2 ?x5), (clear ?x3), (at ?x1 ?x4)]}
Time : 6786.906
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
