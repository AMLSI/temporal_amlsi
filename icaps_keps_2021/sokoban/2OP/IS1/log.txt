# actions 28
# predicate 44
Initial state : pddl/temporal/sokoban/initial_states/initial1.pddl
pddl/temporal/sokoban/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 6319
x+ mean size : 18.0
x- mean size : 11.887324
E+ size : 100
E- size : 41959
e+ mean size : 36.02
e- mean size : 22.11764
Automaton Time : 3.779
100 0
100 0
Recall = 1.0
Precision = 1.0
Automaton Fscore : 1.0
#Observed states : 540.0
#States : 23
#Transitions : 28
Compression level : 23.47826
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 8088.0 current = 8089.0
previous = 8089.0 current = 8089.0
Time : 651.695
Syntactical distance : 0.011111111
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 7718.0 current = 8007.0
previous = 8007.0 current = 8007.0
Time : 807.469
Syntactical distance : 0.011111111
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1578.0 current = 7845.0
previous = 7845.0 current = 7847.0
previous = 7847.0 current = 7847.0
Time : 2382.304
Syntactical distance : 0.0037037039
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 11311.0 current = 11311.0
Time : 311.857
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 5271.0 current = 11209.0
previous = 11209.0 current = 11209.0
Time : 1324.173
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 2149.0 current = 10310.0
previous = 10310.0 current = 10310.0
Time : 2073.954
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 6442
x+ mean size : 18.8
x- mean size : 12.288109
E+ size : 100
E- size : 39113
e+ mean size : 33.42
e- mean size : 21.57334
Automaton Time : 3.478
100 0
100 0
Recall = 1.0
Precision = 1.0
Automaton Fscore : 1.0
#Observed states : 564.0
#States : 23
#Transitions : 28
Compression level : 24.52174
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 8265.0 current = 8267.0
previous = 8267.0 current = 8267.0
Time : 874.271
Syntactical distance : 0.011111111
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1749.0 current = 8250.0
previous = 8250.0 current = 8250.0
Time : 1929.538
Syntactical distance : 0.05
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1538.0 current = 8026.0
previous = 8026.0 current = 8026.0
Time : 2187.145
Syntactical distance : 0.022222223
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 11584.0 current = 11584.0
Time : 433.145
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 3732.0 current = 11508.0
previous = 11508.0 current = 11508.0
Time : 1851.26
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 4072.0 current = 10608.0
previous = 10608.0 current = 10608.0
Time : 2939.166
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 6726
x+ mean size : 18.6
x- mean size : 12.370502
E+ size : 100
E- size : 47617
e+ mean size : 39.24
e- mean size : 23.571371
Automaton Time : 4.094
100 0
100 0
Recall = 1.0
Precision = 1.0
Automaton Fscore : 1.0
#Observed states : 558.0
#States : 23
#Transitions : 28
Compression level : 24.26087
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 8527.0 current = 8527.0
Time : 357.713
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2685.0 current = 8505.0
previous = 8505.0 current = 8505.0
Time : 1177.113
Syntactical distance : 0.022222223
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 990.0 current = 3375.0
previous = 3375.0 current = 3375.0
Time : 1622.24
Syntactical distance : 0.018518519
Intermediate FSCORE : 0.058252428
Temoral FSCORE : 0.058252428
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 11736.0 current = 11736.0
Time : 357.793
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2652.0 current = 11667.0
previous = 11667.0 current = 11667.0
Time : 1575.794
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 2249.0 current = 10218.0
previous = 10218.0 current = 10218.0
Time : 2480.041
Syntactical distance : 0.018518519
Intermediate FSCORE : 0.26385227
Temoral FSCORE : 0.26385227

RPNIR run : 3
I+ size : 30
I- size : 7237
x+ mean size : 21.066668
x- mean size : 12.881028
E+ size : 100
E- size : 42282
e+ mean size : 35.12
e- mean size : 22.872948
Automaton Time : 4.614
100 0
100 0
Recall = 1.0
Precision = 1.0
Automaton Fscore : 1.0
#Observed states : 632.0
#States : 23
#Transitions : 28
Compression level : 27.47826
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 9279.0 current = 9279.0
Time : 594.578
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 5681.0 current = 9254.0
previous = 9254.0 current = 9254.0
Time : 2446.885
Syntactical distance : 0.011111111
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1818.0 current = 8902.0
previous = 8902.0 current = 8902.0
Time : 3518.288
Syntactical distance : 0.04074074
Intermediate FSCORE : 0.32000002
Temoral FSCORE : 0.32000002
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 12951.0 current = 12951.0
Time : 543.485
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 4313.0 current = 12856.0
previous = 12856.0 current = 12856.0
Time : 2722.02
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 2575.0 current = 11824.0
previous = 11824.0 current = 11824.0
Time : 3612.529
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 7127
x+ mean size : 20.933332
x- mean size : 12.58145
E+ size : 100
E- size : 38435
e+ mean size : 32.34
e- mean size : 21.48703
Automaton Time : 4.632
100 0
100 0
Recall = 1.0
Precision = 1.0
Automaton Fscore : 1.0
#Observed states : 628.0
#States : 23
#Transitions : 28
Compression level : 27.304348
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 9149.0 current = 9149.0
Time : 371.464
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2158.0 current = 9123.0
previous = 9123.0 current = 9123.0
Time : 980.466
Syntactical distance : 0.011111111
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1498.0 current = 8793.0
previous = 8793.0 current = 8793.0
Time : 2453.271
Syntactical distance : 0.04074074
Intermediate FSCORE : 0.31496063
Temoral FSCORE : 0.31496063
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 12851.0 current = 12851.0
Time : 373.635
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2172.0 current = 12768.0
previous = 12768.0 current = 12768.0
Time : 1746.401
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1364.0 current = 11778.0
previous = 11778.0 current = 11778.0
Time : 2561.939
Syntactical distance : 0.0
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
