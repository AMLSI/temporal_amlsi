(define (domain sokoban-temporal)
(:requirements :strips :typing :negative-preconditions)
(:types 
location t1 direction - object
stone player - t1
)(:predicates
	(at ?x1 - T1 ?x2 - LOCATION)
	(clear ?x1 - LOCATION)
	(move-dir ?x1 - LOCATION ?x2 - LOCATION ?x3 - DIRECTION)
)
(:action move-start
	:parameters ( ?x1 - PLAYER ?x2 - LOCATION ?x3 - LOCATION ?x4 - DIRECTION )
	:precondition (and
		(clear ?x3)
		(not (clear ?x2))
		(at ?x1 ?x2)
		(not (at ?x1 ?x3))
	)
	:effect (and
		(not (clear ?x3))
		(not (at ?x1 ?x2))
	)
)
(:action move-end
	:parameters ( ?x1 - PLAYER ?x2 - LOCATION ?x3 - LOCATION ?x4 - DIRECTION )
	:precondition (and
	)
	:effect (and
		(clear ?x2)
		(at ?x1 ?x3)
	)
)
(:action move-inv
	:parameters ( ?x1 - PLAYER ?x2 - LOCATION ?x3 - LOCATION ?x4 - DIRECTION )
	:precondition (and
		(move-dir ?x2 ?x3 ?x4)
	)
	:effect (and
	)
)
(:action push-start
	:parameters ( ?x1 - PLAYER ?x2 - STONE ?x3 - LOCATION ?x4 - LOCATION ?x5 - LOCATION ?x6 - DIRECTION )
	:precondition (and
		(not (at ?x2 ?x5))
		(at ?x2 ?x4)
		(not (clear ?x3))
		(clear ?x5)
		(not (at ?x1 ?x4))
		(at ?x1 ?x3)
	)
	:effect (and
		(not (at ?x2 ?x4))
		(not (clear ?x5))
		(not (at ?x1 ?x3))
	)
)
(:action push-end
	:parameters ( ?x1 - PLAYER ?x2 - STONE ?x3 - LOCATION ?x4 - LOCATION ?x5 - LOCATION ?x6 - DIRECTION )
	:precondition (and
	)
	:effect (and
		(at ?x2 ?x5)
		(clear ?x3)
		(at ?x1 ?x4)
	)
)
(:action push-inv
	:parameters ( ?x1 - PLAYER ?x2 - STONE ?x3 - LOCATION ?x4 - LOCATION ?x5 - LOCATION ?x6 - DIRECTION )
	:precondition (and
		(not (at ?x2 ?x3))
		(move-dir ?x4 ?x5 ?x6)
		(not (clear ?x4))
		(not (at ?x1 ?x5))
		(move-dir ?x3 ?x4 ?x6)
	)
	:effect (and
	)
)
)