(define (domain turnandopen-strips)
(:requirements :strips :typing :negative-preconditions)
(:types 
robot door obj gripper room - object
)(:predicates
	(at-robby ?r - ROBOT ?x - ROOM)
	(at ?o - OBJ ?x - ROOM)
	(free ?r - ROBOT ?g - GRIPPER)
	(carry ?r - ROBOT ?o - OBJ ?g - GRIPPER)
	(connected ?x - ROOM ?y - ROOM ?d - DOOR)
	(open ?d - DOOR)
	(closed ?d - DOOR)
	(doorknob-turned ?d - DOOR ?g - GRIPPER)
)
(:action turn-doorknob-start
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR ?g - GRIPPER )
	:precondition (and
		(free ?r ?g)
		(closed ?d)
	)
	:effect (and
		(not (free ?r ?g))
		(doorknob-turned ?d ?g)
	)
)
(:action turn-doorknob-end
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR ?g - GRIPPER )
	:precondition (and
	)
	:effect (and
		(free ?r ?g)
		(not (doorknob-turned ?d ?g))
	)
)
(:action turn-doorknob-inv
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR ?g - GRIPPER )
	:precondition (and
		(at-robby ?r ?from)
		(connected ?from ?to ?d)
	)
	:effect (and
	)
)
(:action open-door-start
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR ?g - GRIPPER )
	:precondition (and
		(closed ?d)
	)
	:effect (and
		(not (closed ?d))
	)
)
(:action open-door-end
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR ?g - GRIPPER )
	:precondition (and
	)
	:effect (and
		(open ?d)
	)
)
(:action open-door-inv
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR ?g - GRIPPER )
	:precondition (and
		(at-robby ?r ?from)
		(connected ?from ?to ?d)
		(doorknob-turned ?d ?g)
	)
	:effect (and
	)
)
(:action move-start
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR )
	:precondition (and
		(at-robby ?r ?from)
	)
	:effect (and
		(not (at-robby ?r ?from))
	)
)
(:action move-end
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR )
	:precondition (and
	)
	:effect (and
		(at-robby ?r ?to)
	)
)
(:action move-inv
	:parameters ( ?r - ROBOT ?from - ROOM ?to - ROOM ?d - DOOR )
	:precondition (and
		(connected ?from ?to ?d)
		(open ?d)
	)
	:effect (and
	)
)
(:action pick-start
	:parameters ( ?r - ROBOT ?obj - OBJ ?room - ROOM ?g - GRIPPER )
	:precondition (and
		(at ?obj ?room)
		(at-robby ?r ?room)
		(free ?r ?g)
	)
	:effect (and
		(not (at ?obj ?room))
		(not (free ?r ?g))
	)
)
(:action pick-end
	:parameters ( ?r - ROBOT ?obj - OBJ ?room - ROOM ?g - GRIPPER )
	:precondition (and
	)
	:effect (and
		(carry ?r ?obj ?g)
	)
)
(:action pick-inv
	:parameters ( ?r - ROBOT ?obj - OBJ ?room - ROOM ?g - GRIPPER )
	:precondition (and
	)
	:effect (and
	)
)
(:action drop-start
	:parameters ( ?r - ROBOT ?obj - OBJ ?room - ROOM ?g - GRIPPER )
	:precondition (and
		(carry ?r ?obj ?g)
		(at-robby ?r ?room)
	)
	:effect (and
		(not (carry ?r ?obj ?g))
	)
)
(:action drop-end
	:parameters ( ?r - ROBOT ?obj - OBJ ?room - ROOM ?g - GRIPPER )
	:precondition (and
	)
	:effect (and
		(at ?obj ?room)
		(free ?r ?g)
	)
)
(:action drop-inv
	:parameters ( ?r - ROBOT ?obj - OBJ ?room - ROOM ?g - GRIPPER )
	:precondition (and
	)
	:effect (and
	)
)
)