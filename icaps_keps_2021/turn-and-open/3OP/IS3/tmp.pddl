(define (domain turnandopen-strips)
(:requirements :strips :typing :negative-preconditions)
(:types 
robot door obj gripper room - object
)(:predicates
	(free ?x1 - ROBOT ?x2 - GRIPPER)
	(closed ?x1 - DOOR)
	(doorknob-turned ?x1 - DOOR ?x2 - GRIPPER)
	(at-robby ?x1 - ROBOT ?x2 - ROOM)
	(open ?x1 - DOOR)
	(at ?x1 - OBJ ?x2 - ROOM)
	(carry ?x1 - ROBOT ?x2 - OBJ ?x3 - GRIPPER)
	(connected ?x1 - ROOM ?x2 - ROOM ?x3 - DOOR)
)
(:action drop-start
	:parameters ( ?x1 - ROBOT ?x2 - OBJ ?x3 - ROOM ?x4 - GRIPPER )
	:precondition (and
		(at-robby ?x1 ?x3)
		(not (at ?x2 ?x3))
		(free ?x1 ?x4)
		(carry ?x1 ?x2 ?x4)
	)
	:effect (and
		(not (free ?x1 ?x4))
		(not (carry ?x1 ?x2 ?x4))
	)
)
(:action drop-end
	:parameters ( ?x1 - ROBOT ?x2 - OBJ ?x3 - ROOM ?x4 - GRIPPER )
	:precondition (and
	)
	:effect (and
		(at ?x2 ?x3)
		(free ?x1 ?x4)
	)
)
(:action drop-inv
	:parameters ( ?x1 - ROBOT ?x2 - OBJ ?x3 - ROOM ?x4 - GRIPPER )
	:precondition (and
	)
	:effect (and
	)
)
(:action move-start
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR )
	:precondition (and
		(not (at-robby ?x1 ?x3))
		(at-robby ?x1 ?x2)
		(not (closed ?x4))
	)
	:effect (and
		(not (at-robby ?x1 ?x2))
		(closed ?x4)
	)
)
(:action move-end
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR )
	:precondition (and
		(closed ?x4)
	)
	:effect (and
		(at-robby ?x1 ?x3)
		(not (closed ?x4))
	)
)
(:action move-inv
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR )
	:precondition (and
		(connected ?x3 ?x2 ?x4)
		(open ?x4)
		(connected ?x2 ?x3 ?x4)
	)
	:effect (and
	)
)
(:action open-door-start
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR ?x5 - GRIPPER )
	:precondition (and
		(closed ?x4)
		(not (open ?x4))
	)
	:effect (and
		(doorknob-turned ?x4 ?x5)
	)
)
(:action open-door-end
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR ?x5 - GRIPPER )
	:precondition (and
		(doorknob-turned ?x4 ?x5)
	)
	:effect (and
		(open ?x4)
	)
)
(:action open-door-inv
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR ?x5 - GRIPPER )
	:precondition (and
		(connected ?x3 ?x2 ?x4)
		(not (at-robby ?x1 ?x3))
		(at-robby ?x1 ?x2)
		(connected ?x2 ?x3 ?x4)
	)
	:effect (and
	)
)
(:action pick-start
	:parameters ( ?x1 - ROBOT ?x2 - OBJ ?x3 - ROOM ?x4 - GRIPPER )
	:precondition (and
		(at-robby ?x1 ?x3)
		(at ?x2 ?x3)
		(free ?x1 ?x4)
		(not (carry ?x1 ?x2 ?x4))
	)
	:effect (and
		(not (at ?x2 ?x3))
		(not (free ?x1 ?x4))
	)
)
(:action pick-end
	:parameters ( ?x1 - ROBOT ?x2 - OBJ ?x3 - ROOM ?x4 - GRIPPER )
	:precondition (and
	)
	:effect (and
		(free ?x1 ?x4)
		(carry ?x1 ?x2 ?x4)
	)
)
(:action pick-inv
	:parameters ( ?x1 - ROBOT ?x2 - OBJ ?x3 - ROOM ?x4 - GRIPPER )
	:precondition (and
	)
	:effect (and
	)
)
(:action turn-doorknob-start
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR ?x5 - GRIPPER )
	:precondition (and
		(not (doorknob-turned ?x4 ?x5))
		(not (open ?x4))
		(free ?x1 ?x5)
	)
	:effect (and
		(closed ?x4)
	)
)
(:action turn-doorknob-end
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR ?x5 - GRIPPER )
	:precondition (and
		(closed ?x4)
	)
	:effect (and
		(not (closed ?x4))
		(free ?x1 ?x5)
	)
)
(:action turn-doorknob-inv
	:parameters ( ?x1 - ROBOT ?x2 - ROOM ?x3 - ROOM ?x4 - DOOR ?x5 - GRIPPER )
	:precondition (and
		(connected ?x3 ?x2 ?x4)
		(not (at-robby ?x1 ?x3))
		(at-robby ?x1 ?x2)
		(connected ?x2 ?x3 ?x4)
	)
	:effect (and
	)
)
)