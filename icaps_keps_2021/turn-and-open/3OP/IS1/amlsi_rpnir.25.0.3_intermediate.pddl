(define (domain turnandopen-strips)
(:requirements :strips :typing :negative-preconditions)
(:types
room door obj gripper robot - object
)
(:predicates
	(free ?x1 - robot ?x2 - gripper)
	(closed ?x1 - door)
	(doorknob-turned ?x1 - door ?x2 - gripper)
	(at-robby ?x1 - robot ?x2 - room)
	(open ?x1 - door)
	(at ?x1 - obj ?x2 - room)
	(carry ?x1 - robot ?x2 - obj ?x3 - gripper)
	(connected ?x1 - room ?x2 - room ?x3 - door)
)
(:action drop-start
	:parameters (?x1 - robot ?x2 - obj ?x3 - room ?x4 - gripper )
	:precondition (and
	(at-robby ?x1 ?x3)
	(not(at ?x2 ?x3))
	(not(free ?x1 ?x4))
	(carry ?x1 ?x2 ?x4))
	:effect (and
	(not(carry ?x1 ?x2 ?x4)))
)
(:action drop-end
	:parameters (?x1 - robot ?x2 - obj ?x3 - room ?x4 - gripper )
	:precondition (and)
	:effect (and
	(at ?x2 ?x3)
	(free ?x1 ?x4))
)
(:action move-start
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door )
	:precondition (and
	(not(at-robby ?x1 ?x3))
	(at-robby ?x1 ?x2))
	:effect (and
	(not(at-robby ?x1 ?x2)))
)
(:action move-end
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door )
	:precondition (and)
	:effect (and
	(at-robby ?x1 ?x3))
)
(:action turn-doorknob-start
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door ?x5 - gripper )
	:precondition (and
	(closed ?x4)
	(not(doorknob-turned ?x4 ?x5))
	(not(open ?x4))
	(free ?x1 ?x5))
	:effect (and
	(doorknob-turned ?x4 ?x5)
	(not(free ?x1 ?x5)))
)
(:action turn-doorknob-end
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door ?x5 - gripper )
	:precondition (and
	(doorknob-turned ?x4 ?x5))
	:effect (and
	(at-robby ?x1 ?x2)
	(not(doorknob-turned ?x4 ?x5))
	(free ?x1 ?x5))
)
(:action pick-start
	:parameters (?x1 - robot ?x2 - obj ?x3 - room ?x4 - gripper )
	:precondition (and
	(at-robby ?x1 ?x3)
	(at ?x2 ?x3)
	(free ?x1 ?x4)
	(not(carry ?x1 ?x2 ?x4)))
	:effect (and
	(not(at ?x2 ?x3))
	(not(free ?x1 ?x4)))
)
(:action pick-end
	:parameters (?x1 - robot ?x2 - obj ?x3 - room ?x4 - gripper )
	:precondition (and)
	:effect (and
	(carry ?x1 ?x2 ?x4))
)
(:action open-door-start
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door ?x5 - gripper )
	:precondition (and
	(closed ?x4)
	(not(open ?x4)))
	:effect (and
	(connected ?x3 ?x2 ?x4)
	(not(closed ?x4))
	(connected ?x2 ?x3 ?x4))
)
(:action open-door-end
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door ?x5 - gripper )
	:precondition (and
	(connected ?x3 ?x2 ?x4)
	(connected ?x2 ?x3 ?x4))
	:effect (and
	(at-robby ?x1 ?x2)
	(open ?x4))
)
(:action pick-inv
	:parameters (?x1 - robot ?x2 - obj ?x3 - room ?x4 - gripper )
	:precondition (and)
	:effect (and)
)
(:action drop-inv
	:parameters (?x1 - robot ?x2 - obj ?x3 - room ?x4 - gripper )
	:precondition (and)
	:effect (and)
)
(:action turn-doorknob-inv
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door ?x5 - gripper )
	:precondition (and
	(connected ?x3 ?x2 ?x4)
	(not(at-robby ?x1 ?x3))
	(at-robby ?x1 ?x2)
	(connected ?x2 ?x3 ?x4))
	:effect (and)
)
(:action open-door-inv
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door ?x5 - gripper )
	:precondition (and
	(not(at-robby ?x1 ?x3))
	(at-robby ?x1 ?x2)
	(doorknob-turned ?x4 ?x5)
	(not(free ?x1 ?x5)))
	:effect (and)
)
(:action move-inv
	:parameters (?x1 - robot ?x2 - room ?x3 - room ?x4 - door )
	:precondition (and
	(connected ?x3 ?x2 ?x4)
	(not(closed ?x4))
	(open ?x4)
	(connected ?x2 ?x3 ?x4))
	:effect (and)
)
)
