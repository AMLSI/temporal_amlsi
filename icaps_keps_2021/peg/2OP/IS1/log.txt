# actions 12
# predicate 30
Initial state : pddl/temporal/peg/initial_states/initial1.pddl
pddl/temporal/peg/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1294
x+ mean size : 8.933333
x- mean size : 8.304482
E+ size : 100
E- size : 4176
e+ mean size : 8.86
e- mean size : 8.26772
Automaton Time : 4.164
68 1
68 1
Recall = 0.68
Precision = 0.98550725
Automaton Fscore : 0.8047337
#Observed states : 268.0
#States : 45
#Transitions : 82
Compression level : 5.9555554
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 2129.0 current = 2135.0
previous = 2135.0 current = 2135.0
Time : 22.934
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1493.0 current = 2142.0
previous = 2142.0 current = 2142.0
Time : 30.875
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 380.0 current = 1769.0
previous = 1769.0 current = 1769.0
Time : 45.852
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.7092199
Temoral FSCORE : 0.7092199
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 4146.0 current = 4146.0
Time : 10.934
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1304.0 current = 4069.0
previous = 4069.0 current = 4069.0
Time : 40.615
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 268.0 current = 3598.0
previous = 3598.0 current = 3598.0
Time : 54.221
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1207
x+ mean size : 8.466666
x- mean size : 7.991715
E+ size : 100
E- size : 4280
e+ mean size : 8.9
e- mean size : 8.270561
Automaton Time : 3.432
74 16
74 16
Recall = 0.74
Precision = 0.82222223
Automaton Fscore : 0.77894735
#Observed states : 253.99998
#States : 42
#Transitions : 83
Compression level : 6.047619
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1963.0 current = 1971.0
previous = 1971.0 current = 1971.0
Time : 21.055
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 390.0 current = 1967.0
previous = 1967.0 current = 1967.0
Time : 34.241
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = -14.0 current = 1874.0
previous = 1874.0 current = 1874.0
Time : 48.47
Syntactical distance : 0.0462963
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3877.0 current = 3877.0
Time : 10.581
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1742.0 current = 3807.0
previous = 3807.0 current = 3807.0
Time : 36.495
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 254.0 current = 3284.0
previous = 3284.0 current = 3284.0
Time : 48.038
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1353
x+ mean size : 8.933333
x- mean size : 8.2158165
E+ size : 100
E- size : 4376
e+ mean size : 8.94
e- mean size : 8.199268
Automaton Time : 4.464
75 2
75 2
Recall = 0.75
Precision = 0.97402596
Automaton Fscore : 0.84745765
#Observed states : 268.0
#States : 49
#Transitions : 92
Compression level : 5.4693875
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 2152.0 current = 2152.0
Time : 12.845
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1638.0 current = 2187.0
previous = 2187.0 current = 2187.0
Time : 29.698
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 268.0 current = 1847.0
previous = 1847.0 current = 1847.0
Time : 58.727
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.63492066
Temoral FSCORE : 0.63492066
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 4222.0 current = 4222.0
Time : 13.072
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2332.0 current = 4175.0
previous = 4175.0 current = 4175.0
Time : 42.009
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 268.0 current = 3659.0
previous = 3659.0 current = 3659.0
Time : 59.835
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1216
x+ mean size : 8.6
x- mean size : 7.8388157
E+ size : 100
E- size : 4255
e+ mean size : 8.76
e- mean size : 8.181904
Automaton Time : 3.488
63 16
63 16
Recall = 0.63
Precision = 0.79746836
Automaton Fscore : 0.70391065
#Observed states : 258.0
#States : 44
#Transitions : 86
Compression level : 5.8636365
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1985.0 current = 1985.0
Time : 10.184
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1528.0 current = 1989.0
previous = 1989.0 current = 1989.0
Time : 23.186
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 14.0 current = 736.0
previous = 736.0 current = 736.0
Time : 37.259
Syntactical distance : 0.027777778
Intermediate FSCORE : 0.0
Temoral FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3934.0 current = 3934.0
Time : 9.916
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = -72.0 current = 3872.0
previous = 3872.0 current = 3872.0
Time : 41.166
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 258.0 current = 3312.0
previous = 3312.0 current = 3312.0
Time : 44.65
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1310
x+ mean size : 8.666667
x- mean size : 8.061069
E+ size : 100
E- size : 4349
e+ mean size : 8.84
e- mean size : 8.266728
Automaton Time : 4.612
82 14
82 14
Recall = 0.82
Precision = 0.8541667
Automaton Fscore : 0.8367347
#Observed states : 260.0
#States : 48
#Transitions : 94
Compression level : 5.4166665
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 2111.0 current = 2111.0
Time : 9.93
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1957.0 current = 2038.0
previous = 2038.0 current = 2038.0
Time : 25.156
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 260.0 current = 1740.0
previous = 1740.0 current = 1740.0
Time : 43.713
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.6944444
Temoral FSCORE : 0.6944444
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 4072.0 current = 4072.0
Time : 10.153
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 648.0 current = 3981.0
previous = 3981.0 current = 3981.0
Time : 43.359
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 260.0 current = 3532.0
previous = 3532.0 current = 3532.0
Time : 49.554
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
