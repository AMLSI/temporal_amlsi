(define (domain pegsolitaire-temporal)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(occupied ?x1 - location)
	(free ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-start
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(in-line ?x1 ?x2 ?x3)
	(in-line ?x3 ?x2 ?x1)
	(not(free ?x2))
	(occupied ?x1)
	(not(free ?x1))
	(occupied ?x2)
	(not(occupied ?x3))
	(free ?x3))
	:effect (and
	(not(occupied ?x1))
	(not(occupied ?x2))
	(not(free ?x3)))
)
(:action jump-end
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(in-line ?x3 ?x2 ?x1)
	(not(occupied ?x1))
	(not(occupied ?x2))
	(not(free ?x3)))
	:effect (and
	(in-line ?x1 ?x2 ?x3)
	(free ?x2)
	(free ?x1)
	(occupied ?x3))
)
)
