(define (domain pegsolitaire-temporal)
(:requirements :strips :typing :negative-preconditions)
(:types 
location - object
)(:predicates
	(occupied ?x1 - LOCATION)
	(free ?x1 - LOCATION)
	(in-line ?x1 - LOCATION ?x2 - LOCATION ?x3 - LOCATION)
)
(:action jump-start
	:parameters ( ?x1 - LOCATION ?x2 - LOCATION ?x3 - LOCATION )
	:precondition (and
		(not (free ?x2))
		(occupied ?x1)
		(not (free ?x1))
		(occupied ?x2)
		(not (occupied ?x3))
		(free ?x3)
	)
	:effect (and
		(not (occupied ?x1))
		(not (occupied ?x2))
		(not (free ?x3))
	)
)
(:action jump-end
	:parameters ( ?x1 - LOCATION ?x2 - LOCATION ?x3 - LOCATION )
	:precondition (and
	)
	:effect (and
		(free ?x2)
		(free ?x1)
		(occupied ?x3)
	)
)
(:action jump-inv
	:parameters ( ?x1 - LOCATION ?x2 - LOCATION ?x3 - LOCATION )
	:precondition (and
		(in-line ?x1 ?x2 ?x3)
		(in-line ?x3 ?x2 ?x1)
	)
	:effect (and
	)
)
)