# actions 12
# predicate 30
Initial state : pddl/temporal/peg/initial_states/initial2.pddl
pddl/temporal/peg/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1094
x+ mean size : 6.3333335
x- mean size : 6.3199267
E+ size : 100
E- size : 3594
e+ mean size : 6.5
e- mean size : 6.420701
Automaton Time : 0.644
77 6
77 6
Recall = 0.77
Precision = 0.92771083
Automaton Fscore : 0.8415301
#Observed states : 190.0
#States : 26
#Transitions : 44
Compression level : 7.3076925
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1619.0 current = 1619.0
Time : 10.183
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1601.0 current = 1626.0
previous = 1626.0 current = 1626.0
Time : 16.811
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 558.0 current = 1528.0
previous = 1528.0 current = 1528.0
Time : 30.0
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3004.0 current = 3004.0
Time : 7.382
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1799.0 current = 2972.0
previous = 2972.0 current = 2972.0
Time : 22.018
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 190.0 current = 2571.0
previous = 2571.0 current = 2571.0
Time : 33.692
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1118
x+ mean size : 6.5333333
x- mean size : 6.484794
E+ size : 100
E- size : 3585
e+ mean size : 6.66
e- mean size : 6.5506277
Automaton Time : 0.813
82 0
82 0
Recall = 0.82
Precision = 1.0
Automaton Fscore : 0.9010989
#Observed states : 196.0
#States : 30
#Transitions : 55
Compression level : 6.5333333
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1678.0 current = 1688.0
previous = 1688.0 current = 1688.0
Time : 22.175
Syntactical distance : 0.0462963
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1602.0 current = 1644.0
previous = 1644.0 current = 1644.0
Time : 17.934
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 348.0 current = 1607.0
previous = 1607.0 current = 1607.0
Time : 38.55
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3103.0 current = 3103.0
Time : 8.592
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1462.0 current = 3060.0
previous = 3060.0 current = 3060.0
Time : 29.888
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 196.0 current = 2695.0
previous = 2695.0 current = 2695.0
Time : 39.294
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1134
x+ mean size : 6.733333
x- mean size : 6.4761906
E+ size : 100
E- size : 3707
e+ mean size : 6.7
e- mean size : 6.5940113
Automaton Time : 0.839
86 0
86 0
Recall = 0.86
Precision = 1.0
Automaton Fscore : 0.9247312
#Observed states : 202.0
#States : 32
#Transitions : 56
Compression level : 6.3125
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1723.0 current = 1723.0
Time : 8.742
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1627.0 current = 1724.0
previous = 1724.0 current = 1724.0
Time : 22.374
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 292.0 current = 1395.0
previous = 1395.0 current = 1564.0
previous = 1564.0 current = 1564.0
Time : 63.127
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3189.0 current = 3189.0
Time : 8.955
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 892.0 current = 3113.0
previous = 3113.0 current = 3113.0
Time : 36.41
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 202.0 current = 2772.0
previous = 2772.0 current = 2772.0
Time : 42.059
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1099
x+ mean size : 6.6666665
x- mean size : 6.651501
E+ size : 100
E- size : 3598
e+ mean size : 6.48
e- mean size : 6.4196777
Automaton Time : 0.772
85 0
85 0
Recall = 0.85
Precision = 1.0
Automaton Fscore : 0.9189189
#Observed states : 200.0
#States : 32
#Transitions : 53
Compression level : 6.25
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1660.0 current = 1660.0
Time : 10.197
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1088.0 current = 1654.0
previous = 1654.0 current = 1654.0
Time : 23.593
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 161.0 current = 671.0
previous = 671.0 current = 663.0
Time : 21.659
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.0
Temoral FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3128.0 current = 3128.0
Time : 8.659
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 2290.0 current = 3083.0
previous = 3083.0 current = 3083.0
Time : 23.056
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 200.0 current = 2716.0
previous = 2716.0 current = 2716.0
Time : 38.49
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1102
x+ mean size : 6.4666667
x- mean size : 6.3647914
E+ size : 100
E- size : 3537
e+ mean size : 6.3
e- mean size : 6.3404016
Automaton Time : 0.672
95 0
95 0
Recall = 0.95
Precision = 1.0
Automaton Fscore : 0.9743589
#Observed states : 194.0
#States : 30
#Transitions : 52
Compression level : 6.4666667
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1634.0 current = 1640.0
previous = 1640.0 current = 1640.0
Time : 18.261
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1105.0 current = 1605.0
previous = 1605.0 current = 1605.0
Time : 24.277
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 672.0 current = 1555.0
previous = 1555.0 current = 1555.0
Time : 35.981
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3060.0 current = 3060.0
Time : 8.452
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 883.0 current = 2995.0
previous = 2995.0 current = 2995.0
Time : 34.522
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 194.0 current = 2538.0
previous = 2538.0 current = 2538.0
Time : 38.692
Syntactical distance : 0.009259259
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
