(define (domain pegsolitaire-temporal)
(:requirements :strips :typing :negative-preconditions)
(:types 
location - object
)(:predicates
	(in-line ?x - LOCATION ?y - LOCATION ?z - LOCATION)
	(occupied ?l - LOCATION)
	(free ?l - LOCATION)
)
(:action jump-start
	:parameters ( ?from - LOCATION ?over - LOCATION ?to - LOCATION )
	:precondition (and
		(occupied ?from)
		(occupied ?over)
		(free ?to)
	)
	:effect (and
		(not (occupied ?from))
		(not (occupied ?over))
		(not (free ?to))
	)
)
(:action jump-end
	:parameters ( ?from - LOCATION ?over - LOCATION ?to - LOCATION )
	:precondition (and
	)
	:effect (and
		(free ?from)
		(free ?over)
		(occupied ?to)
	)
)
(:action jump-inv
	:parameters ( ?from - LOCATION ?over - LOCATION ?to - LOCATION )
	:precondition (and
		(in-line ?from ?over ?to)
	)
	:effect (and
	)
)
)