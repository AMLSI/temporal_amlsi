(define (domain pegsolitaire-temporal)
(:requirements :strips :typing :negative-preconditions :durative-actions)
(:types 
location - object
)(:predicates
	(occupied ?x1 - LOCATION)
	(free ?x1 - LOCATION)
	(in-line ?x1 - LOCATION ?x2 - LOCATION ?x3 - LOCATION)
)
(:durative-action jump
	:parameters ( ?x1 - LOCATION ?x2 - LOCATION ?x3 - LOCATION )
	:duration (= ?duration 1.0)
	:condition (and
		(at start (occupied ?x1) )
		(at start (occupied ?x2) )
		(at start (not (occupied ?x3)) )
		(over all (in-line ?x1 ?x2 ?x3) )
		(over all (in-line ?x3 ?x2 ?x1) )
		(over all (not (free ?x2)) )
	)
	:effect (and
		(at start (not (occupied ?x1)) )
		(at start (not (occupied ?x2)) )
		(at end (free ?x1) )
		(at end (occupied ?x3) )
	)
)
)