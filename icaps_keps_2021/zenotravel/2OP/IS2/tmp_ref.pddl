(define (domain zeno-travel)
(:requirements :strips :typing :negative-preconditions)
(:types 
flevel city localtable locatable - object
person aircraft - locatable
)(:predicates
	(at ?x - LOCATABLE ?c - CITY)
	(in ?p - PERSON ?a - AIRCRAFT)
	(fuel-level ?a - AIRCRAFT ?l - FLEVEL)
	(next ?l1 - FLEVEL ?l2 - FLEVEL)
)
(:action board-start
	:parameters ( ?p - PERSON ?a - AIRCRAFT ?c - CITY )
	:precondition (and
		(at ?p ?c)
	)
	:effect (and
		(not (at ?p ?c))
	)
)
(:action board-end
	:parameters ( ?p - PERSON ?a - AIRCRAFT ?c - CITY )
	:precondition (and
	)
	:effect (and
		(in ?p ?a)
	)
)
(:action board-inv
	:parameters ( ?p - PERSON ?a - AIRCRAFT ?c - CITY )
	:precondition (and
		(at ?a ?c)
	)
	:effect (and
	)
)
(:action debark-start
	:parameters ( ?p - PERSON ?a - AIRCRAFT ?c - CITY )
	:precondition (and
		(in ?p ?a)
	)
	:effect (and
		(not (in ?p ?a))
	)
)
(:action debark-end
	:parameters ( ?p - PERSON ?a - AIRCRAFT ?c - CITY )
	:precondition (and
	)
	:effect (and
		(at ?p ?c)
	)
)
(:action debark-inv
	:parameters ( ?p - PERSON ?a - AIRCRAFT ?c - CITY )
	:precondition (and
		(at ?a ?c)
	)
	:effect (and
	)
)
(:action fly-start
	:parameters ( ?a - AIRCRAFT ?c1 - CITY ?c2 - CITY ?l1 - FLEVEL ?l2 - FLEVEL )
	:precondition (and
		(at ?a ?c1)
		(fuel-level ?a ?l1)
		(next ?l2 ?l1)
	)
	:effect (and
		(not (at ?a ?c1))
		(not (fuel-level ?a ?l1))
	)
)
(:action fly-end
	:parameters ( ?a - AIRCRAFT ?c1 - CITY ?c2 - CITY ?l1 - FLEVEL ?l2 - FLEVEL )
	:precondition (and
	)
	:effect (and
		(at ?a ?c2)
		(fuel-level ?a ?l2)
	)
)
(:action fly-inv
	:parameters ( ?a - AIRCRAFT ?c1 - CITY ?c2 - CITY ?l1 - FLEVEL ?l2 - FLEVEL )
	:precondition (and
	)
	:effect (and
	)
)
(:action zoom-start
	:parameters ( ?a - AIRCRAFT ?c1 - CITY ?c2 - CITY ?l1 - FLEVEL ?l2 - FLEVEL ?l3 - FLEVEL )
	:precondition (and
		(at ?a ?c1)
		(fuel-level ?a ?l1)
		(next ?l2 ?l1)
		(next ?l3 ?l2)
	)
	:effect (and
		(not (at ?a ?c1))
	)
)
(:action zoom-end
	:parameters ( ?a - AIRCRAFT ?c1 - CITY ?c2 - CITY ?l1 - FLEVEL ?l2 - FLEVEL ?l3 - FLEVEL )
	:precondition (and
	)
	:effect (and
		(at ?a ?c2)
		(not (fuel-level ?a ?l1))
		(fuel-level ?a ?l3)
	)
)
(:action zoom-inv
	:parameters ( ?a - AIRCRAFT ?c1 - CITY ?c2 - CITY ?l1 - FLEVEL ?l2 - FLEVEL ?l3 - FLEVEL )
	:precondition (and
	)
	:effect (and
	)
)
(:action refuel-start
	:parameters ( ?a - AIRCRAFT ?c - CITY ?l - FLEVEL ?l1 - FLEVEL )
	:precondition (and
		(fuel-level ?a ?l)
		(next ?l ?l1)
	)
	:effect (and
		(not (fuel-level ?a ?l))
	)
)
(:action refuel-end
	:parameters ( ?a - AIRCRAFT ?c - CITY ?l - FLEVEL ?l1 - FLEVEL )
	:precondition (and
	)
	:effect (and
		(fuel-level ?a ?l1)
	)
)
(:action refuel-inv
	:parameters ( ?a - AIRCRAFT ?c - CITY ?l - FLEVEL ?l1 - FLEVEL )
	:precondition (and
		(at ?a ?c)
	)
	:effect (and
	)
)
)