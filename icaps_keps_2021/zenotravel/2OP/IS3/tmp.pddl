(define (domain zeno-travel)
(:requirements :strips :typing :negative-preconditions)
(:types 
flevel city t1 - object
person aircraft - t1
)(:predicates
	(at ?x1 - T1 ?x2 - CITY)
	(in ?x1 - PERSON ?x2 - AIRCRAFT)
	(fuel-level ?x1 - AIRCRAFT ?x2 - FLEVEL)
	(next ?x1 - FLEVEL ?x2 - FLEVEL)
)
(:action fly-start
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL )
	:precondition (and
		(not (fuel-level ?x1 ?x5))
		(fuel-level ?x1 ?x4)
		(at ?x1 ?x2)
		(not (at ?x1 ?x3))
	)
	:effect (and
		(not (fuel-level ?x1 ?x4))
		(not (at ?x1 ?x2))
	)
)
(:action fly-end
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL )
	:precondition (and
	)
	:effect (and
		(fuel-level ?x1 ?x5)
		(at ?x1 ?x3)
	)
)
(:action fly-inv
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL )
	:precondition (and
		(next ?x5 ?x4)
	)
	:effect (and
	)
)
(:action debark-start
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:precondition (and
		(in ?x1 ?x2)
		(not (at ?x1 ?x3))
	)
	:effect (and
		(not (in ?x1 ?x2))
	)
)
(:action debark-end
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:precondition (and
	)
	:effect (and
		(at ?x1 ?x3)
	)
)
(:action debark-inv
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:precondition (and
		(at ?x2 ?x3)
	)
	:effect (and
	)
)
(:action zoom-start
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL ?x6 - FLEVEL )
	:precondition (and
		(fuel-level ?x1 ?x4)
		(not (fuel-level ?x1 ?x6))
		(at ?x1 ?x2)
		(not (at ?x1 ?x3))
	)
	:effect (and
		(not (at ?x1 ?x2))
	)
)
(:action zoom-end
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL ?x6 - FLEVEL )
	:precondition (and
	)
	:effect (and
		(not (fuel-level ?x1 ?x4))
		(fuel-level ?x1 ?x6)
		(at ?x1 ?x3)
	)
)
(:action zoom-inv
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL ?x6 - FLEVEL )
	:precondition (and
		(next ?x6 ?x5)
		(not (fuel-level ?x1 ?x5))
		(next ?x5 ?x4)
	)
	:effect (and
	)
)
(:action refuel-start
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - FLEVEL ?x4 - FLEVEL )
	:precondition (and
		(fuel-level ?x1 ?x3)
		(not (fuel-level ?x1 ?x4))
	)
	:effect (and
		(not (fuel-level ?x1 ?x3))
	)
)
(:action refuel-end
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - FLEVEL ?x4 - FLEVEL )
	:precondition (and
	)
	:effect (and
		(fuel-level ?x1 ?x4)
	)
)
(:action refuel-inv
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - FLEVEL ?x4 - FLEVEL )
	:precondition (and
		(next ?x3 ?x4)
		(at ?x1 ?x2)
	)
	:effect (and
	)
)
(:action board-start
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:precondition (and
		(not (in ?x1 ?x2))
		(at ?x1 ?x3)
	)
	:effect (and
		(not (at ?x1 ?x3))
	)
)
(:action board-end
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:precondition (and
	)
	:effect (and
		(in ?x1 ?x2)
	)
)
(:action board-inv
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:precondition (and
		(at ?x2 ?x3)
	)
	:effect (and
	)
)
)