(define (domain zeno-travel)
(:requirements :strips :typing :negative-preconditions :durative-actions)
(:types 
flevel city t1 - object
person aircraft - t1
)(:predicates
	(at ?x1 - T1 ?x2 - CITY)
	(in ?x1 - PERSON ?x2 - AIRCRAFT)
	(fuel-level ?x1 - AIRCRAFT ?x2 - FLEVEL)
	(next ?x1 - FLEVEL ?x2 - FLEVEL)
)
(:durative-action fly
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL )
	:duration (= ?duration 180.0)
	:condition (and
		(at start (not (fuel-level ?x1 ?x5)) )
		(at start (fuel-level ?x1 ?x4) )
		(at start (at ?x1 ?x2) )
		(at start (not (at ?x1 ?x3)) )
		(over all (next ?x5 ?x4) )
	)
	:effect (and
		(at start (not (fuel-level ?x1 ?x4)) )
		(at start (not (at ?x1 ?x2)) )
		(at end (fuel-level ?x1 ?x5) )
		(at end (at ?x1 ?x3) )
	)
)
(:durative-action debark
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:duration (= ?duration 30.0)
	:condition (and
		(at start (in ?x1 ?x2) )
		(at start (not (at ?x1 ?x3)) )
		(over all (at ?x2 ?x3) )
	)
	:effect (and
		(at start (not (in ?x1 ?x2)) )
		(at end (at ?x1 ?x3) )
	)
)
(:durative-action zoom
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - CITY ?x4 - FLEVEL ?x5 - FLEVEL ?x6 - FLEVEL )
	:duration (= ?duration 100.0)
	:condition (and
		(at start (not (fuel-level ?x1 ?x6)) )
		(at start (not (at ?x1 ?x3)) )
		(over all (next ?x6 ?x5) )
		(over all (not (fuel-level ?x1 ?x5)) )
		(over all (fuel-level ?x1 ?x4) )
		(over all (next ?x5 ?x4) )
	)
	:effect (and
		(at end (fuel-level ?x1 ?x6) )
		(at end (at ?x1 ?x3) )
	)
)
(:durative-action refuel
	:parameters ( ?x1 - AIRCRAFT ?x2 - CITY ?x3 - FLEVEL ?x4 - FLEVEL )
	:duration (= ?duration 73.0)
	:condition (and
		(at start (fuel-level ?x1 ?x3) )
		(at start (not (fuel-level ?x1 ?x4)) )
		(over all (next ?x3 ?x4) )
		(over all (at ?x1 ?x2) )
	)
	:effect (and
		(at start (not (fuel-level ?x1 ?x3)) )
		(at end (fuel-level ?x1 ?x4) )
	)
)
(:durative-action board
	:parameters ( ?x1 - PERSON ?x2 - AIRCRAFT ?x3 - CITY )
	:duration (= ?duration 20.0)
	:condition (and
		(at start (not (in ?x1 ?x2)) )
		(over all (at ?x2 ?x3) )
		(over all (at ?x1 ?x3) )
	)
	:effect (and
		(at end (in ?x1 ?x2) )
	)
)
)