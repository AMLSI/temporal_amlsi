(define (domain zeno-travel)
(:requirements :strips :typing :negative-preconditions)
(:types
flevel city T1 - object
aircraft person - T1
)
(:predicates
	(at ?x1 - T1 ?x2 - city)
	(in ?x1 - person ?x2 - aircraft)
	(fuel-level ?x1 - aircraft ?x2 - flevel)
	(next ?x1 - flevel ?x2 - flevel)
)
(:action fly-start
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel )
	:precondition (and
	(not(fuel-level ?x1 ?x5))
	(fuel-level ?x1 ?x4)
	(at ?x1 ?x2)
	(not(at ?x1 ?x3)))
	:effect (and
	(not(fuel-level ?x1 ?x4))
	(not(at ?x1 ?x2)))
)
(:action fly-end
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel )
	:precondition (and)
	:effect (and
	(fuel-level ?x1 ?x5)
	(at ?x1 ?x3))
)
(:action debark-start
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and
	(in ?x1 ?x2)
	(not(at ?x1 ?x3)))
	:effect (and
	(not(in ?x1 ?x2)))
)
(:action debark-end
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and)
	:effect (and
	(at ?x1 ?x3))
)
(:action refuel-start
	:parameters (?x1 - aircraft ?x2 - city ?x3 - flevel ?x4 - flevel )
	:precondition (and
	(fuel-level ?x1 ?x3)
	(not(fuel-level ?x1 ?x4)))
	:effect (and
	(not(fuel-level ?x1 ?x3)))
)
(:action refuel-end
	:parameters (?x1 - aircraft ?x2 - city ?x3 - flevel ?x4 - flevel )
	:precondition (and)
	:effect (and
	(fuel-level ?x1 ?x4))
)
(:action zoom-start
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel ?x6 - flevel )
	:precondition (and
	(fuel-level ?x1 ?x4)
	(not(fuel-level ?x1 ?x6))
	(at ?x1 ?x2)
	(not(at ?x1 ?x3)))
	:effect (and
	(not(fuel-level ?x1 ?x4))
	(not(at ?x1 ?x2)))
)
(:action zoom-end
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel ?x6 - flevel )
	:precondition (and)
	:effect (and
	(fuel-level ?x1 ?x6)
	(at ?x1 ?x3))
)
(:action board-start
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and
	(not(in ?x1 ?x2))
	(at ?x1 ?x3))
	:effect (and
	(not(at ?x1 ?x3)))
)
(:action board-end
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and)
	:effect (and
	(in ?x1 ?x2))
)
(:action zoom-inv
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel ?x6 - flevel )
	:precondition (and
	(next ?x6 ?x5)
	(not(fuel-level ?x1 ?x5))
	(next ?x5 ?x4))
	:effect (and)
)
(:action refuel-inv
	:parameters (?x1 - aircraft ?x2 - city ?x3 - flevel ?x4 - flevel )
	:precondition (and
	(next ?x3 ?x4)
	(at ?x1 ?x2))
	:effect (and)
)
(:action fly-inv
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel )
	:precondition (and
	(next ?x5 ?x4))
	:effect (and)
)
(:action board-inv
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and
	(at ?x2 ?x3))
	:effect (and)
)
(:action debark-inv
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and
	(at ?x2 ?x3))
	:effect (and)
)
)
