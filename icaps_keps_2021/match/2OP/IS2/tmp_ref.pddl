(define (domain matchcellar)
(:requirements :strips :typing :negative-preconditions)
(:types 
fuse match - object
)(:predicates
	(handfree)
	(unused ?match - MATCH)
	(mended ?fuse - FUSE)
	(light ?match - MATCH)
)
(:action light_match-start
	:parameters ( ?match - MATCH )
	:precondition (and
		(unused ?match)
	)
	:effect (and
		(not (unused ?match))
		(light ?match)
	)
)
(:action light_match-end
	:parameters ( ?match - MATCH )
	:precondition (and
	)
	:effect (and
		(not (light ?match))
	)
)
(:action light_match-inv
	:parameters ( ?match - MATCH )
	:precondition (and
	)
	:effect (and
	)
)
(:action mend_fuse-start
	:parameters ( ?fuse - FUSE ?match - MATCH )
	:precondition (and
		(handfree)
		(not (mended ?fuse))
	)
	:effect (and
		(not (handfree))
	)
)
(:action mend_fuse-end
	:parameters ( ?fuse - FUSE ?match - MATCH )
	:precondition (and
	)
	:effect (and
		(mended ?fuse)
		(handfree)
	)
)
(:action mend_fuse-inv
	:parameters ( ?fuse - FUSE ?match - MATCH )
	:precondition (and
		(light ?match)
	)
	:effect (and
	)
)
)