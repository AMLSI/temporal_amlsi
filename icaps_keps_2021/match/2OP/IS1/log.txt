# actions 6
# predicate 7
Initial state : pddl/temporal/match/initial_states/initial1.pddl
pddl/temporal/match/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 446
x+ mean size : 6.0666666
x- mean size : 7.32287
E+ size : 100
E- size : 1416
e+ mean size : 6.02
e- mean size : 6.9985876
Automaton Time : 0.698
71 10
71 10
Recall = 0.71
Precision = 0.8765432
Automaton Fscore : 0.7845304
#Observed states : 182.0
#States : 23
#Transitions : 52
Compression level : 7.9130435
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 611.0 current = 677.0
previous = 677.0 current = 673.0
Time : 2.535
Syntactical distance : 0.0925926
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.8368201

*** Noise = 1.0% ***
previous = 634.0 current = 710.0
previous = 710.0 current = 710.0
Time : 4.008
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 567.0 current = 644.0
previous = 644.0 current = 645.0
previous = 645.0 current = 645.0
Time : 6.547
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 1254.0 current = 1254.0
Time : 2.054
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1074.0 current = 1223.0
previous = 1223.0 current = 1223.0
Time : 3.613
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 266.0 current = 1082.0
previous = 1082.0 current = 1082.0
Time : 5.52
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 494
x+ mean size : 6.4666667
x- mean size : 7.59919
E+ size : 100
E- size : 1391
e+ mean size : 5.86
e- mean size : 7.1056795
Automaton Time : 0.591
65 1
65 1
Recall = 0.65
Precision = 0.9848485
Automaton Fscore : 0.7831325
#Observed states : 194.0
#States : 24
#Transitions : 48
Compression level : 8.083333
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 696.0 current = 739.0
previous = 739.0 current = 739.0
Time : 4.258
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 717.0 current = 760.0
previous = 760.0 current = 760.0
Time : 3.835
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 531.0 current = 653.0
previous = 653.0 current = 653.0
Time : 4.954
Syntactical distance : 0.09722222
Intermediate FSCORE : 0.40983605
Temoral FSCORE : 0.40983605
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 1382.0 current = 1382.0
Time : 1.633
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1258.0 current = 1371.0
previous = 1371.0 current = 1371.0
Time : 3.578
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 686.0 current = 1203.0
previous = 1203.0 current = 1203.0
Time : 5.329
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 452
x+ mean size : 6.266667
x- mean size : 7.641593
E+ size : 100
E- size : 1535
e+ mean size : 6.48
e- mean size : 7.5270357
Automaton Time : 0.702
55 7
55 7
Recall = 0.55
Precision = 0.88709676
Automaton Fscore : 0.67901236
#Observed states : 188.0
#States : 30
#Transitions : 55
Compression level : 6.266667
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 673.0 current = 695.0
previous = 695.0 current = 695.0
Time : 4.118
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 702.0 current = 725.0
previous = 725.0 current = 725.0
Time : 3.765
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 551.0 current = 676.0
previous = 676.0 current = 676.0
Time : 4.081
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 1312.0 current = 1312.0
Time : 1.504
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1174.0 current = 1304.0
previous = 1304.0 current = 1292.0
Time : 2.052
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.89502764
Temoral FSCORE : 0.89502764

*** Noise = 10.0% ***
previous = 610.0 current = 1097.0
previous = 1097.0 current = 1097.0
Time : 5.306
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 481
x+ mean size : 6.733333
x- mean size : 7.7505198
E+ size : 100
E- size : 1540
e+ mean size : 6.12
e- mean size : 7.364935
Automaton Time : 1.085
65 8
65 8
Recall = 0.65
Precision = 0.89041096
Automaton Fscore : 0.75144506
#Observed states : 202.0
#States : 31
#Transitions : 65
Compression level : 6.516129
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 723.0 current = 759.0
previous = 759.0 current = 759.0
Time : 4.054
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 729.0 current = 741.0
previous = 741.0 current = 741.0
Time : 3.751
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 584.0 current = 724.0
previous = 724.0 current = 724.0
Time : 5.262
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 1417.0 current = 1417.0
Time : 1.814
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1243.0 current = 1405.0
previous = 1405.0 current = 1405.0
Time : 4.175
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 758.0 current = 1207.0
previous = 1207.0 current = 1207.0
Time : 6.222
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 455
x+ mean size : 6.3333335
x- mean size : 7.279121
E+ size : 100
E- size : 1465
e+ mean size : 6.18
e- mean size : 7.41843
Automaton Time : 0.66
71 12
71 12
Recall = 0.71
Precision = 0.85542166
Automaton Fscore : 0.7759563
#Observed states : 190.0
#States : 25
#Transitions : 55
Compression level : 7.6
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 634.0 current = 709.0
previous = 709.0 current = 709.0
Time : 3.955
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 700.0 current = 720.0
previous = 720.0 current = 722.0
previous = 722.0 current = 722.0
Time : 6.445
Syntactical distance : 0.0925926
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.8733625

*** Noise = 10.0% ***
previous = 493.0 current = 675.0
previous = 675.0 current = 675.0
Time : 5.493
Syntactical distance : 0.0787037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 1322.0 current = 1322.0
Time : 1.684
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1248.0 current = 1318.0
previous = 1318.0 current = 1318.0
Time : 3.488
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 624.0 current = 1135.0
previous = 1135.0 current = 1135.0
Time : 5.604
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
