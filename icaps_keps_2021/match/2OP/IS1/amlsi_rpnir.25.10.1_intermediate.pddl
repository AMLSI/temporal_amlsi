(define (domain matchcellar)
(:requirements :strips :typing :negative-preconditions)
(:types
match fuse - object
)
(:predicates
	(unused ?x1 - match)
	(light ?x1 - match)
	(handfree)
	(mended ?x1 - fuse)
)
(:action mend_fuse-start
	:parameters (?x1 - fuse ?x2 - match )
	:precondition (and
	(not(mended ?x1))
	(handfree)
	(unused ?x2)
	(light ?x2))
	:effect (and
	(not(handfree))
	(not(unused ?x2)))
)
(:action mend_fuse-end
	:parameters (?x1 - fuse ?x2 - match )
	:precondition (and
	(not(handfree))
	(not(unused ?x2))
	(light ?x2))
	:effect (and
	(mended ?x1)
	(handfree)
	(unused ?x2))
)
(:action light_match-start
	:parameters (?x1 - match )
	:precondition (and
	(unused ?x1)
	(not(light ?x1)))
	:effect (and
	(light ?x1))
)
(:action light_match-end
	:parameters (?x1 - match )
	:precondition (and
	(handfree)
	(light ?x1))
	:effect (and
	(not(light ?x1)))
)
)
