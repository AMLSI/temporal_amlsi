# actions 20
# predicate 13
Initial state : pddl/temporal/match/initial_states/initial3.pddl
pddl/temporal/match/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1426
x+ mean size : 11.266666
x- mean size : 10.959327
E+ size : 100
E- size : 6084
e+ mean size : 11.52
e- mean size : 11.280408
Automaton Time : 21.379
4 20
4 20
Recall = 0.04
Precision = 0.16666667
Automaton Fscore : 0.06451613
#Observed states : 338.0
#States : 65
#Transitions : 185
Compression level : 5.2
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 597.0 current = 1711.0
previous = 1711.0 current = 1711.0
Time : 19.029
Syntactical distance : 0.050925925
Intermediate FSCORE : 0.27972028
Temoral FSCORE : 0.27816412

*** Noise = 1.0% ***
previous = 572.0 current = 1688.0
previous = 1688.0 current = 1688.0
Time : 21.07
Syntactical distance : 0.06944445
Intermediate FSCORE : 0.27972028
Temoral FSCORE : 0.27816412

*** Noise = 10.0% ***
previous = 482.0 current = 1628.0
previous = 1628.0 current = 1628.0
Time : 24.902
Syntactical distance : 0.14351851
Intermediate FSCORE : 0.45454547
Temoral FSCORE : 0.45045042
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 2759.0 current = 2860.0
previous = 2860.0 current = 2860.0
Time : 13.68
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.9304813
Temoral FSCORE : 0.9166666

*** Noise = 1.0% ***
previous = 2158.0 current = 2831.0
previous = 2831.0 current = 2831.0
Time : 18.519
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.9304813
Temoral FSCORE : 0.9166666

*** Noise = 10.0% ***
previous = 704.0 current = 2589.0
previous = 2589.0 current = 2550.0
Time : 31.686
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.9304813
Temoral FSCORE : 0.9166666

RPNIR run : 1
I+ size : 30
I- size : 1623
x+ mean size : 11.0
x- mean size : 10.860135
E+ size : 100
E- size : 5687
e+ mean size : 11.38
e- mean size : 11.218217
Automaton Time : 21.314
6 15
6 15
Recall = 0.06
Precision = 0.2857143
Automaton Fscore : 0.09917355
#Observed states : 330.0
#States : 71
#Transitions : 187
Compression level : 4.647887
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1751.0 current = 1780.0
previous = 1780.0 current = 1780.0
Time : 17.205
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.99009895

*** Noise = 1.0% ***
previous = 798.0 current = 1750.0
previous = 1750.0 current = 1750.0
Time : 25.713
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.99009895

*** Noise = 10.0% ***
previous = 330.0 current = 1690.0
previous = 1690.0 current = 1690.0
Time : 43.726
Syntactical distance : 0.14351851
Intermediate FSCORE : 0.49261087
Temoral FSCORE : 0.49019608
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 2743.0 current = 2797.0
previous = 2797.0 current = 2797.0
Time : 16.865
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.96907216
Temoral FSCORE : 0.9591837

*** Noise = 1.0% ***
previous = 2499.0 current = 2773.0
previous = 2773.0 current = 2773.0
Time : 19.319
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.96907216
Temoral FSCORE : 0.9591837

*** Noise = 10.0% ***
previous = 880.0 current = 2498.0
previous = 2498.0 current = 2498.0
Time : 35.978
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.96907216
Temoral FSCORE : 0.9591837

RPNIR run : 2
I+ size : 30
I- size : 1606
x+ mean size : 10.8
x- mean size : 10.772104
E+ size : 100
E- size : 5994
e+ mean size : 11.1
e- mean size : 11.029363
Automaton Time : 19.233
6 15
6 15
Recall = 0.06
Precision = 0.2857143
Automaton Fscore : 0.09917355
#Observed states : 324.0
#States : 70
#Transitions : 180
Compression level : 4.6285715
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 608.0 current = 1731.0
previous = 1731.0 current = 1731.0
Time : 20.639
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9708738

*** Noise = 1.0% ***
previous = 568.0 current = 1738.0
previous = 1738.0 current = 1738.0
Time : 19.248
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9708738

*** Noise = 10.0% ***
previous = 511.0 current = 1662.0
previous = 1662.0 current = 1662.0
Time : 28.286
Syntactical distance : 0.050925925
Intermediate FSCORE : 0.26525196
Temoral FSCORE : 0.2628121
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 2659.0 current = 2672.0
previous = 2672.0 current = 2724.0
previous = 2724.0 current = 2724.0
Time : 23.978
Syntactical distance : 0.050925925
Intermediate FSCORE : 0.26525196
Temoral FSCORE : 0.2628121

*** Noise = 1.0% ***
previous = 2227.0 current = 2639.0
previous = 2639.0 current = 2692.0
previous = 2692.0 current = 2692.0
Time : 28.866
Syntactical distance : 0.050925925
Intermediate FSCORE : 0.26525196
Temoral FSCORE : 0.2628121

*** Noise = 10.0% ***
previous = 1290.0 current = 2399.0
previous = 2399.0 current = 2399.0
Time : 22.782
Syntactical distance : 0.0925926
Intermediate FSCORE : 0.4357298
Temoral FSCORE : 0.4301075

RPNIR run : 3
I+ size : 30
I- size : 1492
x+ mean size : 11.2
x- mean size : 10.824397
E+ size : 100
E- size : 5762
e+ mean size : 11.84
e- mean size : 11.5473795
Automaton Time : 21.545
3 11
3 11
Recall = 0.03
Precision = 0.21428572
Automaton Fscore : 0.05263158
#Observed states : 336.0
#States : 72
#Transitions : 190
Compression level : 4.6666665
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 604.0 current = 1741.0
previous = 1741.0 current = 1741.0
Time : 18.174
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9950249

*** Noise = 1.0% ***
previous = 617.0 current = 1735.0
previous = 1735.0 current = 1724.0
Time : 21.001
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9950249

*** Noise = 10.0% ***
previous = 485.0 current = 1602.0
previous = 1602.0 current = 1602.0
Time : 29.013
Syntactical distance : 0.050925925
Intermediate FSCORE : 0.27434844
Temoral FSCORE : 0.27322406
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 2725.0 current = 2796.0
previous = 2796.0 current = 2796.0
Time : 15.161
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.9583333
Temoral FSCORE : 0.9583333

*** Noise = 1.0% ***
previous = 2322.0 current = 2778.0
previous = 2778.0 current = 2778.0
Time : 19.322
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.9583333
Temoral FSCORE : 0.9583333

*** Noise = 10.0% ***
previous = 922.0 current = 2433.0
previous = 2433.0 current = 2398.0
Time : 30.271
Syntactical distance : 0.037037037
Intermediate FSCORE : 0.9583333
Temoral FSCORE : 0.9583333

RPNIR run : 4
I+ size : 30
I- size : 1749
x+ mean size : 11.466666
x- mean size : 11.088622
E+ size : 100
E- size : 6067
e+ mean size : 11.14
e- mean size : 11.109939
Automaton Time : 21.11
6 23
6 23
Recall = 0.06
Precision = 0.20689656
Automaton Fscore : 0.093023255
#Observed states : 344.0
#States : 64
#Transitions : 187
Compression level : 5.375
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 1550.0 current = 1987.0
previous = 1987.0 current = 1987.0
Time : 22.128
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9852216

*** Noise = 1.0% ***
previous = 729.0 current = 729.0
Time : 0.193
Syntactical distance : 0.15277778
Intermediate FSCORE : 0.044812907
Temoral FSCORE : 0.043020003

*** Noise = 10.0% ***
previous = 540.0 current = 795.0
previous = 795.0 current = 810.0
previous = 810.0 current = 810.0
Time : 20.272
Syntactical distance : 0.0462963
Intermediate FSCORE : 0.30508474
Temoral FSCORE : 0.30508474
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 3006.0 current = 3006.0
Time : 9.172
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9852216

*** Noise = 1.0% ***
previous = 2208.0 current = 2968.0
previous = 2968.0 current = 2968.0
Time : 23.598
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9852216

*** Noise = 10.0% ***
previous = 707.0 current = 2545.0
previous = 2545.0 current = 2646.0
previous = 2646.0 current = 2646.0
Time : 48.939
Syntactical distance : 0.018518519
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.9852216
