(define (domain matchcellar)
(:requirements :strips :typing :negative-preconditions :durative-actions)
(:types 
fuse match - object
)(:predicates
	(unused ?x1 - MATCH)
	(light ?x1 - MATCH)
	(handfree)
	(mended ?x1 - FUSE)
)
(:durative-action mend_fuse
	:parameters ( ?x1 - FUSE ?x2 - MATCH )
	:duration (= ?duration 2.0)
	:condition (and
		(at start (handfree) )
		(at start (unused ?x2) )
		(over all (not (mended ?x1)) )
		(at end (light ?x2) )
	)
	:effect (and
		(at start (not (handfree)) )
		(at start (not (unused ?x2)) )
		(at start (light ?x2) )
		(at end (handfree) )
		(at end (unused ?x2) )
	)
)
(:durative-action light_match
	:parameters ( ?x1 - MATCH )
	:duration (= ?duration 5.0)
	:condition (and
		(at start (not (light ?x1)) )
		(at end (light ?x1) )
	)
	:effect (and
		(at start (light ?x1) )
		(at end (not (light ?x1)) )
	)
)
)