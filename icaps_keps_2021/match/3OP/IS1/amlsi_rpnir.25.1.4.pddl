(define (domain matchcellar)
(:requirements :strips :typing :negative-preconditions :durative-actions)
(:types 
fuse match - object
)(:predicates
	(unused ?x1 - MATCH)
	(light ?x1 - MATCH)
	(handfree)
	(mended ?x1 - FUSE)
)
(:durative-action mend_fuse
	:parameters ( ?x1 - FUSE ?x2 - MATCH )
	:duration (= ?duration 2.0)
	:condition (and
		(at start (not (mended ?x1)) )
		(at start (handfree) )
		(over all (not (unused ?x2)) )
		(over all (light ?x2) )
	)
	:effect (and
		(at start (not (handfree)) )
		(at end (mended ?x1) )
		(at end (handfree) )
		(at end (light ?x2) )
	)
)
(:durative-action light_match
	:parameters ( ?x1 - MATCH )
	:duration (= ?duration 5.0)
	:condition (and
		(at start (unused ?x1) )
		(at start (not (light ?x1)) )
		(at end (handfree) )
		(at end (light ?x1) )
	)
	:effect (and
		(at start (not (unused ?x1)) )
		(at start (light ?x1) )
		(at end (not (light ?x1)) )
	)
)
)