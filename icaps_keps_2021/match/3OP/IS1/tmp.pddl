(define (domain matchcellar)
(:requirements :strips :typing :negative-preconditions)
(:types 
fuse match - object
)(:predicates
	(unused ?x1 - MATCH)
	(light ?x1 - MATCH)
	(handfree)
	(mended ?x1 - FUSE)
)
(:action mend_fuse-start
	:parameters ( ?x1 - FUSE ?x2 - MATCH )
	:precondition (and
		(not (mended ?x1))
		(handfree)
	)
	:effect (and
		(not (handfree))
	)
)
(:action mend_fuse-end
	:parameters ( ?x1 - FUSE ?x2 - MATCH )
	:precondition (and
	)
	:effect (and
		(mended ?x1)
		(handfree)
	)
)
(:action mend_fuse-inv
	:parameters ( ?x1 - FUSE ?x2 - MATCH )
	:precondition (and
		(not (unused ?x2))
		(light ?x2)
	)
	:effect (and
	)
)
(:action light_match-start
	:parameters ( ?x1 - MATCH )
	:precondition (and
		(unused ?x1)
		(not (light ?x1))
	)
	:effect (and
		(not (unused ?x1))
		(light ?x1)
	)
)
(:action light_match-end
	:parameters ( ?x1 - MATCH )
	:precondition (and
		(handfree)
		(light ?x1)
	)
	:effect (and
		(not (light ?x1))
	)
)
(:action light_match-inv
	:parameters ( ?x1 - MATCH )
	:precondition (and
	)
	:effect (and
	)
)
)