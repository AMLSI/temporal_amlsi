# actions 6
# predicate 7
Initial state : pddl/temporal/match/initial_states/initial1.pddl
pddl/temporal/match/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 446
x+ mean size : 12.9
x- mean size : 16.159193
E+ size : 100
E- size : 1416
e+ mean size : 12.63
e- mean size : 14.891949
Automaton Time : 2.921
73 11
73 11
Recall = 0.73
Precision = 0.86904764
Automaton Fscore : 0.79347825
#Observed states : 387.0
#States : 22
#Transitions : 73
Compression level : 17.59091
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 587.0 current = 1009.0
previous = 1009.0 current = 1009.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 9.869
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 572.0 current = 1056.0
previous = 1056.0 current = 1056.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 12.579
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 431.0 current = 960.0
previous = 960.0 current = 960.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 14.732
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 1956.0 current = 1956.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 4.463
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 895.0 current = 1907.0
previous = 1907.0 current = 1907.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 12.297
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 742.0 current = 1684.0
previous = 1684.0 current = 1684.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 18.506
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 494
x+ mean size : 13.7
x- mean size : 16.455465
E+ size : 100
E- size : 1391
e+ mean size : 12.09
e- mean size : 14.938893
Automaton Time : 3.15
63 4
63 4
Recall = 0.63
Precision = 0.9402985
Automaton Fscore : 0.75449103
#Observed states : 411.0
#States : 32
#Transitions : 78
Compression level : 12.84375
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 958.0 current = 1101.0
previous = 1101.0 current = 1096.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 10.288
Syntactical distance : 0.06481481
Intermediate FSCORE : 0.7220217
Temoral FSCORE : 0.7194244

*** Noise = 1.0% ***
previous = 970.0 current = 1131.0
previous = 1131.0 current = 1124.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 12.533
Syntactical distance : 0.050925925
Intermediate FSCORE : 0.7220217
Temoral FSCORE : 0.7194244

*** Noise = 10.0% ***
previous = 172.0 current = 172.0
{mend_fuse=[(handfree), (unused ?x2)], light_match=[(not (light ?x1))]}
Time : 0.078
Syntactical distance : 0.10185185
Intermediate FSCORE : 0.0
Temoral FSCORE : 0.28193834
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 2127.0 current = 2127.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 4.861
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1491.0 current = 2112.0
previous = 2112.0 current = 2112.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 12.2
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 834.0 current = 1693.0
previous = 1693.0 current = 1672.0
{mend_fuse=[(mended ?x1), (handfree), (not (unused ?x2)), (light ?x2)], light_match=[(not (light ?x1))]}
Time : 12.282
Syntactical distance : 0.060185183
Intermediate FSCORE : 0.5815603
Temoral FSCORE : 0.5815603

RPNIR run : 2
I+ size : 30
I- size : 452
x+ mean size : 12.866667
x- mean size : 16.090708
E+ size : 100
E- size : 1535
e+ mean size : 13.96
e- mean size : 16.481434
Automaton Time : 4.006
55 21
55 21
Recall = 0.55
Precision = 0.7236842
Automaton Fscore : 0.625
#Observed states : 386.0
#States : 39
#Transitions : 97
Compression level : 9.897436
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 578.0 current = 1015.0
previous = 1015.0 current = 1015.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 10.487
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 600.0 current = 1044.0
previous = 1044.0 current = 1044.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 10.554
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 327.0 current = 1002.0
previous = 1002.0 current = 1000.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 10.641
Syntactical distance : 0.06481481
Intermediate FSCORE : 0.5865103
Temoral FSCORE : 0.5865103
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 1986.0 current = 1986.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 4.405
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1690.0 current = 1974.0
previous = 1974.0 current = 1974.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 10.189
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 603.0 current = 1651.0
previous = 1651.0 current = 1651.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 17.819
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 481
x+ mean size : 15.166667
x- mean size : 17.841995
E+ size : 100
E- size : 1540
e+ mean size : 13.18
e- mean size : 16.374676
Automaton Time : 7.382
70 6
70 6
Recall = 0.7
Precision = 0.92105263
Automaton Fscore : 0.7954545
#Observed states : 455.0
#States : 42
#Transitions : 111
Compression level : 10.833333
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 635.0 current = 1174.0
previous = 1174.0 current = 1174.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 13.047
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 442.0 current = 442.0
{mend_fuse=[(mended ?x1)], light_match=[(not (light ?x1))]}
Time : 0.088
Syntactical distance : 0.083333336
Intermediate FSCORE : 0.30452675
Temoral FSCORE : 0.30452675

*** Noise = 10.0% ***
previous = 486.0 current = 792.0
previous = 792.0 current = 788.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(handfree)]}
Time : 21.205
Syntactical distance : 0.14351852
Intermediate FSCORE : 0.540146
Temoral FSCORE : 0.540146
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 2288.0 current = 2288.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 5.318
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1553.0 current = 2270.0
previous = 2270.0 current = 2270.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 13.235
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 1002.0 current = 1942.0
previous = 1942.0 current = 1942.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 19.96
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 455
x+ mean size : 13.7
x- mean size : 15.973626
E+ size : 100
E- size : 1465
e+ mean size : 13.21
e- mean size : 16.309898
Automaton Time : 3.557
81 12
81 12
Recall = 0.81
Precision = 0.87096775
Automaton Fscore : 0.8393782
#Observed states : 411.0
#States : 27
#Transitions : 76
Compression level : 15.222222
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
previous = 606.0 current = 1048.0
previous = 1048.0 current = 1048.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 11.77
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 587.0 current = 1051.0
previous = 1051.0 current = 1053.0
previous = 1053.0 current = 1055.0
previous = 1055.0 current = 1053.0
{mend_fuse=[(mended ?x1), (handfree), (light ?x2)], light_match=[(not (light ?x1))]}
Time : 19.253
Syntactical distance : 0.050925925
Intermediate FSCORE : 1.0
Temoral FSCORE : 0.8733625

*** Noise = 10.0% ***
previous = 427.0 current = 1004.0
previous = 1004.0 current = 1004.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(handfree), (not (light ?x1))]}
Time : 21.693
Syntactical distance : 0.06944445
Intermediate FSCORE : 0.65789473
Temoral FSCORE : 0.65789473
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
previous = 2076.0 current = 2076.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 4.551
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 1.0% ***
previous = 1558.0 current = 2064.0
previous = 2064.0 current = 2064.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 11.602
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0

*** Noise = 10.0% ***
previous = 492.0 current = 1749.0
previous = 1749.0 current = 1749.0
{mend_fuse=[(mended ?x1), (handfree)], light_match=[(not (light ?x1))]}
Time : 19.11
Syntactical distance : 0.037037037
Intermediate FSCORE : 1.0
Temoral FSCORE : 1.0
