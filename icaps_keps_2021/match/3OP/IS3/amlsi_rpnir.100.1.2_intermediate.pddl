(define (domain matchcellar)
(:requirements :strips :typing :negative-preconditions)
(:types
match fuse - object
)
(:predicates
	(unused ?x1 - match)
	(light ?x1 - match)
	(handfree)
	(mended ?x1 - fuse)
)
(:action mend_fuse-start
	:parameters (?x1 - fuse ?x2 - match )
	:precondition (and
	(not(mended ?x1))
	(handfree)
	(not(unused ?x2))
	(light ?x2))
	:effect (and)
)
(:action mend_fuse-end
	:parameters (?x1 - fuse ?x2 - match )
	:precondition (and)
	:effect (and
	(light ?x2))
)
(:action light_match-start
	:parameters (?x1 - match )
	:precondition (and
	(not(light ?x1)))
	:effect (and
	(light ?x1))
)
(:action light_match-end
	:parameters (?x1 - match )
	:precondition (and
	(light ?x1))
	:effect (and
	(not(light ?x1)))
)
(:action light_match-inv
	:parameters (?x1 - match )
	:precondition (and)
	:effect (and)
)
(:action mend_fuse-inv
	:parameters (?x1 - fuse ?x2 - match )
	:precondition (and)
	:effect (and)
)
)
